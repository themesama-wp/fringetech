<?php
/*
	The template for displaying Comments.
*/
?>
<?php 
function comment_theme($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class("clearfix"); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment_wrap">
			<div class="gravatar">
				<?php echo get_avatar($comment,$size='60',$default=''); ?>
				<time class="comment_time"><?php echo get_comment_date(); ?></time>
			</div>
			<div class='comment_content'>
				<div class="comment_meta">
					<h4><?php printf( '<cite class="comment_author">%s</cite>', get_comment_author_link()) ?></h4>
					
				</div>
				<div class='comment_text'>
					<?php comment_text() ?>
					<?php if ($comment->comment_approved == '0') : ?>
					<span class="unapproved"><?php _e('Your comment is awaiting moderation.', 'fringe_tech') ?></span>
					<?php endif; ?>
					<?php edit_comment_link(__('(Edit)', 'fringe_tech' ),'  ','') ?>
				</div>
				<div class="reply">
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			</div>
		</div>
<?php
}
?>

<div id="comments">
<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'fringe_tech' ); ?></p>
</div>
<?php	return;
	endif;
?>

<?php if ( have_comments() ) : ?>
	<h3 id="comments-title"><?php
	printf( _n( 'One Response to %2$s', '%1$s Responses to %2$s', get_comments_number(), 'fringe_tech' ),
	number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
	?></h3>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<div class="navigation">
		<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'fringe_tech' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'fringe_tech' ) ); ?></div>
	</div>
<?php endif; ?>
<div id="contents">
	<ol class="commentlist">
		<?php wp_list_comments( array( 'callback' => 'comment_theme' ) ); ?>
	</ol>
</div>
<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'fringe_tech' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'fringe_tech' ) ); ?></div>
			</div><!-- .navigation -->
<?php endif; // check for comment navigation ?>

<?php else :
	if ( ! comments_open() ) :
?>
	</div>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>


<?php if ( comments_open() ) { ?>

	<div id="respond">

		<h3><?php comment_form_title( __( 'Leave a Reply', 'fringe_tech' ), __( 'Leave a Reply to %s', 'fringe_tech' ) ); ?></h3>

		<div class="cancel-comment-reply">
			<small><?php cancel_comment_reply_link(); ?></small>
		</div>

		<?php 
		if ( get_option('comment_registration') && !is_user_logged_in() ) {
			printf( __('<p style="clear:both;">You must be <a href="%s">logged in</a> to post a comment.</p></div>', 'fringe_tech' ), wp_login_url( get_permalink()) );
		} else { 
		?>

			<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="sendform">

				<?php if ( is_user_logged_in() ) { ?>

				<div class="logged_out">
					<?php printf( __('Logged in as <a href="%1$s/wp-admin/profile.php">%2$s</a>.', 'fringe_tech' ), get_option('siteurl'), $user_identity ); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php esc_html_e('Log out of this account', 'fringe_tech'); ?>"><?php esc_html_e('Log out &raquo;', 'fringe_tech'); ?></a>
				</div>
				
				<?php  } else { ?>

				<span class="comment_element">
					
					<span class="comment_input">
					
						<input type="text" name="author" class="input" id="author" value="<?php if($comment_author != "") { echo $comment_author; } else { echo "Name *"; } ?>" size="22" tabindex="1"  />
					
					</span>
					
					<span class="comment_label">
					
						<small><?php if ($req) esc_html_e('* required', 'fringe_tech'); ?></small>
						
					</span>
					
				</span>

				<span class="comment_element">
					
					<span class="comment_input">
						
						<input type="text" name="email" class="input" id="email" value="<?php if($comment_author_email != "") { echo $comment_author_email; } else { echo "Email *"; } ?>" size="22" tabindex="2" />
				
					</span>
					
					<span class="comment_label">
					
						<small><?php esc_html_e('(will not be published)', 'fringe_tech'); ?> <?php if ($req) esc_html_e('* required', 'fringe_tech'); ?></small>
						
					</span>
					
				</span>

				<span class="comment_element">
					
					<span class="comment_input">
						
						<input type="text" name="url" class="input" id="url" value="<?php if($comment_author_url != "") { echo $comment_author_url; } else { echo "Web Site"; } ?>" size="22" tabindex="3" />		
		
					</span>

				</span>

				<?php } ?>

				<span class="comment_element">

					<span class="comment_textarea">

						<textarea class="textarea" name="comment" id="comment" cols="70" rows="10" tabindex="4"><?php esc_html_e('Message *', 'fringe_tech'); ?></textarea>
						
					</span>
					
				</span>
				
				<div class="comment_element">
				
					<div style="float:left;">
						<h6>
							<span class="small_buttons <?php if ( !is_user_logged_in() ) { ?>send_comment<?php } else { ?>send_comment_admin<?php } ?>">
								<span class="<?php echo get_option("button_colors", "ocean"); ?>_l small_left"><span class="<?php echo get_option("button_colors", "ocean"); ?>_r small_right"><?php echo get_option("button_post_comment", "Post Comment"); ?></span></span>
							</span>
						</h6>
					</div>
					
					<span class="comment_message"></div>
					<p><?php comment_id_fields(); ?></p>
					
				</div>

				<?php do_action('comment_form', $post->ID); ?>

			</form>

		<?php } ?>
	</div>
<?php }  ?>

