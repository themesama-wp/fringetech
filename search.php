<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div class="page_wrap">

<?php if ( have_posts() ) : ?>

	<div class="page_title">
		<h2><?php printf( __( 'Search Results for: %s', 'fringe_tech' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
	</div>
	
	<div class="page_container alignleft full">
	<?php get_template_part( 'loop', 'search' ); ?>
	
<?php else : ?>

	<div class="page_title">
		<h2><?php _e( 'Nothing Found', 'fringe_tech' ); ?></h2>
	</div>
	
		<div class="page_container alignleft full">
				
			<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'fringe_tech' ); ?></p>
			
			<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
				<input size="50" name="s" id="s" type="text" value="<?php echo get_option("header_search_value", "Enter your search keywords..."); ?>" />
				<input type="submit" value="search">
			</form>
			
				
<?php endif; ?>
		</div>

	</div>

<?php get_footer(); ?>
