<?php
/*
	Available vars:
	
	content_type 		= $type
	content_name 		= $name
	content_value 		= $value
	content_custom		= $custom_value
	content_attribute	= $attribute
	content_group 		= $group
	content_class 		= $class
*/
function render_item($type=null, $name=null, $value=null, $custom_value=null, $attribute=null, $group=null, $class=null){
	
	global $post, $wp_version;
	
	$str = "";
	$custom = "";
	$values = $value;

	if(get_option($name) != ""){
		$current_values = get_option($name);
	}else if( !empty($post) ){
		$current_values = get_post_meta($post->ID, $name, true);
	}else if($custom_value){
		$current_values = $custom_value;
	}else{
		$current_values = $value;
	}

	$value_array = "options";
	$name_framework = "[".$name."]";

	
	
	switch($type){
		

		case 'text':
			$str.= "<div class=\"option_text\" ".$attribute.">".$name."</div>";
		break;
		

		case 'input':
			$str.= "<div class=\"option_input_text ".$class."\"><input type=\"text\" id=\"".$name."\" name=\"".$value_array.$name_framework."\" value=\"".$current_values."\" ".$attribute."></div>";
		break;


		case 'textarea':
			$str.= "<div class=\"option_textarea ".$class."\"><textarea id=\"".$name."\" name=\"".$value_array.$name_framework."\" ".$attribute.">".$current_values."</textarea></div>";
		break;

		case 'select':
			
			$str	.= "<div class=\"option_select ".$class."\">";

			if (preg_match('/\[posts\]/i', $values)){
				

				$category = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($category[1] == "all"){
					$args["post_type"] = array( 'post', 'blog_mod', 'portfolio_mod' );
				}else{
					$args["post_type"] = $category[1];
				}
				$args["order"] = $category[2];
				$args["orderby"] = $category[3];

				$options_array = query_posts($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
			}else if(preg_match('/\[pages\]/i', $values)){
				

				$arg = str_replace(array("[","]"), array("",""), explode(",", $values));
				$args["sort_order"]	= $arg[1];
				$args["sort_column"]	= $arg[2];
				
				$options_array = &get_pages($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
			}else if(preg_match('/\[categories\]/i', $values)){
				

				$taxonomy = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($taxonomy[1] == "all"){
					$args["taxonomy"] = array( 'category', 'blog_type', 'portfolio_type' );
				}else{
					$args["taxonomy"] = $taxonomy[1];
				}
				$args["hide_empty"] = false;
				$args["order"] = $taxonomy[2];
				$args["orderby"] = $taxonomy[3];
				
				$options_array = &get_categories($args);
				
				$get_name = "name";
				$check_name = "term_id";
				
			}else if(preg_match('/\[sliders\]/i', $values)){
				

				global $wpdb, $category_table;
				
				$s_types = str_replace(array("[","]"), array("",""), explode(",", $values));
				$query_attr = "";
				
				if($s_types[1] != "all"){
					$query_attr = "WHERE s_id = '". $s_types[1]."'";
				}
				
				$options_array = $wpdb->get_results("SELECT id,c_name FROM ". $category_table ." ". $query_attr ." ORDER BY ". $s_types[3] ." ". $s_types[2]."");
				
				$get_name = "c_name";
				$check_name = "id";
				
			}else{
				$options_array = explode(',', $values);
				$custom = true;
			}
			

			if(get_option($name) != ""){
				$mkarray = explode(",", get_option($name));
			}else if( !empty($post) ){
				$mkarray = explode(",", get_post_meta($post->ID, $name, true));
			}else if($custom_value != ""){
				$mkarray = explode(",", $custom_value);
			}else{
				$mkarray = array();
			}
			

			$str	.= "<select name=\"".$value_array."[selectboxes][".$name."][]\" ".$attribute.">";
			

			if ( !empty( $options_array[0] ) ){


				if( !preg_match('/multiple/i', $attribute) ){
				$str	.= "<option value=\"\">-- Choose one --</option>";
				}
				

				foreach ( $options_array as $option ){
					

					if($custom == true){
					

						$selected = '';
						$orginal_option = $option;
						

						$option	= str_replace("=selected", "", $option);
	

						if ( in_array( trim($option), $mkarray) ) {
							$selected = ' selected="selected"';
						}else if( preg_match('/=selected/i', $orginal_option) && !get_option($name) && empty($post) && !$custom_value ){
							$selected = ' selected="selected"';
						}						

						$str	.= "<option value=\"".$option."\" ".$selected.">".$option."</option>";
					}else{
					
						$selected = '';

						if ( in_array( trim($option->$check_name), $mkarray) ) {
							$selected = ' selected="selected"';
						}

						$str	.= "<option value=\"".$option->$check_name."\" ".$selected.">".$option->$get_name."</option>";
					}

				}
			} else {
				$str	.= '<option value="">No Items Available</option>';
			}
			
			$str	.= "</select>";
			
			
			if( preg_match('/multiple/i', $attribute) && $options_array[0] != ""){
				$str	.= "<div><span class=\"select_all span_select\">Select All</span> <span class=\"span_select\">&nbsp;-&nbsp;</span> <span class=\"unselect_all span_select\">Reset</span></div>";
			}

			$hidden = "";
			if( preg_match('/=selected/i', $values) ) { $hidden = "~-~"; }
			
			$str	.= "<input type=\"hidden\" name=\"".$value_array."[selectboxes][".$name."][]\" value=\"". $hidden ."\" checked=\"checked\">";
			$str	.= "</div>";

		break;
		

		case 'radio':
		
			$class 	!= "" ? $class_out = " class=\"".$class."\"" : $class_out = "";
			$str	.= "<div".$class_out.">";
			
			if (preg_match('/\[posts\]/i', $values)){
			
				$category = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($category[1] == "all"){
					$args["post_type"] = array( 'post', 'blog_mod', 'portfolio_mod' );
				}else{
					$args["post_type"] = $category[1];
				}
				$args["order"] = $category[2];
				$options_array = query_posts($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
				
			}else if(preg_match('/\[pages\]/i', $values)){
			
				$arg = str_replace(array("[","]"), array("",""), explode(",", $values));
				$args["sort_order"]	= $arg[1];
				$args["sort_column"]	= $arg[2];
				
				$options_array = &get_pages($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
			}else if(preg_match('/\[categories\]/i', $values)){
				
				$taxonomy = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($taxonomy[1] == "all"){
					$args["taxonomy"] = array( 'category', 'blog_type', 'portfolio_type' );
				}else{
					$args["taxonomy"] = $taxonomy[1];
				}
				$args["hide_empty"] = false;
				$args["order"] = $taxonomy[2];
				$args["orderby"] = $taxonomy[3];
				
				$options_array = &get_categories($args);
				
				$get_name = "name";
				$check_name = "term_id";
				
			}else if(preg_match('/\[sliders\]/i', $values)){
				

				global $wpdb, $category_table;
				
				$s_types = str_replace(array("[","]"), array("",""), explode(",", $values));
				$query_attr = "";
				
				if($s_types[1] != "all"){
					$query_attr = "WHERE s_id = '". $s_types[1]."'";
				}
				
				$options_array = $wpdb->get_results("SELECT id,c_name FROM ". $category_table ." ". $query_attr ." ORDER BY ". $s_types[3] ." ". $s_types[2]."");
				
				$get_name = "c_name";
				$check_name = "id";
				
			}else{
				$options_array = explode(',', $values);
				$custom = true;
			}
			
			if(get_option($name) != ""){
				$mkarray = explode(",", get_option($name));
			}else if( !empty($post) ){
				$mkarray = explode(",", get_post_meta($post->ID, $name, true));
			}else if($custom_value != ""){
				$mkarray = explode(",", $custom_value);
			}else{
				$mkarray = array();
			}			
			
			if ( $options_array[0] != "" ){

				$str	.= "<ul>";
				foreach ( $options_array as $option ) 
				{
					
					if($custom == true){
					
						$checked = '';
						
						$orginal_option = $option;
						$option	= str_replace("=selected", "", $option);
	
						if ( in_array( trim($option), $mkarray) ) {
							$checked = ' checked="checked"';
						}else if( preg_match('/=selected/i', $orginal_option) ){
							$checked = ' checked="checked"';
						}

						$str	.= "<li class=\"radio_" . $name."_".$option . "\"><label><input type=\"radio\" name=\"".$value_array."[radios][".$name."][]\" value=\"".$option."\" ".$checked.">&nbsp;".$option."</label></li>";
					}else{
					
						$checked = '';
						
						if ( in_array( trim($option->$check_name), $mkarray) ) {
							$checked = ' checked="checked"';
						}
						
						$str	.= "<li class=\"radio_" . $name."_".$option->$get_name . "\"><label><input type=\"radio\" name=\"".$value_array."[radios][".$name."][]\" value=\"".$option->$check_name."\" ".$checked.">&nbsp;".$option->$get_name."</label></li>";
					}

				}
			}else {
				echo "<li style=\"list-style:none;\">No Items Available</li>";
			}
			$str	.= "</ul>";
			
			$str	.= "<input type=\"hidden\" name=\"".$value_array."[radios][".$name."][]\" value=\"\" checked=\"checked\">";
			
			$str	.= "</div>";
			
		break;

		case 'checkbox':
		
			$class 	!= "" ? $class_out = " class=\"".$class."\"": $class_out = "";
			$str	.= "<div".$class_out.">";
			
			if (preg_match('/\[posts\]/i', $values)){

				$category = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($category[1] == "all"){
					$args["post_type"] = array( 'post', 'blog_mod', 'portfolio_mod' );
				}else{
					$args["post_type"] = $category[1];
				}
				$args["order"] = $category[2];
				$options_array = query_posts($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
			}else if(preg_match('/\[pages\]/i', $values)){
			
				$arg = str_replace(array("[","]"), array("",""), explode(",", $values));
				$args["sort_order"]	= $arg[1];
				$args["sort_column"]	= $arg[2];
				
				$options_array = &get_pages($args);
				$get_name = "post_title";
				$check_name = "post_title";
				
			}else if(preg_match('/\[categories\]/i', $values)){
				
				$taxonomy = str_replace(array("[","]"), array("",""), explode(",", $values));
				if($taxonomy[1] == "all"){
					$args["taxonomy"] = array( 'category', 'blog_type', 'portfolio_type' );
				}else{
					$args["taxonomy"] = $taxonomy[1];
				}
				$args["hide_empty"] = false;
				$args["order"] = $taxonomy[2];
				$args["orderby"] = $taxonomy[3];
				
				$options_array = &get_categories($args);
				
				$get_name = "name";
				$check_name = "term_id";
				
			}else if(preg_match('/\[sliders\]/i', $values)){
				

				global $wpdb, $category_table;
				
				$s_types = str_replace(array("[","]"), array("",""), explode(",", $values));
				$query_attr = "";
				
				if($s_types[1] != "all"){
					$query_attr = "WHERE s_id = '". $s_types[1]."'";
				}
				
				$options_array = $wpdb->get_results("SELECT id,c_name FROM ". $category_table ." ". $query_attr ." ORDER BY ". $s_types[3] ." ". $s_types[2]."");
				
				$get_name = "c_name";
				$check_name = "id";
				
			}else{
				$options_array = explode(',', $values);
				$custom = true;
			}
			
			
			if(get_option($name) != ""){
				$mkarray = explode(",", get_option($name));
			}else if( !empty($post) ){
				$mkarray = explode(",", get_post_meta($post->ID, $name, true));
			}else if($custom_value != ""){
				$mkarray = explode(",", $custom_value);
			}else{
				$mkarray = array();
			}
			
			if ( $options_array[0] != "" ){
				
				$str	.= "<ul>";
				foreach ( $options_array as $option ) 
				{
					
					if($custom == true){
						
						$checked = '';
						
						$orginal_option = $option;
						$option	= str_replace("=selected", "", $option);
	
						if ( in_array( trim($option), $mkarray) ) {
							$checked = ' checked="checked"';
						}else if( preg_match('/=selected/i', $orginal_option) && !get_option($name) && !empty($post) && !$custom_value ){
							$checked = ' checked="checked"';
						}
						
						$str	.= "<li class=\"check_" . $name."_".$option . "\"><label><input type=\"checkbox\" name=\"".$value_array."[checkboxes][".$name."][]\" value=\"".$option."\" ".$checked.">&nbsp;".$option."</label></li>";
					}else{
					
						$checked = '';
						
						if ( in_array( trim($option->$check_name), $mkarray) ) {
							$checked = ' checked="checked"';
						}
						
						$str	.= "<li class=\"radio_" . $name."_".$option->$get_name . "\"><label><input type=\"checkbox\" name=\"".$value_array."[checkboxes][".$name."][]\" value=\"".$option->$check_name."\" ".$checked.">&nbsp;".$option->$get_name."</label></li>";
					}

				}
			} else {
				echo "<li style=\"list-style:none;\">No Items Available</li>";
			}
			$str	.= "</ul>";
			
			if($options_array[0] != ""){
				$str	.= "<span class=\"checkbox_all span_select\">Select All</span> <span class=\"span_select\">&nbsp;-&nbsp;</span> <span class=\"uncheckbox_all span_select\">Reset</span>";
			}

			$hidden = "";
			if( preg_match('/=selected/i', $values) ) { $hidden = "~-~"; }
			
			$str	.= "<input type=\"hidden\" name=\"".$value_array."[checkboxes][".$name."][]\" value=\"". $hidden ."\" checked=\"checked\">";
			
			$str	.= "</div>";
			
		break;
		

		case 'colorpicker':
		ob_start();
		$class 	!= "" ? $class_out = " class=\"".$class."\"": $class_out = "";
		?>
		<div <?php echo $class_out; ?> style="width:65%;" <?php echo $attribute; ?>>
			<script type="text/javascript">make_colorpicker("<?php echo $name; ?>");</script>
			<div class="option_colorpicker">
				<input type="text" name="<?php echo $value_array.$name_framework; ?>" id="colorpic_<?php echo $name; ?>" value="<?php echo $current_values; ?>" class="inputcolor_<?php echo $name; ?>" />
			</div>
			<div id="colordiv_<?php echo $name; ?>" class="cp_box">
			  <div style="background-color:<?php echo $current_values; ?>;">&nbsp;</div>
			</div> 
		</div>
		<?php
		$str	.= ob_get_clean();
		break;
		

		case 'header':
			$str	.= "<div class=\"option_header ".$class."\" ".$attribute.">". $current_values ."</div>";
		break;


		case 'full_text':
			$str	.= "<div class=\"option_full_text ".$class."\" ".$attribute.">". $current_values ."</div>";
		break;
		
		

		case 'upload':
			$class 	!= "" ? $class_out = " class=\"".$class."\"": $class_out = "";
			$str	.= "<div".$class_out.">";
			$str	.= "<div class=\"option_upload_input\"><textarea id=\"upload_".$name."\" name=\"".$value_array.$name_framework."\" class=\"upload_input upload_".$name."\" ".$attribute.">". $current_values ."</textarea></div>";
			$str	.= "<div class=\"option_upload_insert\"><a href=\"media-upload.php?post_id=".last_pid."&type=image&TB_iframe=true\" id=\"upload_".$name."\" class=\"set_input thickbox\" title=\"Uploading...\">&nbsp;</a></div>";
			$str	.= "</div>";
		break;


		case 'slider_ui':
			$valuem = explode(",", $values);
			
			if(get_option($name) != ""){
				$cur_val = get_option($name);
			}else if( !empty($post) ){
				$cur_val = get_post_meta($post->ID, $name, true);
			}else if($custom_value != ""){
				$cur_val = $custom_value;
			}else{
				$cur_val = $valuem[0];
			}
		ob_start();
		$class 	!= "" ? $class_out = " class=\"".$class."\"": $class_out = "";		
		?>
		<script type="text/javascript">make_slider_ui("<?php echo $name; ?>", "<?php echo $cur_val; ?>", "<?php echo $valuem[1]; ?>", "<?php echo $valuem[2]; ?>");</script>
		
		<div <?php echo $class_out; ?> <?php echo $attribute; ?>>
		
			<div class="option_slider_holder">
			
				<div class="option_slider_drag">
					<div id="slider_ui_<?php echo $name; ?>"></div>
				</div>
				
			</div>
			
			<div class="option_slider_text">
				<input type="text" class="slider_ui_name_<?php echo $name; ?>" name="<?php echo $value_array.$name_framework; ?>" value="<?php echo $cur_val; ?>" />
				<div class="option_slider_info"><?php echo $valuem[3]; ?></div>
			</div>

			<?php if ( @$valuem[4] == "font" ) { ?>
			<div class="slider_ui_size_<?php echo $name; ?>" style="display:table; clear:both; font-size:<?php echo $cur_val; ?>px; line-height:<?php echo $cur_val; ?>px; margin:5px 0;">(Live Preview Size)</div>
			<?php } else if ( @$valuem[4] == "alpha" ) { ?>
			<div class="slider_ui_alpha_<?php echo $name; ?> option_slider_alpha">Live Alpha</div>
			<?php } ?>
		
		</div>
		
		<?php
		$str	.= ob_get_clean();
		break;
		


		case 'on_off_ui':
			
			if( $current_values == "on" ) { $status = "on"; } else { $status = "off"; }
			?>
			<script type="text/javascript"> make_on_off_ui("<?php echo $name; ?>"); </script>
			<?php
			$str	.= "<div class=\"lambs lamb_".$status."\"></div>";
			$str	.= "<input id=".$name." type=\"checkbox\" name=\"".$value_array."[checkboxes][".$name."][]\" value=\"".$status."\" checked=\"checked\" ".$attribute.">";
		break;

		case 'tinymce':
			/* [UPDATED] */
			ob_start();
			if ( $wp_version >= 3.3 ) {
				wp_editor($current_values, $name, array('dfw' => false, 'textarea_rows'=> 10, 'textarea_name' => $value_array.$name_framework, 'tabindex' => 1) );
			}else{
				$str	.= "<div class=\"option_textarea ".$class."\"><textarea id=\"".$name."\" name=\"".$value_array.$name_framework."\" ".$attribute.">".$current_values."</textarea></div>";
			}
			$str	.= ob_get_clean();
		break;
		
		case 'help':
			$str.= "<div class=\"option_help\" ".$attribute."><img src=\"[path_libs]/images/help.png\" alt=\"\" class=\"help_img\"/> ". $name ."</div>";
		break;

		case 'tooltip':
			$str.= "<div class=\"option_tooltip\"><img src=\"[path_libs]/images/help.png\" alt=\"\" class=\"help_img\"/> <span class=\"tooltip\">". $name ."</span></div>";
		break;

		case 'blank':
			$str.= "<div>&nbsp;</div>";
		break;


	}
	
	$str  = str_replace(array('[path]', '[path_libs]', '[path_wp_admin]'), array(get_template_directory_uri(), get_template_directory_uri()."/".F_PATH."/libs", site_url()."/wp-admin/"), $str);
	
	echo $str;
}
?>