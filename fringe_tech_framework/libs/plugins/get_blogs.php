<?php
function get_blogs($b_id, $sidebar){
	global $wpdb, $taxonomy_table, $post;
	
	ob_start();
?>
<?php
	if($sidebar == "none"){
		$sidebar = "full";
	}
	$sidebar_mod	= get_sidebar_single($sidebar);
	$sidebar_pos	= $sidebar_mod[2];
	$position		= $sidebar_mod[1];
	$class			= $sidebar_mod[0];

?>
	<div class="page_container align<?php echo $position; ?> <?php echo $class; ?>">
		
		<?php
		
			/* getting category settings */
			
			$get_datas = $wpdb->get_results("SELECT * FROM " . $taxonomy_table . " WHERE t_id IN (" . $b_id . ")");
			$it_work = false;
			
			foreach($get_datas as $data){
				
				$blog_type	= get_the_value( "blog_type", htmlSafe( $data->t_value));
				$b_limit 	= get_the_value( "b_limit", htmlSafe( $data->t_value) );
				$b_info 	= get_the_value( "b_info", htmlSafe( $data->t_value) );
				
				$it_work = true;
			}

			if( $it_work != true ){
				$blog_type	= "style1";
				$b_limit 	= 10;
				$b_info 	= "on";			
			}
		?>
		<div class="blog_boxes" style="width:100%; float:left;">
		
			<?php
			$p_items = get_taxonomy_posts($b_id, $b_limit, "DESC", true);
			
			$i = 1;
			foreach ( $p_items as $post ) {
				setup_postdata($post);
				
				$post_thumbnail	= get_post_meta($post->ID, "_post_thumbnail", true);
				
				$thumb_type	= "";
				if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $post_thumbnail) ){
					$thumb_type	= "pic";
				}
				
				$post_fullsize		= get_post_meta($post->ID, "_post_fullsize", true);
				$post_short_desc	= get_post_meta($post->ID, "_short_description", true);
				
				preg_match_all("/(.*?)[\&\?]type=(.*?)\&target=(.*?)$/i", $post_fullsize, $matches);
				
				if( ( $blog_type == "style1" || $blog_type == "" ) && ( $sidebar_pos == "left" || $sidebar_pos == "right") ){
			
					$blog_content_css = "width:100%; float:left; clear:both;";
					$image_css = "max-width:690px;";
				
				} else if ( ( $blog_type == "style1" || $blog_type == "" ) && ( $sidebar_pos == "" || $sidebar_pos == "full") ){
				
					$blog_content_css = "width:100%; float:left; clear:both;";
					$image_css = "max-width:948px;";
					
				} else if($blog_type == "style2" && ( $sidebar_pos == "left" || $sidebar_pos == "right") && $post_thumbnail != ""){
				
					$blog_content_css = "width:460px; float:right;";
					$image_css = "max-width:200px;";
					
				} else if($blog_type == "style2" && ( $sidebar_pos == "" || $sidebar_pos == "full") && $post_thumbnail != "" ){
				
					$blog_content_css = "width:525px; float:right;";
					$image_css = "max-width:400px;";

				} else if ( $post_thumbnail == "" ){
					$blog_content_css = "width:100%; float:left;";
					$image_css = "";
				}
			?>
			<div class="blog_box">
				
				<?php if ($blog_type == "style1" || $blog_type == "") { ?>
				
				<div class="blog_title">
					
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				
				</div>
				
				<?php } ?>
				
				<div class="blog_image">
					
					<?php if ( $post_thumbnail != "" ) { ?>
					<div class="box_image_shadow">
					
						<div class="box_image_inside">
							
							<?php if( $thumb_type == "pic" ) { ?>
							
								<?php if ( $post_fullsize != "" && !$matches[2][0] == "link" ) { ?>
									<a href="<?php echo $post_fullsize; ?>" rel="prettyPhoto[<?php echo $b_id; ?>]">
								<?php } ?>
								
								<?php if ( $matches[2][0] == "link" ) { ?>
									
										<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>">
										
								<?php } ?>
								
								<img src="<?php echo $post_thumbnail; ?>" alt="<?php echo get_the_title(); ?>" style="<?php echo $image_css; ?>"/>
							
								<?php if ( $post_fullsize != "") { ?></a><?php } ?>
							
							<?php } else { ?>
							
								<div class="box_custom_thumbnail">
								<?php echo $post_thumbnail; ?>
								</div>
								
							<?php } ?>
							
						</div>
						
					</div>
					<?php } ?>
					
					
					<?php if ($blog_type == "style2" && $b_info == "on"){ ?>
					<div class="blog_tag">
						
						<?php
						if(get_the_tags()){ 
							echo "Tags:";
							the_tags('<ul><li>','</li><li>','</li></ul>'); 
						} 
						?>

					</div>
					<?php } ?>
					
				</div>
				
				<div class="blog_content" style="<?php echo $blog_content_css; ?>">
					
					
						<?php if ($blog_type == "style2") { ?>
						<div class="blog_title">
							
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						</div>
						<?php } ?>
						
				
						<?php if ( $b_info == "on" ) { ?>
						<div class="blog_info">
						
							<ul>
								<li><?php _e( 'by', 'fringe_tech' ); ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author() ?></a></li>
								<li><?php _e( 'on', 'fringe_tech' ); ?> <?php the_time('M j, Y'); ?></li>
								<?php if( get_the_tag_list('', ', ' ) != "" && $blog_type == "style1") {?>
								<li><?php printf( "Tags: %s", get_the_tag_list('', ', ' ) ); ?></li>
								<?php } ?>								
								<li><?php _e( 'in', 'fringe_tech' ); ?> <?php the_terms($post->ID, 'blog_type', '', ', ', ''); ?></li>
								<li><?php comments_popup_link("( No Comment )", "( One Comment )", '( % Comments )', 'commentslink', 'Off'); ?></li>
							</ul>
							
						</div>
						<?php } ?>
							
				
					
					<div class="blog_short_text">
					
						<div class="blog_content_text">
						
							<?php if ( $post_short_desc != "" ) { ?>			
							<div class="">
								<?php echo add_lightbox_rel ( do_shortcode ( htmlSafe ( $post_short_desc ) ) ); ?>
							</div>
							
							<h6>
							<a href="<?php the_permalink(); ?>">
								<span class="small_buttons">
									<span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_l small_left"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_r small_right">Continue Reading</span></span>
								</span>
							</a>
							</h6>
							
							<?php
								} else {
									global $more;
									$more = 0;
									the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'fringe_tech' ) );
								}
							?>
							
						</div>
					
						
						
					</div>

				</div>

			</div>
			
			<?php $i++; } ?>
		
		</div>
		
		<?php wp_pagenavi();  ?>

	</div>

	<?php if($class != "" && $class != "full") { ?>
	<div class="page_sidebar align<?php echo $sidebar_pos; ?>">
		
		<?php dynamic_sidebar( "shortcode_blog" ); ?>
		<?php get_sidebar(); ?>		

	</div>
<?php 
	}
	return ob_get_clean();
}
?>