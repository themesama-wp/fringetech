<?php
/* Start WordPress */
$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $parse_uri[0].'wp-load.php';
require_once($wp_load);

/*
	Getting & Settings some variables
*/
$name			= $_REQUEST["name"];
$email			= $_REQUEST["email"];
$tel			= $_REQUEST["tel"];
$subject		= $_REQUEST["subject"];
$content		= $_REQUEST["message"];

$address 		= get_option('site_form_email', get_bloginfo('admin_email'));
$sender			= mb_encode_mimeheader($name, "UTF-7", "Q") . " <" . $email . ">";
$subject_utf	= mb_encode_mimeheader($subject,"UTF-8", "B", "\n");


function sendMail($from, $to, $subject, $plain, $html = false, $charset = "iso-8859-1") {

	$cur_lb = get_option("email_set", "LF");
	
	if($cur_lb == "CRLF"){
		$lb = "\r\n";
	}else if($cur_lb == "LF"){
		$lb = "\n";
	}
	
	if($html == false) {

		$headers = "From: ".$from.$lb;
		$headers .= "Reply-To: ".$from.$lb;
		$headers .= "MIME-Version: 1.0".$lb;
		$headers .= "X-Priority: 3".$lb;
		$headers .= "X-Mailer: RobustMail [1.0]".$lb;
		$headers .= "Content-type: text/html; charset=".$charset.$lb;
		$mail_sent = @mail($to, $subject, $plain, $headers);
		return $mail_sent;

	}

	$random_hash = md5(microtime());
	$headers = "From: ".$from.$lb;
	$headers .= "Reply-To: ".$from.$lb;
	$headers .= "MIME-Version: 1.0".$lb;
	$headers .= "X-Priority: 3".$lb;
	$headers .= "X-Mailer: RobustMail [1.0]".$lb;
	$headers .= "Content-Type: multipart/alternative;".$lb."\tboundary=\"bo_".$random_hash."\"";

ob_start();

?>
--bo_<?php echo $random_hash.$lb; ?>
Content-Type: text/plain; charset = "<?php echo $charset; ?>"
Content-Transfer-Encoding: 7bit

<?php echo $plain; ?>


--bo_<?php echo $random_hash.$lb; ?>
Content-Type: text/html; charset = "<?php echo $charset; ?>"
Content-Transfer-Encoding: 7bit

<?php echo $html; ?>




--bo_<?php echo $random_hash; ?>--
<?php
	$message = ob_get_clean();
	$to = is_array($to) ? implode(", " , $to) : $to;
	$mail_sent = @mail($to, $subject, $message, $headers);
	return $mail_sent;
}

$mailBody	 = "";
$mailBody	.= get_option("mail_inbox_title", "You got an email from your website!")."<br /><br />";
$mailBody	.= get_option("field_name", "Name *")." : ". $name . "<br />";
$mailBody	.= get_option("field_email", "Email *")." : ". $email . "<br />"; 
$mailBody	.= get_option("field_telephone", "Telephone")." : ". $tel . "<br />"; 
$mailBody	.= get_option("field_subject", "Subject *")." : ". $subject . "<br />";
$mailBody	.= get_option("field_message", "Message *")." : ". $content . "<br />";

$send_mail = sendMail($sender, $address, $subject_utf, $mailBody, $html = false, $charset = "utf-8");

$thanks_message	= get_option("_thanks_message", "Success! Thank you for contact.");
$error_message	= get_option("_error_mail", "Mail Failed, Please try again!");

if( $send_mail ) { echo $thanks_message; } else { echo $error_message; }

?>