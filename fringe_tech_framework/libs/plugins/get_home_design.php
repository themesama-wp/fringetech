<?php

function get_home_design(){

	global $wpdb, $post;
	
	
	$w_value	= get_option("home_types");
	$model		= get_the_value("home_model", $w_value );

	ob_start();
?>
<!-- begin homebox wrapper -->
<div class="homebox_wrap">

<?php if( $model == "recently_and_boxes" || $model == "recently" ){ ?>
	<!-- 
		begin recently module
		v1.0 recently!
	-->
	<div class="recently">
		
		<?php 
			/* getting recently variables from value */
			$recently_ids		= get_the_value("wb_id", $w_value );
			$recently_all		= get_the_value("wb_all", $w_value );
			$recently_intro		= get_the_value("wb_recently_intro", $w_value );
			$recently_added		= get_the_value("wb_recently_added", $w_value );
			$recently_content	= get_the_value("wb_recently_content", $w_value );
			$wb_width			= get_the_value("wb_width", $w_value );
			$wb_height			= get_the_value("wb_height", $w_value );
			$wb_limit			= get_the_value("wb_limit", $w_value );
			$wb_column			= get_the_value("wb_column", $w_value );
			$wb_padding			= get_the_value("wb_padding", $w_value );

			
		?>
		
		<?php if ( $recently_intro == "on" ) {  ?>
		<!-- begin recently info -->
		<div class="intro">
		
			<div class="title"><h5><?php echo $recently_added; ?></h5></div>
			<div class="text"><?php echo $recently_content; ?></div>
		
		</div>
		<!-- end recently info -->
		<?php } ?>
	
		<?php if ( $recently_ids != "") { ?>
		
			<!-- begin items -->
			<div class="items" <?php if ( $recently_intro == "off" ) { echo "style=\"width:960px; margin-left:0;\""; } ?>>

				<!-- begin recently categories -->
				<div class="categories">
				
					<ul>
					
						<?php if ( $recently_all	==	"on" && $recently_ids != "") { ?>
						<li class="current_page_item"><a href="#"><?php _e("All", 'fringe_tech'); ?></a></li>
						<?php } ?>
						
						<?php
						/*
							Recently Categories'ler Ekrana UL LI i�ine yazd�r�l�yor.
							Category Link'i alma ��yle : <?php echo get_term_link( intval($category->term_id), $category->taxonomy); ?>
						*/
						$categories = get_taxonomy_name_by_id($recently_ids);
						$c = 0;
						foreach ( $categories as $category ) {
						?>
						<li <?php if ( $recently_all ==	"off" && $c == 0) { echo "class=\"current_page_item\""; } ?>><a href="#"><?php echo $category->name; ?></a></li>
						<?php $c++; } ?>

					</ul>
				
					<div class="categories_line"><div class="categories_cline">&nbsp;</div></div>
					
				</div>
				<!-- end recently categories -->

				
				<!-- begin recently boxes -->
				<div class="recently_boxes">
				
					<?php if ( $recently_all == "on" ) { ?>
					<ul class="first">
						
						<?php
						$allitems = get_taxonomy_posts($recently_ids, $wb_limit, "DESC", false);
						
						$i = 1;
						foreach ( $allitems as $post ) {
							setup_postdata($post);
							
							$post_thumbnail		= get_post_meta($post->ID, "_post_thumbnail", true);
							$thumb_type	= "";
							if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $post_thumbnail) ){
								$thumb_type	= "pic";
							}
							
							$post_fullsize		= get_post_meta($post->ID, "_post_fullsize", true);
							
							preg_match_all("/(.*?)[\&\?]type=(.*?)\&target=(.*?)$/i", $post_fullsize, $matches);
							
						?>
						<li <?php if($i == $wb_column) { echo " class=\"last\""; $i=0; } ?> style="margin-right:<?php echo $wb_padding; ?>px;">
						
							<div class="content" style="width:<?php echo $wb_width; ?>px;">							
							
								<div class="preview" style="height:<?php echo $wb_height; ?>px;">
									
									<div class="<?php if( $post_fullsize != "" && $matches[2][0] != "link" ) { ?>picture<?php } ?>">
									
										<div class="alpha">

											<?php if( $thumb_type == "pic" ) { ?>
											
												<?php if ( $post_fullsize != "" && !$matches[2][0] == "link" ) { ?>
													<a href="<?php echo $post_fullsize; ?>" rel="prettyPhoto[<?php echo $recently_ids; ?>]">
												<?php } ?>
												
												<?php if ( $matches[2][0] == "link" ) { ?>
												
													<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>">
													
												<?php } ?>
												
												<img src="<?php echo $post_thumbnail; ?>" alt="<?php echo $post->post_title; ?>" style="width:<?php echo $wb_width; ?>px; height:<?php echo $wb_height; ?>px;"/>
											
												<?php if ( $post_fullsize != "") { ?></a><?php } ?>
											
											<?php } else { ?>
											
												<?php echo $post_thumbnail; ?>
											
											<?php } ?>	
										
										</div>
										
										<span class="date"><?php the_time('M j, Y'); ?></span>
										<span class="comment"><?php comments_popup_link("0", "1", '% C', 'commentslink', 'Off'); ?></span>
									</div>
									
								</div>
								
								<div class="title">
								
									<?php if ($matches[2][0] == "link" ) { ?>

									<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>"><?php the_title(); ?></a>
									
									<?php } else { ?>
							
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									
									<?php } ?>
									
								</div>
							
							</div>
							
						</li>
						<?php $i++; } ?>
		
					</ul>
					<?php } ?>				
					
					<?php 
						
						foreach ( $categories as $sub_cat ) {
						
						$tax	= $sub_cat->taxonomy;
						$s_ids	= $sub_cat->term_id;

					?>
					<ul <?php if ( $recently_all ==	"off" && $b == 0) { echo "class=\"first\""; } ?>>
						<?php 
						$get_totalposts = get_taxonomy_posts($s_ids, $wb_limit, "DESC", false);
						
						$b = 1;
						foreach ( $get_totalposts as $post ) {
							
							setup_postdata($post);
							
							$post_thumbnail		= get_post_meta($post->ID, "_post_thumbnail", true);
							$thumb_type	= "";
							if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $post_thumbnail) ){
								$thumb_type	= "pic";
							}
							
							$post_fullsize		= get_post_meta($post->ID, "_post_fullsize", true);
							
							preg_match_all("/(.*?)[\&\?]type=(.*?)\&target=(.*?)$/i", $post_fullsize, $matches);
							
						?>
						<li <?php if($b == $wb_column) { echo " class=\"last\""; $b=0; } ?> style="margin-right:<?php echo $wb_padding; ?>px;">
						
 							<div class="content" style="width:<?php echo $wb_width; ?>px;">
							
								<div class="preview" style="height:<?php echo $wb_height; ?>px;">
									
									<div class="<?php if( $post_fullsize != "" && $matches[2][0] != "link" ) { ?>picture<?php } ?>">
										
										<div class="alpha">

											<?php if( $thumb_type == "pic" ) { ?>
											
												<?php if ( $post_fullsize != "" && !$matches[2][0] == "link" ) { ?>
													<a href="<?php echo $post_fullsize; ?>" rel="prettyPhoto[<?php echo $recently_ids; ?>]">
												<?php } ?>
												
												<?php if ( $matches[2][0] == "link" ) { ?>
												
													<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>">
													
												<?php } ?>
												
												<img src="<?php echo $post_thumbnail; ?>" alt="<?php echo $post->post_title; ?>" style="width:<?php echo $wb_width; ?>px; height:<?php echo $wb_height; ?>px;"/>
											
												<?php if ( $post_fullsize != "") { ?></a><?php } ?>
											
											<?php } else { ?>
											
												<?php echo $post_thumbnail; ?>
											
											<?php } ?>
	
										</div>
										
										<span class="date"><?php the_time('M j, Y'); ?></span>
										<span class="comment"><?php comments_popup_link("0", "1", '% C', 'commentslink', 'Off'); ?></span>
									</div>
									
								</div>
								
								<div class="title">
								
									<?php if ($matches[2][0] == "link" ) { ?>

									<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>"><?php the_title(); ?></a>
									
									<?php } else { ?>
							
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									
									<?php } ?>
									
								</div>
							
							</div>
							
						</li>
					
						<?php $b++; } ?>
						
					</ul>
					<?php } ?>
					
				</div>
				<!-- end recently boxes -->

			</div>
			<!-- begin items -->
		
		<?php } else  { ?>
			Please, choose your settings for recently module from <a href="wp-admin/admin.php?page=home_design">Home Design Manager</a>( Categories, Items, Text, etc... )
		<?php } ?>
	
	</div>
	<!-- end recently -->
	<?php } ?>

	<?php if( $model == "recently_and_boxes" || $model == "boxes" ){ ?>
	
	<?php 
		$w_limit		= get_the_value("w_limit", $w_value );
		$w_width		= get_the_value("w_width", $w_value );
		$w_height		= get_the_value("w_height", $w_value );
		$w_column		= get_the_value("w_column", $w_value );
		$w_padding		= get_the_value("w_padding", $w_value );
	?>
	<div class="home_cboxes">
		
		<?php
			$f = 1;
			$c = 1;
			for($kval = 1; $kval < $w_limit+1; $kval++){
				$b_values	= get_option("home_box_".$kval);
				$b_name		= get_the_value("w_name", $b_values );
				$b_desc		= get_the_value("w_desc", $b_values );				
				$b_icon		= get_the_value("w_icon", $b_values );

		?>
		<?php if($f == $w_column+1 ) { echo "<div class=\"clearcboxes\"></div>";  $f = 1;  } ?>

		<div class="one_cbox <?php if($c == $w_column) { echo "last"; $c = 0; }?>" style="width:<?php echo $w_width; ?>px; margin-right:<?php echo $w_padding; ?>px;">
			
			<?php if ( $b_name != "" ){ ?>
			<div class="title" style="width:<?php echo $w_width; ?>px;">
				<?php if( $b_icon != "" ) { ?><span class="icon <?php echo $b_icon; ?>"></span><?php } ?>
				<h5><?php echo do_shortcode ( htmlSafe ( $b_name ) ); ?></h5>
			</div>
			<?php } ?>
			
			<?php if ( $b_desc != "" ){ ?>
			<div class="text"><?php echo add_lightbox_rel ( do_shortcode ( htmlSafe ( $b_desc ) ) ); ?></div>
			<?php } ?>
			
			
		</div>

		<?php $f++; $c++; } ?>
		
	</div>
	<?php } ?>
	
	
	<?php if( $model == "custom" ){ 
		$w_value	= get_option("home_custom");
		$w_name		= get_the_value("w_name", $w_value );
		$w_desc		= get_the_value("w_desc", $w_value );
	?>
	<div class="home_custom">
		
		<?php if ( $w_name != "" ) {  ?>
		<div class="home_custom_title">
			<h5 class="italic"><?php echo $w_name; ?></h5>
		</div>
		<?php } ?>
		
		<div class="home_custom_content">
			<?php echo add_lightbox_rel ( do_shortcode ( htmlSafe ( $w_desc ) ) ); ?>
		</div>
	</div>
	<?php } ?>
	

</div>
<!-- end homebox wrapper -->

<?php
	return ob_get_clean();
}
?>