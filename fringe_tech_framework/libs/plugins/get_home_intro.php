<?php 
function get_home_intro(){
ob_start();
if ( get_option("home_page_intro_active", "on") == "on" ) { 
?>
<!-- begin intro -->
<div class="intro_text">
	
	<div class="intro_content">
	
		<?php echo add_lightbox_rel ( do_shortcode(get_option('home_page_intro', '<h4 style="text-align: center;">WordPress is web software you can use to create a beautiful website or blog. We like to say that WordPress is both free and priceless at the same time.</h4>') ) ); ?>
	
	</div>
	
</div>
<!-- begin intro -->
<?php
	}
	return ob_get_clean();
}
?>