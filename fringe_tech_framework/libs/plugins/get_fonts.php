<?php

function get_font($font){
	$font = preg_replace( array("/:(.*)/", "/\+/"), array("", " "), $font);
	return $font;
}

function google_fonts(){
	
	$families	= array();
	$no_google_fonts	= array("Georgia", "Arial", "Verdana", "Tahoma", "Trebuchet MS", "Times New Roman", "sans-serif");

	$h_font_family = get_clean_option( "h_font_family", "Georgia" );
	$g_font_family = get_clean_option( "general_font_family", "Tahoma" );
	$m_font_family = get_clean_option( "main_menu_font_family", "Tahoma" );
	$d_font_family = get_clean_option( "drop_down_font_family", "Tahoma" );
	
	if( in_array ( $h_font_family, $no_google_fonts ) && in_array ( $g_font_family, $no_google_fonts ) && in_array ( $m_font_family, $no_google_fonts ) && in_array ( $d_font_family, $no_google_fonts ) ) {
		return false;
	}
	
	if( !in_array ( $h_font_family, $no_google_fonts ) ) {
		$families[] = "'".str_replace("|", ",", $h_font_family)."'";
	}
	
	if( !in_array ( $g_font_family, $no_google_fonts ) ) {
		$families[] = "'".str_replace("|", ",", $g_font_family)."'";
	}
	
	if( !in_array ( $m_font_family, $no_google_fonts ) ) {
		$families[] = "'".str_replace("|", ",", $m_font_family)."'";
	}
	
	if( !in_array ( $d_font_family, $no_google_fonts ) ) {
		$families[] = "'".str_replace("|", ",", $d_font_family)."'";
	}

	
	
	$familys = implode(",", $families);
	
	ob_start();
?>
<script type="text/javascript">

jQuery(document).ready(function() {

	WebFontConfig = {
	google: { families: [ <?php echo $familys; ?> ] },
	fontactive: function(){
		jQuery('.header_menu').codeline();
		jQuery(".categories").find("li.current").trigger("line_trig");
	}
	};
	(function() {
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
	})();

});
</script>
<?php
	$content = ob_get_clean();
	echo $content;
}
?>