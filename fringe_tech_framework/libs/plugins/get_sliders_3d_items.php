<?php
/* Start WordPress */
$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $parse_uri[0].'wp-load.php';
require_once($wp_load);
	
$s_id = @$_REQUEST["s_id"];
	
global $wpdb, $item_table, $category_table;

$get_slider_items	= $wpdb->get_results("SELECT * FROM " . $item_table . " WHERE c_id = '" . $s_id . "' ORDER BY orderby DESC");
$slider_count		= $wpdb->get_var($wpdb->prepare("SELECT COUNT(id) FROM " . $item_table . " WHERE c_id = %d", $s_id));
$slider_settings	= $wpdb->get_row($wpdb->prepare("SELECT * FROM " . $category_table . " WHERE id = %d", $s_id));
	
$s_shadow			= get_the_value( "s_shadow", htmlSafe( $slider_settings->c_value ) );
$s_width			= get_the_value( "s_width", htmlSafe( $slider_settings->c_value ) );
$s_height			= get_the_value( "s_height", htmlSafe( $slider_settings->c_value ) );
$s_delay			= get_the_value( "s_delay", htmlSafe( $slider_settings->c_value ) );
$s_autoplay			= get_the_value( "s_autoplay", htmlSafe( $slider_settings->c_value ) );

if( $s_autoplay == "off") { $s_delay = 0; }
if( $s_shadow 	== "off") { $shadow = 0; } else { $shadow = 0.5; }

?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<Piecemaker>
  <Contents>
	<?php
	foreach($get_slider_items as $item){ 
	$s_picture	= get_the_value( "s_picture", htmlSafe( $item->s_value) );
	$s_name		= $item->s_name;
	$s_desc		= $item->s_desc;
	$s_link		= get_the_value( "s_link", htmlSafe( $item->s_value) );
	$s_target	= get_the_value( "s_target", htmlSafe( $item->s_value) );
	?>
	
		<?php if ( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $s_picture ) ) { ?>
		<Image Source="<?php echo $s_picture; ?>" Title="<?php echo $s_name; ?>">
			<?php if ( $s_desc != "" ) { ?>
			<Text><h1><?php echo $s_name; ?></h1><p><?php echo $s_desc; ?></p></Text>
			<?php } ?>
			<?php if($s_link != ""){ ?>
			<Hyperlink URL="<?php echo $s_link; ?>" Target="<?php echo $s_target; ?>" />
			<?php } ?>
		</Image>
		<?php } ?>
		
		<?php if ( preg_match('/mp4$|avi$|flv$/', $s_picture ) ) { ?>
		<Video Source="<?php echo $s_picture; ?>" Title="<?php echo $s_name; ?>" Width="<?php echo $s_width; ?>" Height="<?php echo $s_height; ?>" Autoplay="true">
			<Image Source="<?php echo T_URI."/images/blank.gif"; ?>"/>
		</Video>
		<?php } ?>

		<?php if ( preg_match('/swf$/', $s_picture ) ) { ?>
		<Flash Source="<?php echo $s_picture; ?>" Title="<?php echo $s_name; ?>">
			<Image Source="<?php echo T_URI."/images/blank.gif"; ?>"/>
		</Flash>
		<?php } ?>

	<?php } ?>
  </Contents>
  <Settings ImageWidth="<?php echo $s_width; ?>" ImageHeight="<?php echo $s_height; ?>" LoaderColor="0x333333" InnerSideColor="0x222222" SideShadowAlpha="0.8" DropShadowAlpha="<?php echo $shadow; ?>" DropShadowDistance="20" DropShadowScale="0.8" DropShadowBlurX="40" DropShadowBlurY="4" MenuDistanceX="20" MenuDistanceY="50" MenuColor1="0x999999" MenuColor2="0x333333" MenuColor3="0xFFFFFF" ControlSize="50" ControlDistance="20" ControlColor1="0x222222" ControlColor2="0xFFFFFF" ControlAlpha="0.8" ControlAlphaOver="0.95" ControlsX="<?php echo ($s_width/2); ?>" ControlsY="<?php echo ($s_height - 50); ?>" ControlsAlign="center" TooltipHeight="30" TooltipColor="0x222222" TooltipTextY="5" TooltipTextStyle="P-Italic" TooltipTextColor="0xFFFFFF" TooltipMarginLeft="5" TooltipMarginRight="7" TooltipTextSharpness="50" TooltipTextThickness="-<?php echo ($s_width/2); ?>" InfoWidth="<?php echo ($s_width/2); ?>" InfoBackground="0xFFFFFF" InfoBackgroundAlpha="0.95" InfoMargin="30" InfoSharpness="0" InfoThickness="0" Autoplay="<?php echo $s_delay; ?>" FieldOfView="45"></Settings>
  <Transitions>
    <Transition Pieces="9" Time="1.2" Transition="easeInOutBack" Delay="0.1" DepthOffset="300" CubeDistance="30"></Transition>
    <Transition Pieces="15" Time="3" Transition="easeInOutElastic" Delay="0.03" DepthOffset="200" CubeDistance="10"></Transition>
    <Transition Pieces="5" Time="1.3" Transition="easeInOutCubic" Delay="0.1" DepthOffset="500" CubeDistance="50"></Transition>
    <Transition Pieces="9" Time="1.25" Transition="easeInOutBack" Delay="0.1" DepthOffset="960" CubeDistance="5"></Transition>
  </Transitions>
</Piecemaker>