<?php
function get_slider($s_id){
	
	
	
	global $wpdb, $item_table, $category_table;
	
	$get_slider_items	= $wpdb->get_results("SELECT * FROM " . $item_table . " WHERE c_id = '" . $s_id . "' ORDER BY orderby DESC");
	$slider_count		= $wpdb->get_var($wpdb->prepare("SELECT COUNT(id) FROM " . $item_table . " WHERE c_id = %d", $s_id));
	$slider_settings	= $wpdb->get_row($wpdb->prepare("SELECT * FROM " . $category_table . " WHERE id = %d", $s_id));
	
	if( $slider_settings->s_id == 1) {
		return creating_slider_1($s_id, $slider_count, $get_slider_items, $slider_settings);
	}else if( $slider_settings->s_id == 2) {
		return creating_slider_2($s_id, $slider_count, $get_slider_items, $slider_settings);
	}else if( $slider_settings->s_id == 3) {
		return creating_slider_3($s_id, $slider_count, $get_slider_items, $slider_settings);
	}else if( $slider_settings->s_id == 4) {
		return creating_slider_4($s_id, $slider_count, $get_slider_items, $slider_settings);
	}else if( get_option("home_page_slider_active", "on") != "off" ){
		echo no_slide();
	}

}

function no_slide() {
?>
<div class="no_slide">

	<img src="<?php echo T_URI; ?>/images/dummy/slider.png" alt="" />
	<div class="full_shadow">&nbsp;</div>

</div>
<?php
}

/* Custom Slider Creating*/
function creating_slider_1($s_id, $slider_count, $get_slider_items, $slider_settings){
	
	$s_arrows			= get_the_value( "s_arrows", htmlSafe( $slider_settings->c_value ) );
	$s_buttons			= get_the_value( "s_buttons", htmlSafe( $slider_settings->c_value ) );
	$s_shadow			= get_the_value( "s_shadow", htmlSafe( $slider_settings->c_value ) );
	$s_width			= get_the_value( "s_width", htmlSafe( $slider_settings->c_value ) );
	$s_height			= get_the_value( "s_height", htmlSafe( $slider_settings->c_value ) );
	$s_line				= get_the_value( "s_line", htmlSafe( $slider_settings->c_value ) );
	$s_delay			= get_the_value( "s_delay", htmlSafe( $slider_settings->c_value ) );
	$s_autoplay			= get_the_value( "s_autoplay", htmlSafe( $slider_settings->c_value ) );
	$s_padding_top		= get_the_value( "s_padding_top", htmlSafe( $slider_settings->c_value ) );
	$s_padding_bottom	= get_the_value( "s_padding_bottom", htmlSafe( $slider_settings->c_value ) );
	$s_position			= get_clean_option ( get_the_value( "s_position", htmlSafe( $slider_settings->c_value ) ) );
	
	$slider_padding = 0;

	if( $s_autoplay == "on"){ $autoplay = "true"; } else { $autoplay = "false"; }
	if( $slider_count > 1 && $s_buttons == "on") { $buttons = "true"; $slider_padding = $slider_padding + 25; } else { $buttons = "false"; }
	if( $s_line == "on") { $slider_padding = $slider_padding + 10; }
	
	ob_start();
?>
	
<?php if( $slider_count > 0	 ) { ?>
<div><script type="text/javascript">jQuery(document).ready(function() { jQuery('.codeslide_<?php echo $s_id; ?>').codeslide({'autoplay': '<?php echo $autoplay; ?>', 'speed': <?php echo $s_delay*1000; ?>, 'buttons': '<?php echo $buttons; ?>'});}); </script></div>
<div class="slider_container_<?php echo $s_id; ?>" style="clear:both; width:<?php echo $s_width; ?>px; height:<?php echo $s_height+$slider_padding; ?>px; <?php if( $s_line == "on" ) { ?> border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>; <?php } ?> <?php if( $s_position == "center") { ?> margin-left: auto; margin-right: auto; <?php } else { ?> float: <?php echo $s_position; ?>; <?php } ?> margin-top: <?php echo $s_padding_top; ?>px; margin-bottom: <?php echo $s_padding_bottom; ?>px;">
<div class="codeslide codeslide_<?php echo $s_id; ?>" style="width:<?php echo $s_width; ?>px; height:<?php echo $s_height; ?>px;">
<ul>
<?php 
foreach($get_slider_items as $item){ 

$s_picture	= get_the_value( "s_picture", htmlSafe( $item->s_value) );
$s_name		= $item->s_name;
$s_link		= get_the_value( "s_link", htmlSafe( $item->s_value) );
$s_target	= get_the_value( "s_target", htmlSafe( $item->s_value) );

?>
<li><?php if ( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $s_picture ) ) { ?><?php if($s_link != ""){ echo '<a href="'.$s_link.'" target="'.$s_target.'">'; } ?><img src="<?php echo $s_picture; ?>" alt="<?php echo $s_name; ?>" width="<?php echo $s_width; ?>" height="<?php echo $s_height; ?>"/><?php if($s_link != ""){ echo '</a>'; } ?><?php } else { echo $s_picture; } ?></li>
<?php } ?>
</ul>
<?php if( $slider_count > 1	&& $s_arrows == "on") { ?>
<div><div class="prev">prev</div><div class="next">next</div></div>
<?php } ?>
</div>
<?php if( $s_shadow == "on" ) { ?>
<div class="full_shadow"></div>
<?php } ?>
<div class="nums" style="width:<?php echo $slider_count * 17; ?>px; margin-left:<?php echo (($s_width / 2) - ( $slider_count * 17 ) / 2); ?>px;">
<ul>
<?php foreach($get_slider_items as $item){ ?>
<li></li>
<?php } ?>
</ul></div></div>
<?php } else { ?>
	<center>There is not any slider, Please add a slider! :)</center>
<?php 
	}
	
	return ob_get_clean();
}

/* Nivo Slider Parsing */
function creating_slider_2($s_id, $slider_count, $get_slider_items, $slider_settings){


	$s_arrows			= get_the_value( "s_arrows", htmlSafe( $slider_settings->c_value ) );
	$s_buttons			= get_the_value( "s_buttons", htmlSafe( $slider_settings->c_value ) );
	$s_shadow			= get_the_value( "s_shadow", htmlSafe( $slider_settings->c_value ) );
	$s_width			= get_the_value( "s_width", htmlSafe( $slider_settings->c_value ) );
	$s_height			= get_the_value( "s_height", htmlSafe( $slider_settings->c_value ) );
	$s_line				= get_the_value( "s_line", htmlSafe( $slider_settings->c_value ) );
	$s_delay			= get_the_value( "s_delay", htmlSafe( $slider_settings->c_value ) );
	$s_autoplay			= get_the_value( "s_autoplay", htmlSafe( $slider_settings->c_value ) );
	$s_padding_top		= get_the_value( "s_padding_top", htmlSafe( $slider_settings->c_value ) );
	$s_padding_bottom	= get_the_value( "s_padding_bottom", htmlSafe( $slider_settings->c_value ) );
	$s_position			= get_clean_option ( get_the_value( "s_position", htmlSafe( $slider_settings->c_value ) ) );
	$s_effect			= get_clean_option ( get_the_value( "s_effect", htmlSafe( $slider_settings->c_value ) ) );
	$s_slices			= get_the_value( "s_slices", htmlSafe( $slider_settings->c_value ) );
	$s_pause			= get_the_value( "s_pause", htmlSafe( $slider_settings->c_value ) );
	$s_title_color		= get_the_value( "s_title_color", htmlSafe( $slider_settings->c_value ) );
	$s_title_bg_color	= get_the_value( "s_title_bg_color", htmlSafe( $slider_settings->c_value ) );
	$s_title_size		= get_the_value( "s_title_size", htmlSafe( $slider_settings->c_value ) );
	
	$s_desc_color		= get_the_value( "s_desc_color", htmlSafe( $slider_settings->c_value ) );
	$s_desc_bg_color	= get_the_value( "s_desc_bg_color", htmlSafe( $slider_settings->c_value ) );
	$s_desc_size		= get_the_value( "s_desc_size", htmlSafe( $slider_settings->c_value ) );
	
	
	$slider_padding = 0;

	
	if( $s_buttons	== "on"){	$buttons	= "true"; } else { $buttons	= "false"; }
	if( $s_arrows	== "on"){	$arrows		= "true"; } else { $arrows	= "false"; }
	if( $s_pause	== "on"){	$pause		= "true"; } else { $pause	= "false"; }
	if( $s_autoplay	== "off"){	$s_delay	= 999; }
	
	$nav_width = $slider_count * 17;
	$nav_margin_left = (($s_width / 2) - ( $slider_count * 17 ) / 2);

	if( $slider_count > 1 && $s_buttons == "on") { $slider_padding = $slider_padding + 25; } else { $buttons	= "false"; }
	if( $s_line == "on") { $slider_padding = $slider_padding + 20; }

	ob_start();
?>
<?php if( $slider_count > 0	 ) { ?>
<div><script type="text/javascript">jQuery(document).ready(function() { jQuery(window).load(function() { jQuery('#slider_<?php echo $s_id; ?>').nivoSlider({controlNav:<?php echo $buttons; ?>, directionNav:<?php echo $arrows; ?>, pauseTime:<?php echo $s_delay*1000; ?>, controlNavW:<?php echo $nav_width; ?>, controlNavP:<?php echo $nav_margin_left; ?>, effect:'<?php echo $s_effect; ?>', slices:<?php echo $s_slices; ?>, pauseOnHover:<?php echo $pause; ?>}); }); });</script></div>
<div style="clear:both; width:<?php echo $s_width; ?>px; height:<?php echo $s_height+$slider_padding; ?>px; <?php if( $s_line == "on" ) { ?> border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>; <?php } ?> <?php if( $s_position == "center") { ?> margin-left: auto; margin-right: auto; <?php } else { ?> float: <?php echo $s_position; ?>; <?php } ?> margin-top: <?php echo $s_padding_top; ?>px; margin-bottom: <?php echo $s_padding_bottom; ?>px;">
<div id="slider_<?php echo $s_id; ?>" class="nivoShow nivoSlider" style="clear:both; width:<?php echo $s_width; ?>px; height:<?php echo $s_height; ?>px;"><?php
foreach($get_slider_items as $item){ 
$s_picture	= get_the_value( "s_picture", htmlSafe( $item->s_value) );
$s_name		= htmlSafe( $item->s_name );
$s_desc		= htmlSafe( $item->s_desc );
$s_link		= get_the_value( "s_link", htmlSafe( $item->s_value) );
$s_target	= get_the_value( "s_target", htmlSafe( $item->s_value) );
?><?php if($s_link != ""){ echo '<a href="'.$s_link.'" target="'.$s_target.'" class="nivoLink">'; } else { echo "<div>"; } ?><img src="<?php echo $s_picture; ?>" alt="<?php echo $s_name; ?>" width="<?php echo $s_width; ?>" height="<?php echo $s_height; ?>"/><?php if ( $s_name != "" || $s_desc != "" ) { ?><span class="holder_desc"><?php if ( $s_name != "" ) { ?><span class="ntitle" style="font-size:<?php echo $s_title_size; ?>px; color:<?php echo $s_title_color; ?>; background-color:<?php echo $s_title_bg_color; ?>;"><?php echo $s_name; ?></span><?php } ?><?php if ( $s_desc != "" ) { ?><span class="ndescription" style="font-size:<?php echo $s_desc_size; ?>px; color:<?php echo $s_desc_color; ?>; background-color:<?php echo $s_desc_bg_color; ?>;"><?php echo $s_desc; ?></span><?php } ?></span><?php } ?><?php if($s_link != ""){ echo '</a>'; } else { echo "</div>"; } ?><?php } ?></div><div class="hide_desc"></div><?php if( $s_shadow == "on" ) { ?><div class="full_shadow">&nbsp;</div><?php } ?></div><?php } else { ?>
	<center>There is not any slider, Please add a slider! :)</center>
<?php 
	}
	
	return ob_get_clean();
}

/* 3D Slider Parsing */
function creating_slider_3($s_id, $slider_count, $get_slider_items, $slider_settings){

	$s_buttons			= get_the_value( "s_buttons", htmlSafe( $slider_settings->c_value ) );
	$s_width			= get_the_value( "s_width", htmlSafe( $slider_settings->c_value ) );
	$s_height			= get_the_value( "s_height", htmlSafe( $slider_settings->c_value ) );
	$s_line				= get_the_value( "s_line", htmlSafe( $slider_settings->c_value ) );
	$s_padding_top		= get_the_value( "s_padding_top", htmlSafe( $slider_settings->c_value ) );
	$s_padding_bottom	= get_the_value( "s_padding_bottom", htmlSafe( $slider_settings->c_value ) );
	$s_position			= get_clean_option ( get_the_value( "s_position", htmlSafe( $slider_settings->c_value ) ) );

	$slider_padding 	= 0;
	if( $s_buttons == "on") { $slider_padding = $slider_padding + 70; }
	if( $s_line	== "on") { $slider_padding = $slider_padding + 20; }
	
	ob_start();
?>
<div style="clear:both; width:<?php echo $s_width; ?>px; height:<?php echo $s_height+$slider_padding; ?>px; <?php if( $s_line == "on" ) { ?> border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>; <?php } ?> <?php if( $s_position == "center") { ?> margin-left: auto; margin-right: auto; <?php } else { ?> float: <?php echo $s_position; ?>; <?php } ?> margin-top: <?php echo $s_padding_top; ?>px; margin-bottom: <?php echo $s_padding_bottom; ?>px;">
<div><script type="text/javascript">
var tag = new FlashTag('<?php echo T_URI; ?>/images/swf/piecemaker.swf', <?php echo $s_width; ?>, <?php echo $s_height+$slider_padding; ?>, '9,0,0');
tag.setFlashvars('xmlSource=<?php echo T_URI."/".F_PATH; ?>/libs/plugins/get_sliders_3d_items.php?s_id=<?php echo $s_id; ?>&amp;cssSource=<?php echo T_URI; ?>/css/piecemaker.css');
tag.setId('myFlashContent');
tag.setBgcolor('000000');
tag.write(document);
</script></div></div>
<?php 
	return ob_get_clean();
} 


/* Kwicks Slider Creating*/
function creating_slider_4($s_id, $slider_count, $get_slider_items, $slider_settings){
	
	$s_width			= get_the_value( "s_width", htmlSafe( $slider_settings->c_value ) );
	$s_desc_height		= get_the_value( "s_desc_height", htmlSafe( $slider_settings->c_value ) );
	$s_height			= get_the_value( "s_height", htmlSafe( $slider_settings->c_value ) );
	$s_shadow			= get_the_value( "s_shadow", htmlSafe( $slider_settings->c_value ) );
	$s_line				= get_the_value( "s_line", htmlSafe( $slider_settings->c_value ) );
	$s_padding_top		= get_the_value( "s_padding_top", htmlSafe( $slider_settings->c_value ) );
	$s_padding_bottom	= get_the_value( "s_padding_bottom", htmlSafe( $slider_settings->c_value ) );
	$s_position			= get_clean_option ( get_the_value( "s_position", htmlSafe( $slider_settings->c_value ) ) );
	$s_title_color		= get_the_value( "s_title_color", htmlSafe( $slider_settings->c_value ) );
	$s_desc_color		= get_the_value( "s_desc_color", htmlSafe( $slider_settings->c_value ) );
	$s_desc_bg_color	= get_the_value( "s_desc_bg_color", htmlSafe( $slider_settings->c_value ) );
	
	$slider_padding = 0;

	if( $s_shadow == "on") {	$slider_padding = $slider_padding + 10; }
	if( $s_line == "on") { $slider_padding = $slider_padding + 10; }
	

	ob_start();
?>	
<?php if( $slider_count > 0	 ) { ?>
<div><script type="text/javascript">jQuery(document).ready(function() {	jQuery('.kwicks_<?php echo $s_id; ?>').kwicks({max : <?php echo ( $s_width - ($s_width/$slider_count) ); ?>}); }); </script></div>
<div class="slider_container_<?php echo $s_id; ?>" style="clear:both; width:<?php echo $s_width; ?>px; height:<?php echo $s_height+$slider_padding; ?>px; <?php if( $s_line == "on" ) { ?> border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>; <?php } ?> <?php if( $s_position == "center") { ?> margin-left: auto; margin-right: auto; <?php } else { ?> float: <?php echo $s_position; ?>; <?php } ?> margin-top: <?php echo $s_padding_top; ?>px; margin-bottom: <?php echo $s_padding_bottom; ?>px;">
<ul class="kwicks kwicks_<?php echo $s_id; ?>" style="width:<?php echo $s_width; ?>px !important; height:<?php echo $s_height; ?>px; ">
<?php
$i = 0;
foreach($get_slider_items as $item){ 

$s_short_name	= get_the_value( "s_short_name", htmlSafe( $item->s_value) );
$s_picture		= get_the_value( "s_picture", htmlSafe( $item->s_value) );
$s_name			= $item->s_name;
$s_desc			= $item->s_desc;
$s_link			= get_the_value( "s_link", htmlSafe( $item->s_value) );
$s_target		= get_the_value( "s_target", htmlSafe( $item->s_value) );

?>
<li class="kwick <?php if ( $i == ($slider_count-1) ) { echo "last"; } ?>" style="width:<?php echo $s_width/$slider_count; ?>px; height:<?php echo $s_height; ?>px;">
<div class="kwick_shadow"></div>
<?php if ( $s_short_name != "" ) { ?>
<div class="kwick_title" style="color:<?php echo $s_title_color; ?>; background-color:<?php echo $s_desc_bg_color; ?>;"><?php echo $s_short_name; ?></div>
<?php } ?>
<div><?php if($s_link != ""){ ?><a href="<?php echo $s_link; ?>" target="<?php echo $s_target; ?>"><?php } ?><?php if ( $s_name != "" || $s_desc != "" ) { ?><span class="kwick_desc" style="width:<?php echo ( $s_width - ($s_width/$slider_count) - 30 ); ?>px; background-color:<?php echo $s_desc_bg_color; ?>;"><span class="kwick_desc_title" style="color:<?php echo $s_title_color; ?>;"><?php echo $s_name; ?></span><span style="color:<?php echo $s_desc_color; ?>;"><?php echo $s_desc; ?></span></span><?php } ?><span><img src="<?php echo $s_picture; ?>" alt="<?php echo $s_name; ?>" /></span><?php if($s_link != ""){ ?></a><?php } ?></div>
</li>
<?php $i++; } ?>
</ul>	
<div class="full_shadow">&nbsp;</div>
</div>
<?php } else { ?>
	<center>There is not any slider, Please add a slider! :)</center>
<?php 
	}
	return ob_get_clean();
}
?>