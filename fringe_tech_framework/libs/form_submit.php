<?php
/*
	CP Framework update & delete option method
*/

$action = @$_REQUEST["action"];

$request_options = @$_REQUEST["options"];

if( $request_options != "" ) {

	switch($action){

		case 'save':
			
			foreach($request_options as $key => $option) {
				
				if(!is_array($option)){
					
					update_option( $key, stripslashes($option) );
					
					
				}else{
					foreach($request_options[$key] as $option_name => $op) {
					
						$values = array();
						
						foreach($op as $k => $v){
							if($v != ""){
								$values[] = $v;
							}
						}
						
						$values = implode(",", $values);
						update_option( $option_name, stripslashes($values) );
						
					}
				}
			}
		break;
		
		
		case 'reset':

			foreach($request_options as $key => $option) {
			
				if(!is_array($option)){
					delete_option($key);
				}else{
					foreach($request_options[$key] as $option_name => $op) {
						delete_option($option_name);
					}
				}
			}
			
		break;	
	}

}
?>