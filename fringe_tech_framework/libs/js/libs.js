function make_slider_ui($name, $value, $max, $step){
	

	jQuery(document).ready(function() {
		
		
		var val = parseInt($value);
		var maxi = parseInt($max);
		var stepi = parseInt($step);

		jQuery( "#slider_ui_"+$name ).slider({
			range: "min",
			min: 0,
			step: stepi,
			max: maxi,
			value: val,
			slide: function( event, ui ) {
				jQuery( ".slider_ui_print_"+$name ).text(ui.value + "px");
				jQuery( ".slider_ui_size_"+$name ).css({"font-size": ui.value + "px", "line-height": ui.value + "px"});
				jQuery( ".slider_ui_name_"+$name ).val( ui.value );
				jQuery( ".slider_ui_alpha_"+$name ).css({opacity: (ui.value/100)});
	
			}
		});

		jQuery( ".slider_ui_alpha_"+$name ).css({opacity: ($value/100)});

		jQuery( ".slider_ui_name_"+$name ).bind("keyup trig_slider", function(){
			current_value = parseInt(jQuery(this).val());
			jQuery( "#slider_ui_"+$name ).slider( "option", "value", current_value );
			jQuery( ".slider_ui_size_"+$name ).css({"font-size": current_value + "px", "line-height": current_value + "px"});
			jQuery( ".slider_ui_alpha_"+$name ).css({opacity: (current_value/100)});
		});
		
	});
}

jQuery(document).ready(function() {

	jQuery(".checkbox_all").click(function(){
		jQuery(this).parent().find("input[type=checkbox]").each(function(){
			jQuery(this).attr("checked", true);
		});
	});	
	
	jQuery(".uncheckbox_all").click(function(){
		jQuery(this).parent().find("input[type=checkbox]").each(function(){
			jQuery(this).attr("checked", false);
		});
	});	
	
	jQuery(".select_all").click(function(){
		jQuery(this).parent().prev().find("option[value!=]").each(function(){
			jQuery(this).attr("selected", true);
		});
	});
	
	jQuery(".unselect_all").click(function(){
		jQuery(this).parent().prev().find("option[value!=]").each(function(){
			jQuery(this).attr("selected", false);
		});
	});

});





(function($) {

    $.fn.checkToggle = function() {
			
		function toggle(element){
			
			var checked = $(element).parent().prev().attr("value");
			
			
			if(checked == "on"){
				
				$(element).find(".switchHandle").animate({marginLeft: '34px'},100, 
				
				function(){
					$(element).parent().parent().find(".lambs").removeClass("lamb_on");
					$(element).parent().parent().find(".lambs").addClass("lamb_off");

					$(element).prev().css("color","#cccccc");
					$(element).next().css("color","#333333");
					$(element).css("background-color", "red");
					$(element).parent().prev().attr("value", "off");
					$(this).removeClass("left").addClass("right");
				});
			
			}else{
			
				$(element).find(".switchHandle").animate({marginLeft: '0'}, 100, 
				
				function(){
				
					$(element).parent().parent().find(".lambs").removeClass("lamb_off");
					$(element).parent().parent().find(".lambs").addClass("lamb_on");
					
					$(element).css("background-color", "green");
					$(element).parent().prev().attr("value", "on");
					$(this).removeClass("right").addClass("left");
				});
			
			}
		
		};

		return this.each(function () {
		
			
			
			$(this).css('display','none');
			
			if($(this).attr("value") == "on"){
				$(this).after('<div class="toggleSwitch"><div class="switchArea" style="background-color: green"><span class="switchHandle left" style="margin-left: 0;"><\/span><\/div><\/div>');
			}else{
				$(this).after('<div class="toggleSwitch"><div class="switchArea" style="background-color: red"><span class="switchHandle right" style="margin-left:34px;"><\/span><\/div><\/div>');
			}			
			
			$(this).next().find('div.switchArea').bind("click", function () { 
				toggle(this);  
				val = $(this).parent().prev().attr("value");
				idm = $(this).parent().prev().attr("id");
				$("#"+idm).trigger('on_off', [val]);
			});
			
			

		});
		

	};

})(jQuery);

function make_on_off_ui($name){
	jQuery(document).ready(function() {
		jQuery("#"+$name).checkToggle();
	});
}













/**
 *
 * Color picker
 * Author: Stefan Petre www.eyecon.ro
 * 
 * Dependencies: jQuery
 *
 */
(function ($) {
	var ColorPicker = function () {
		var
			ids = {},
			inAction,
			charMin = 65,
			visible,
			tpl = '<div class="colorpicker"><div class="colorpicker_color"><div><div></div></div></div><div class="colorpicker_hue"><div></div></div><div class="colorpicker_new_color"></div><div class="colorpicker_current_color"></div><div class="colorpicker_hex"><input type="text" maxlength="6" size="6" /></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_submit"></div></div>',
			defaults = {
				eventName: 'click',
				onShow: function () {},
				onBeforeShow: function(){},
				onHide: function () {},
				onChange: function () {},
				onSubmit: function () {},
				color: 'ff0000',
				livePreview: true,
				flat: false
			},
			fillRGBFields = function  (hsb, cal) {
				var rgb = HSBToRGB(hsb);
				$(cal).data('colorpicker').fields
					.eq(1).val(rgb.r).end()
					.eq(2).val(rgb.g).end()
					.eq(3).val(rgb.b).end();
			},
			fillHSBFields = function  (hsb, cal) {
				$(cal).data('colorpicker').fields
					.eq(4).val(hsb.h).end()
					.eq(5).val(hsb.s).end()
					.eq(6).val(hsb.b).end();
			},
			fillHexFields = function (hsb, cal) {
				$(cal).data('colorpicker').fields
					.eq(0).val(HSBToHex(hsb)).end();
			},
			setSelector = function (hsb, cal) {
				$(cal).data('colorpicker').selector.css('backgroundColor', '#' + HSBToHex({h: hsb.h, s: 100, b: 100}));
				$(cal).data('colorpicker').selectorIndic.css({
					left: parseInt(150 * hsb.s/100, 10),
					top: parseInt(150 * (100-hsb.b)/100, 10)
				});
			},
			setHue = function (hsb, cal) {
				$(cal).data('colorpicker').hue.css('top', parseInt(150 - 150 * hsb.h/360, 10));
			},
			setCurrentColor = function (hsb, cal) {
				$(cal).data('colorpicker').currentColor.css('backgroundColor', '#' + HSBToHex(hsb));
			},
			setNewColor = function (hsb, cal) {
				$(cal).data('colorpicker').newColor.css('backgroundColor', '#' + HSBToHex(hsb));
			},
			keyDown = function (ev) {
				var pressedKey = ev.charCode || ev.keyCode || -1;
				if ((pressedKey > charMin && pressedKey <= 90) || pressedKey == 32) {
					return false;
				}
				var cal = $(this).parent().parent();
				if (cal.data('colorpicker').livePreview === true) {
					change.apply(this);
				}
			},
			change = function (ev) {
				var cal = $(this).parent().parent(), col;
				if (this.parentNode.className.indexOf('_hex') > 0) {
					cal.data('colorpicker').color = col = HexToHSB(fixHex(this.value));
				} else if (this.parentNode.className.indexOf('_hsb') > 0) {
					cal.data('colorpicker').color = col = fixHSB({
						h: parseInt(cal.data('colorpicker').fields.eq(4).val(), 10),
						s: parseInt(cal.data('colorpicker').fields.eq(5).val(), 10),
						b: parseInt(cal.data('colorpicker').fields.eq(6).val(), 10)
					});
				} else {
					cal.data('colorpicker').color = col = RGBToHSB(fixRGB({
						r: parseInt(cal.data('colorpicker').fields.eq(1).val(), 10),
						g: parseInt(cal.data('colorpicker').fields.eq(2).val(), 10),
						b: parseInt(cal.data('colorpicker').fields.eq(3).val(), 10)
					}));
				}
				if (ev) {
					fillRGBFields(col, cal.get(0));
					fillHexFields(col, cal.get(0));
					fillHSBFields(col, cal.get(0));
				}
				setSelector(col, cal.get(0));
				setHue(col, cal.get(0));
				setNewColor(col, cal.get(0));
				cal.data('colorpicker').onChange.apply(cal, [col, HSBToHex(col), HSBToRGB(col)]);
			},
			blur = function (ev) {
				var cal = $(this).parent().parent();
				cal.data('colorpicker').fields.parent().removeClass('colorpicker_focus')
			},
			focus = function () {
				charMin = this.parentNode.className.indexOf('_hex') > 0 ? 70 : 65;
				$(this).parent().parent().data('colorpicker').fields.parent().removeClass('colorpicker_focus');
				$(this).parent().addClass('colorpicker_focus');
			},
			downIncrement = function (ev) {
				var field = $(this).parent().find('input').focus();
				var current = {
					el: $(this).parent().addClass('colorpicker_slider'),
					max: this.parentNode.className.indexOf('_hsb_h') > 0 ? 360 : (this.parentNode.className.indexOf('_hsb') > 0 ? 100 : 255),
					y: ev.pageY,
					field: field,
					val: parseInt(field.val(), 10),
					preview: $(this).parent().parent().data('colorpicker').livePreview					
				};
				$(document).bind('mouseup', current, upIncrement);
				$(document).bind('mousemove', current, moveIncrement);
			},
			moveIncrement = function (ev) {
				ev.data.field.val(Math.max(0, Math.min(ev.data.max, parseInt(ev.data.val + ev.pageY - ev.data.y, 10))));
				if (ev.data.preview) {
					change.apply(ev.data.field.get(0), [true]);
				}
				return false;
			},
			upIncrement = function (ev) {
				change.apply(ev.data.field.get(0), [true]);
				ev.data.el.removeClass('colorpicker_slider').find('input').focus();
				$(document).unbind('mouseup', upIncrement);
				$(document).unbind('mousemove', moveIncrement);
				return false;
			},
			downHue = function (ev) {
				var current = {
					cal: $(this).parent(),
					y: $(this).offset().top
				};
				current.preview = current.cal.data('colorpicker').livePreview;
				$(document).bind('mouseup', current, upHue);
				$(document).bind('mousemove', current, moveHue);
			},
			moveHue = function (ev) {
				change.apply(
					ev.data.cal.data('colorpicker')
						.fields
						.eq(4)
						.val(parseInt(360*(150 - Math.max(0,Math.min(150,(ev.pageY - ev.data.y))))/150, 10))
						.get(0),
					[ev.data.preview]
				);
				return false;
			},
			upHue = function (ev) {
				fillRGBFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
				fillHexFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
				$(document).unbind('mouseup', upHue);
				$(document).unbind('mousemove', moveHue);
				return false;
			},
			downSelector = function (ev) {
				var current = {
					cal: $(this).parent(),
					pos: $(this).offset()
				};
				current.preview = current.cal.data('colorpicker').livePreview;
				$(document).bind('mouseup', current, upSelector);
				$(document).bind('mousemove', current, moveSelector);
			},
			moveSelector = function (ev) {
				change.apply(
					ev.data.cal.data('colorpicker')
						.fields
						.eq(6)
						.val(parseInt(100*(150 - Math.max(0,Math.min(150,(ev.pageY - ev.data.pos.top))))/150, 10))
						.end()
						.eq(5)
						.val(parseInt(100*(Math.max(0,Math.min(150,(ev.pageX - ev.data.pos.left))))/150, 10))
						.get(0),
					[ev.data.preview]
				);
				return false;
			},
			upSelector = function (ev) {
				fillRGBFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
				fillHexFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
				$(document).unbind('mouseup', upSelector);
				$(document).unbind('mousemove', moveSelector);
				return false;
			},
			enterSubmit = function (ev) {
				$(this).addClass('colorpicker_focus');
			},
			leaveSubmit = function (ev) {
				$(this).removeClass('colorpicker_focus');
			},
			clickSubmit = function (ev) {
				var cal = $(this).parent();
				var col = cal.data('colorpicker').color;
				cal.data('colorpicker').origColor = col;
				setCurrentColor(col, cal.get(0));
				cal.data('colorpicker').onSubmit(col, HSBToHex(col), HSBToRGB(col));
				cal.hide();
			},
			show = function (ev) {
				var cal = $('#' + $(this).data('colorpickerId'));
				cal.data('colorpicker').onBeforeShow.apply(this, [cal.get(0)]);
				var pos = $(this).offset();
				var viewPort = getViewport();
				var top = pos.top + this.offsetHeight;
				var left = pos.left;
				if (top + 176 > viewPort.t + viewPort.h) {
					top -= this.offsetHeight + 176;
				} else {
				  top += 5;
				}
				if (left + 356 > viewPort.l + viewPort.w) {
					left -= 356;
				}
				cal.css({left: left + 'px', top: top + 'px'});
				if (cal.data('colorpicker').onShow.apply(this, [cal.get(0)]) != false) {
					cal.show();
				}
				$(document).bind('mousedown', {cal: cal}, hide);
				return false;
			},
			hide = function (ev) {
				if (!isChildOf(ev.data.cal.get(0), ev.target, ev.data.cal.get(0))) {
					if (ev.data.cal.data('colorpicker').onHide.apply(this, [ev.data.cal.get(0)]) != false) {
						ev.data.cal.hide();
					}
					$(document).unbind('mousedown', hide);
				}
			},
			isChildOf = function(parentEl, el, container) {
				if (parentEl == el) {
					return true;
				}
				if (parentEl.contains) {
					return parentEl.contains(el);
				}
				if ( parentEl.compareDocumentPosition ) {
					return !!(parentEl.compareDocumentPosition(el) & 16);
				}
				var prEl = el.parentNode;
				while(prEl && prEl != container) {
					if (prEl == parentEl)
						return true;
					prEl = prEl.parentNode;
				}
				return false;
			},
			getViewport = function () {
				var m = document.compatMode == 'CSS1Compat';
				return {
					l : window.pageXOffset || (m ? document.documentElement.scrollLeft : document.body.scrollLeft),
					t : window.pageYOffset || (m ? document.documentElement.scrollTop : document.body.scrollTop),
					w : window.innerWidth || (m ? document.documentElement.clientWidth : document.body.clientWidth),
					h : window.innerHeight || (m ? document.documentElement.clientHeight : document.body.clientHeight)
				};
			},
			fixHSB = function (hsb) {
				return {
					h: Math.min(360, Math.max(0, hsb.h)),
					s: Math.min(100, Math.max(0, hsb.s)),
					b: Math.min(100, Math.max(0, hsb.b))
				};
			}, 
			fixRGB = function (rgb) {
				return {
					r: Math.min(255, Math.max(0, rgb.r)),
					g: Math.min(255, Math.max(0, rgb.g)),
					b: Math.min(255, Math.max(0, rgb.b))
				};
			},
			fixHex = function (hex) {
				var len = 6 - hex.length;
				if (len > 0) {
					var o = [];
					for (var i=0; i<len; i++) {
						o.push('0');
					}
					o.push(hex);
					hex = o.join('');
				}
				return hex;
			}, 
			HexToRGB = function (hex) {
				var hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
				return {r: hex >> 16, g: (hex & 0x00FF00) >> 8, b: (hex & 0x0000FF)};
			},
			HexToHSB = function (hex) {
				return RGBToHSB(HexToRGB(hex));
			},
			RGBToHSB = function (rgb) {
				var hsb = {};
				hsb.b = Math.max(Math.max(rgb.r,rgb.g),rgb.b);
				hsb.s = (hsb.b <= 0) ? 0 : Math.round(100*(hsb.b - Math.min(Math.min(rgb.r,rgb.g),rgb.b))/hsb.b);
				hsb.b = Math.round((hsb.b /255)*100);
				if((rgb.r==rgb.g) && (rgb.g==rgb.b)) hsb.h = 0;
				else if(rgb.r>=rgb.g && rgb.g>=rgb.b) hsb.h = 60*(rgb.g-rgb.b)/(rgb.r-rgb.b);
				else if(rgb.g>=rgb.r && rgb.r>=rgb.b) hsb.h = 60  + 60*(rgb.g-rgb.r)/(rgb.g-rgb.b);
				else if(rgb.g>=rgb.b && rgb.b>=rgb.r) hsb.h = 120 + 60*(rgb.b-rgb.r)/(rgb.g-rgb.r);
				else if(rgb.b>=rgb.g && rgb.g>=rgb.r) hsb.h = 180 + 60*(rgb.b-rgb.g)/(rgb.b-rgb.r);
				else if(rgb.b>=rgb.r && rgb.r>=rgb.g) hsb.h = 240 + 60*(rgb.r-rgb.g)/(rgb.b-rgb.g);
				else if(rgb.r>=rgb.b && rgb.b>=rgb.g) hsb.h = 300 + 60*(rgb.r-rgb.b)/(rgb.r-rgb.g);
				else hsb.h = 0;
				hsb.h = Math.round(hsb.h);
				return hsb;
			},
			HSBToRGB = function (hsb) {
				var rgb = {};
				var h = Math.round(hsb.h);
				var s = Math.round(hsb.s*255/100);
				var v = Math.round(hsb.b*255/100);
				if(s == 0) {
					rgb.r = rgb.g = rgb.b = v;
				} else {
					var t1 = v;
					var t2 = (255-s)*v/255;
					var t3 = (t1-t2)*(h%60)/60;
					if(h==360) h = 0;
					if(h<60) {rgb.r=t1;	rgb.b=t2; rgb.g=t2+t3}
					else if(h<120) {rgb.g=t1; rgb.b=t2;	rgb.r=t1-t3}
					else if(h<180) {rgb.g=t1; rgb.r=t2;	rgb.b=t2+t3}
					else if(h<240) {rgb.b=t1; rgb.r=t2;	rgb.g=t1-t3}
					else if(h<300) {rgb.b=t1; rgb.g=t2;	rgb.r=t2+t3}
					else if(h<360) {rgb.r=t1; rgb.g=t2;	rgb.b=t1-t3}
					else {rgb.r=0; rgb.g=0;	rgb.b=0}
				}
				return {r:Math.round(rgb.r), g:Math.round(rgb.g), b:Math.round(rgb.b)};
			},
			RGBToHex = function (rgb) {
				var hex = [
					rgb.r.toString(16),
					rgb.g.toString(16),
					rgb.b.toString(16)
				];
				$.each(hex, function (nr, val) {
					if (val.length == 1) {
						hex[nr] = '0' + val;
					}
				});
				return hex.join('');
			},
			HSBToHex = function (hsb) {
				return RGBToHex(HSBToRGB(hsb));
			};
		return {
			init: function (options) {
				options = $.extend({}, defaults, options||{});
				if (typeof options.color == 'string') {
					options.color = HexToHSB(options.color);
				} else if (options.color.r != undefined && options.color.g != undefined && options.color.b != undefined) {
					options.color = RGBToHSB(options.color);
				} else if (options.color.h != undefined && options.color.s != undefined && options.color.b != undefined) {
					options.color = fixHSB(options.color);
				} else {
					return this;
				}
				options.origColor = options.color;
				return this.each(function () {
					if (!$(this).data('colorpickerId')) {
						var id = 'collorpicker_' + parseInt(Math.random() * 1000);
						$(this).data('colorpickerId', id);
						var cal = $(tpl).attr('id', id);
						if (options.flat) {
							cal.appendTo(this).show();
						} else {
							cal.appendTo(document.body);
						}
						options.fields = cal
											.find('input')
											.bind('keydown', keyDown)
											.bind('change', change)
											.bind('blur', blur)
											.bind('focus', focus);
						cal.find('span').bind('mousedown', downIncrement);
						options.selector = cal.find('div.colorpicker_color').bind('mousedown', downSelector);
						options.selectorIndic = options.selector.find('div div');
						options.hue = cal.find('div.colorpicker_hue div');
						cal.find('div.colorpicker_hue').bind('mousedown', downHue);
						options.newColor = cal.find('div.colorpicker_new_color');
						options.currentColor = cal.find('div.colorpicker_current_color');
						cal.data('colorpicker', options);
						cal.find('div.colorpicker_submit')
							.bind('mouseenter', enterSubmit)
							.bind('mouseleave', leaveSubmit)
							.bind('click', clickSubmit);
						fillRGBFields(options.color, cal.get(0));
						fillHSBFields(options.color, cal.get(0));
						fillHexFields(options.color, cal.get(0));
						setHue(options.color, cal.get(0));
						setSelector(options.color, cal.get(0));
						setCurrentColor(options.color, cal.get(0));
						setNewColor(options.color, cal.get(0));
						if (options.flat) {
							cal.css({
								position: 'relative',
								display: 'block'
							});
						} else {
							$(this).bind(options.eventName, show);
						}
					}
				});
			},
			showPicker: function() {
				return this.each( function () {
					if ($(this).data('colorpickerId')) {
						show.apply(this);
					}
				});
			},
			hidePicker: function() {
				return this.each( function () {
					if ($(this).data('colorpickerId')) {
						$('#' + $(this).data('colorpickerId')).hide();
					}
				});
			},
			setColor: function(col) {
				if (typeof col == 'string') {
					col = HexToHSB(col);
				} else if (col.r != undefined && col.g != undefined && col.b != undefined) {
					col = RGBToHSB(col);
				} else if (col.h != undefined && col.s != undefined && col.b != undefined) {
					col = fixHSB(col);
				} else {
					return this;
				}
				return this.each(function(){
					if ($(this).data('colorpickerId')) {
						var cal = $('#' + $(this).data('colorpickerId'));
						cal.data('colorpicker').color = col;
						cal.data('colorpicker').origColor = col;
						fillRGBFields(col, cal.get(0));
						fillHSBFields(col, cal.get(0));
						fillHexFields(col, cal.get(0));
						setHue(col, cal.get(0));
						setSelector(col, cal.get(0));
						setCurrentColor(col, cal.get(0));
						setNewColor(col, cal.get(0));
					}
				});
			}
		};
	}();
	$.fn.extend({
		ColorPicker: ColorPicker.init,
		ColorPickerHide: ColorPicker.hide,
		ColorPickerShow: ColorPicker.show,
		ColorPickerSetColor: ColorPicker.setColor
	});
})(jQuery)




function make_colorpicker($name){

	jQuery(document).ready(function() {  
		jQuery('#colorpic_'+$name).ColorPicker({
			onSubmit: function(hsb, hex, rgb) {
				jQuery('#colorpic_'+$name).val('#'+hex);
			},
			onBeforeShow: function () {
				jQuery(this).ColorPickerSetColor(this.value);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#colordiv_'+$name+' div').css({'backgroundColor':'#'+hex, 'backgroundImage': 'none', 'borderColor':'#'+hex});
				jQuery('.inputcolor_'+$name).attr('value', '#'+hex);
			}
		})	
		.bind('keyup', function(){
		jQuery(this).ColorPickerSetColor(this.value);
		});
	  
		jQuery('.inputcolor_'+$name).bind('keyup', function(){
			$val = jQuery(this).attr('value');
			jQuery('#colordiv_'+$name+' div').css({'backgroundColor':$val, 'backgroundImage': 'none', 'borderColor':$val});
		});
	 
	});

}






/*
	Insert Into Button Code for uploads!
	Picture live preview function here!
*/

jQuery.noConflict();
jQuery(document).ready(function()
{
	/* begin slider upload button */ 
	jQuery('.upload_input').each(function()
	{
		$get_div = '#' + jQuery(this).attr('id') + '_preview';
		var rege = /gif$|jpg$|png$|GIF$|JPG$|PNG$|ico$|ICO$/;
		
		if( rege.test( jQuery(this).val() ) ){ 
			
			jQuery($get_div).css({"background-image": "url("+jQuery(this).val()+")"}); 
			
		}else{
			
			if(jQuery(this).val() != ""){
				jQuery($get_div).css("background-image", "");
				jQuery($get_div).removeClass("upload_pic");
				jQuery($get_div).addClass("external_pic");
			}
			
		}
		

		
		jQuery(this).bind('change focus blur framework keyup', function()
		{	
			
			$get_cur_div = '#' + jQuery(this).attr('id') + '_preview';
			if( rege.test( jQuery(this).val() ) ){  

				jQuery($get_cur_div).css({"background-image": "url("+jQuery(this).val()+")"}); 
				
				if(jQuery(this).val() != ""){
					jQuery($get_cur_div).removeClass("external_pic");
					jQuery($get_cur_div).addClass("upload_pic");
				}
			
			}else{
			
				if(jQuery(this).val() != ""){
					jQuery($get_cur_div).css("background-image", "");
					jQuery($get_cur_div).removeClass("upload_pic");
					jQuery($get_cur_div).addClass("external_pic");
				}
				jQuery($get_cur_div).html('');
			
			}
			
		});
	});
   
   /* new send to media editor */
   window.original_send_to_editor = window.send_to_editor;
   window.media_send_to_custom_editor = function(html) {
      var _img = jQuery(html).attr('src') || jQuery(html).find('img').attr('src') || jQuery(html).attr('href');
      
      console.log(_img);

      if(_img != undefined){
         jQuery('.'+elem_id).val(_img).trigger('framework');
         tb_remove();
         window.send_to_editor = window.original_send_to_editor;
      }else{
         console.log('there');
			window.original_send_to_editor(html);
		}
   }
   
	jQuery('.set_input').live('click', function(){
      window.elem_id = jQuery(this).attr('id');
      window.send_to_editor = window.media_send_to_custom_editor;
	});
   
   
});



jQuery(document).ready(function(){

	closeStatus = true;
	
	jQuery(".hows_to_infos").mouseover(function(){
		closeStatus = false;
	}).mouseout(function(){
		closeStatus = true;
	});

	jQuery(document).mousedown(function(){
		
		if(closeStatus == true){
			jQuery('.hows_to_infos').fadeOut("fast");
		}
		
	});


	jQuery(".hows_to li").click(function(e){
		
		position = jQuery(this).position();

		jQuery('.hows_to_infos_bg').html( jQuery(this).find("span").html() );
		jQuery('.hows_to_infos').fadeIn("fast");

		width = parseInt(jQuery('.hows_to_infos_bg').width() / 2) - parseInt(jQuery(this).width() - 5)/2;
		height = parseInt(jQuery('.hows_to_infos_bg').height()) + 20;
		
		jQuery('.hows_to_infos').css( {"left": position.left-width, "top": position.top-height});

		/*
			Portfolio pre settings!
		*/
		jQuery(".set_auto_portfolio").click (function (){
				
			the_title_column = jQuery(this).attr("title");
			
			/*
				var pre_settings = [
				p_width(0), p_height(1), p_column(2), p_padding(3), p_limit(4), 
				p_more(5), p_date(6), p_up(7), p_title(8), p_desc(9)
				];
			*/
			switch(the_title_column){
				case 'one-column':
					var pre_settings = [950, 350, 1, 30, 10, "off", "off", "off", "off", "off"];
				break;
				case '2-columns':
					var pre_settings = [450, 300, 2, 35, 6, "off", "off", "off", "off", "off"];
				break;
				case '3-columns':
					var pre_settings = [280, 150, 3, 40, 9, "off", "off", "off", "off", "off"];
				break;
				case '4-columns':
					var pre_settings = [200, 125, 4, 37, 12, "off", "off", "off", "off", "off"];
				break;
				case '5-columns':
					var pre_settings = [160, 125, 5, 25, 15, "off", "off", "off", "off", "off"];
				break;
				
				case 'one-column-ws':
					var pre_settings = [700, 350, 1, 30, 10, "off", "off", "off", "off", "off"];
				break;
				case '2-columns-ws':
					var pre_settings = [325, 250, 2, 35, 6, "off", "off", "off", "off", "off"];
				break;
				case '3-columns-ws':
					var pre_settings = [200, 150, 3, 35, 9, "off", "off", "off", "off", "off"];
				break;
				case '4-columns-ws':
					var pre_settings = [140, 100, 4, 25, 12, "off", "off", "off", "off", "off"];
				break;
				case '5-columns-ws':
					var pre_settings = [110, 90, 5, 25, 15, "off", "off", "off", "off", "off"];
				break;
				
				case 'just-photos':
					var pre_settings = [160, 125, 5, 25, 15, "on", "on", "on", "on", "on"];
				break;
				
				case 'just-photos-2':
					var pre_settings = [280, 150, 3, 40, 15, "on", "on", "on", "off", "on"];
				break;
			}
			
			jQuery(".slider_ui_name_p_width").val(pre_settings[0]).trigger("trig_slider");
			jQuery(".slider_ui_name_p_height").val(pre_settings[1]).trigger("trig_slider");
			jQuery(".slider_ui_name_p_column").val(pre_settings[2]).trigger("trig_slider");
			jQuery(".slider_ui_name_p_padding").val(pre_settings[3]).trigger("trig_slider");
			jQuery(".slider_ui_name_p_limit").val(pre_settings[4]).trigger("trig_slider");

			jQuery("#p_more").val(pre_settings[5]).next().find(".switchArea").trigger("click");
			jQuery("#p_date").val(pre_settings[6]).next().find(".switchArea").trigger("click");
			jQuery("#p_up").val(pre_settings[7]).next().find(".switchArea").trigger("click");
			jQuery("#p_title").val(pre_settings[8]).next().find(".switchArea").trigger("click");
			jQuery("#p_desc").val(pre_settings[9]).next().find(".switchArea").trigger("click");
			
					
			jQuery('.hows_to_infos').fadeOut("fast");
			
			
			return false;
			
		});
		

		/*
			Home Page boxes Pre-Configured Settings
		*/
		jQuery(".set_auto_box").click (function (){

			the_title_widget = jQuery(this).attr("title");
			
			switch(the_title_widget){
				case '1-box':
					var widget_settings = [960, 1, 0, 1];
				break;
				case '2-boxes':
					var widget_settings = [460, 2, 40, 2];
				break;
				case '3-boxes':
					var widget_settings = [300, 3, 30, 3];
				break;
				case '4-boxes':
					var widget_settings = [460, 2, 40, 4];
				break;
				case '5-boxes':
					var widget_settings = [180, 5, 15, 5];
				break;
				case '6-boxes':
					var widget_settings = [300, 3, 30, 6];
				break;
				case '9-boxes':
					var widget_settings = [300, 3, 30, 9];
				break;
				case '12-boxes':
					var widget_settings = [300, 3, 30, 12];
				break;
			}
			
			jQuery(".slider_ui_name_w_width").val(widget_settings[0]).trigger("trig_slider");
			jQuery(".slider_ui_name_w_column").val(widget_settings[1]).trigger("trig_slider");
			jQuery(".slider_ui_name_w_padding").val(widget_settings[2]).trigger("trig_slider");
			jQuery(".slider_ui_name_w_limit").val(widget_settings[3]).trigger("trig_slider");

			jQuery('.hows_to_infos').fadeOut("fast");
			
			return false;
			
		});
		
		
		
		
	});

	jQuery("#_timthumb").each(function(){


		jQuery(this).bind('on_off',function(event, t){
			
			if( t == "off" ){
				jQuery(".thumbnail_holder").slideToggle();
				
				jQuery("#upload_post_thumbnail").attr("name", "");
			}else{
				jQuery(".thumbnail_holder").slideToggle();
				jQuery("#upload_post_thumbnail").attr("name", "options[post_thumbnail]");
			}
			
			
		});
	
	});

	jQuery("#p_filter").each(function(){

		current_val = jQuery(".slider_ui_name_p_limit").val();

		jQuery(this).bind('on_off',function(event, t){
			
			if( t == "off" ){

				jQuery(".slider_ui_name_p_limit").val(50).trigger("trig_slider");
		
			}else{
			
				jQuery(".slider_ui_name_p_limit").val(current_val).trigger("trig_slider");

			}			
			
		});
	
	});
	
});


jQuery(document).ready(function(){
	
	jQuery("#submit_slider_category").click( function() {
		if(!jQuery("#c_name").val()){
			alert("Error, don't forget to write category name!");
			return false;
		}		
	});

	jQuery(".save_new_item").click(function(){
		
		return_it = false;
		jQuery(".categories_sliders").each(function(){
			if( jQuery(this).is(':checked') ){
				return_it = true;
			}
		});
		
		if(return_it == false ){
			alert("Error, don't forget to choose a category!");
			return false;
		}

		
	});	

});

jQuery(document).ready(function(){
	
	jQuery('#page_template').each(function(){
		
		jQuery(this).bind('change templates_page', function(){
			
			current_val = jQuery(this).val();
			
			jQuery("#portfolio_page_custom_fields").slideUp('fast');
			jQuery("#blog_page_custom_fields").slideUp('fast');
			
			jQuery("#blog_page_custom_fields, #portfolio_page_custom_fields").each( function() {
				jQuery(this).find("input, select, textarea").each( function (){
					jQuery(this).attr( "name" , jQuery(this).attr("name").replace('options','-not-changed-') );
				}); 
			});
			
			if(current_val == "page-portfolio-template.php"){
				
				jQuery("#portfolio_page_custom_fields").slideDown('fast');
				jQuery("#portfolio_page_custom_fields").each( function() {
				jQuery(this).find("input, select, textarea").each( function (){
						jQuery(this).attr( "name" , jQuery(this).attr("name").replace('-not-changed-','options') );
					}); 
				});
				
			}else if(current_val == "page-blog-template.php"){
				
				jQuery("#blog_page_custom_fields").slideDown('fast');
				jQuery("#blog_page_custom_fields").each( function() {
					jQuery(this).find("input, select, textarea").each( function (){
						jQuery(this).attr( "name" , jQuery(this).attr("name").replace('-not-changed-','options') );
					}); 
				});
							
			}
			
		});
		
		jQuery("#page_template").trigger("templates_page");
		
	});

});


jQuery(document).ready(function(){
	
	jQuery('.checkbox__sidebar_type li').each( function(){
	
		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus sidebar_trigger', function(){
			
			
			if( jQuery(this).find("input").val() == "left" || jQuery(this).find("input").val() == "right" && jQuery(this).find("input").is(":checked") ){
				jQuery(".sidebar_active").fadeIn("fast");
			}else{
				jQuery(".sidebar_active").fadeOut("fast");
			}
			
			jQuery('.checkbox__sidebar_type li.selected').removeClass('selected');
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.checkbox__sidebar_type li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});
		
	

	});
	
	
	jQuery('.check_bonus li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus check_bonus_trigger', function(){
			
			jQuery('.check_bonus li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.check_bonus li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});

	
	
	jQuery('.home_model li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus home_design_trigger', function(){
			
			jQuery('.home_model li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.home_model li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});
	
	
	
	
	/* fringe tech options */
	jQuery('.header_type li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.header_type li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.header_type li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});
	
	
	
	/* fringe tech options */
	jQuery('.w_icon li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.w_icon li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.w_icon li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});
	
	
	/* fringe tech options */
	jQuery('.footer_type li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.footer_type li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.footer_type li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});
	
	
	
	/* fringe tech options */
	jQuery('.sidebar_position li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.sidebar_position li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
				
			}else{
				jQuery('.sidebar_position li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});

	
	/* fringe tech options */
	jQuery('.blog_type li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.blog_type li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
			}else{
			
				jQuery('.blog_type li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});

	
	/* fringe tech options */
	jQuery('.button_colors li').each( function(){

		if ( jQuery(this).find("input").is(":checked") ){
		
			jQuery(this).addClass('selected');
			
		}
			
		jQuery(this).bind('click blur focus option_name_trigger', function(){
			
			jQuery('.button_colors li.selected').removeClass('selected');
			
			jQuery(this).addClass('selected');
			if ( jQuery(this).find("input").is(":checked") ){
			
				jQuery(this).addClass('selected');
				
			}else{
			
				jQuery('.button_colors li.selected').removeClass('selected');
		
				jQuery(this).parent().find("input").each(function(){
					jQuery(this).attr("checked", false);
				});
				
			}
			
		});

	});


	/*
		Help ToolTip
	*/
	jQuery(".option_tooltip").hover(function () {
	
		jQuery('.tooltip_text').html('').html(jQuery(this).find("span.tooltip").html());
		position = jQuery(this).position();
		
		c_pad		= 5;
		c_width		= parseInt( jQuery('.tooltip_content').width() + 10 );
        c_height	= parseInt( ( jQuery('.tooltip_content').height() / 2 ) - c_pad );
        
		jQuery('.tooltip_content').css({"left": position.left - c_width, "top": position.top - c_height});
		jQuery('.tooltip_arrow').css({"margin-top": c_height});

		jQuery('.tooltip_content')
		.fadeIn()
		.stop()
		.css({"left": position.left - c_width + 10, "top": position.top - c_height})
		.animate({"opacity": 1, "left": position.left - c_width  }, 250);
		
	}, function () {

        jQuery('.tooltip_content').hide();
		jQuery('.tooltip_text').html('');
		jQuery('.tooltip_arrow').css({"margin-top": 0});		
		
	});	
	

});
	
	