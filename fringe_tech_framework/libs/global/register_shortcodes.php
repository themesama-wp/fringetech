<?php
/*
	Do ShortCode!
*/
function my_formatter($content) {
	$new_content = '';
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

	foreach ($pieces as $piece) {
		if (preg_match($pattern_contents, $piece, $matches)) {
			$new_content .= $matches[1];
		} else {
			$new_content .= wptexturize(wpautop($piece));
		}
	}

	return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');
add_filter('the_content', 'my_formatter', 99);
add_filter('widget_text', 'my_formatter', 99);
add_filter('the_content', 'do_shortcode', 11);
add_filter('widget_text', 'do_shortcode', 11);

/*********************************************************
	Register Shortcode "PORTFOLIO"
**********************************************************/
$shortcode_attrs["portfolio"] = array( 'type' => 's', 'group' => 'Basics', 'name' => 'Portfolio', 'attr' => array ( 'id' => '' ) );

function portfolio( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["portfolio"]["attr"], $atts ) );
	return get_portfolios( esc_attr($id) );
}
add_shortcode('portfolio', 'portfolio');


/*********************************************************
	Register Shortcode "BLOG"
**********************************************************/
$shortcode_attrs["blog"] = array( 'type' => 's', 'group' => 'Basics', 'name' => 'Blog', 'attr' => array ( 'id' => '', 'sidebar_position' => 'none' ) );

function blog( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["blog"]["attr"], $atts ) );
	return get_blogs( esc_attr($id), esc_attr($sidebar_position));
}
add_shortcode('blog', 'blog');


/*********************************************************
	Register Shortcode "Slider"
**********************************************************/
$shortcode_attrs["slider"] = array( 'type' => 's', 'group' => 'Basics', 'name' => 'Slider', 'attr' => array ( 'id' => '' ) );

function slider( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["slider"]["attr"], $atts ) );
	return get_slider( esc_attr($id) );
}
add_shortcode('slider', 'slider');


/*********************************************************
	Register Shortcode "Simples"
**********************************************************/
$shortcode_attrs["button"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'Button', 'intro' => 'YourTextHere', 'attr' => array ( 'link' => 'http://', 'target' => '', 'color' => get_clean_option("button_colors", "ocean") ) );

function button( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["button"]["attr"], $atts ) );
	
	$shortcode = '<div><a href="'.esc_attr($link).'" target="'.esc_attr($target).'"><span class="small_buttons"><span class="'.esc_attr($color).'_l small_left"><span class="'.esc_attr($color).'_r small_right">' . $content . '</span></span></span></a></div>';
	return $shortcode;
	
}
add_shortcode('button', 'button');

/* DropCap */
$shortcode_attrs["dropcap"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'DropCap', 'intro' => 'W', 'attr' => array ( 'color' => '#ccc'  ) );

function dropcap( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["dropcap"]["attr"], $atts ) );
	
	return '<div class="dropcap" style="color:'.esc_attr($color).'">'. do_shortcode($content) . '</div>';
	
}
add_shortcode('dropcap', 'dropcap');


/* Blockquote */
$shortcode_attrs["blockquote"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'Blockquote', 'intro' => 'YourText', 'attr' => array ( 'cite' => 'Codestar'  ) );

function blockquote( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["blockquote"]["attr"], $atts ) );

	if(esc_attr($cite) != ""){
	
		$cite = "<cite>" . esc_attr($cite) . "</cite>";

	}
	
	return '<blockquote class="'. esc_attr($type) .'"><p>'. do_shortcode($content) . $cite . '</p></blockquote>';
	
}
add_shortcode('blockquote', 'blockquote');


/* justify */
$shortcode_attrs["justify"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'Justify', 'intro' => 'YourText' );

function justify( $atts, $content = null ) {	
	return '<span class="justify">'. do_shortcode($content) .'</span>';
}
add_shortcode('justify', 'justify');



/* Tabs Shortcode */
$shortcode_attrs["tabs"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'Tabs', 'intro' => '\n[tab]TabContent[/tab]\n[tab]TabContent[/tab]\n[tab]TabContent[/tab]\n', 'attr' => array ( 'titles' => 'Tab1, Tab2, Tab3', 'model' => '1' ) );

function tabs( $atts, $content = null ) {
	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["tabs"]["attr"], $atts ) );
	
	$class_id =  rand(0, 100);

	$tab_titles  = esc_attr($titles);
	$tab_totals = explode(",", $tab_titles);
		
		
	$result		= '<div class="tab_model_'.esc_attr($model).'">';
	
	$result .= '<ul class="tabs_'.$class_id.' tabs">';
	$s = 1;
	for ( $i = 0; $i <= count($tab_totals)-1; $i++) {
		$result .= '<li><h6><a href="#tab'.$s++.'">'.trim($tab_totals[$i]).'</a></h6></li>';
	} 
	$result .= '</ul>';
	
	$result .= '<ul class="tab_'.$class_id.' tab_container">';
	$result .= do_shortcode($content);
	$result .= '</ul>';
	$result .= '</div>';
	
	return $result;
	
}
add_shortcode('tabs', 'tabs');

/* Tabs Shortcode */
/*$shortcode_attrs["tab"] = array( 'type' => 'h', 'group' => 'Basics', 'name' => 'Tab', 'intro' => 'TabContent' );*/

function tab( $atts, $content = null ) {

	$result .= '<li>';
	$result .= do_shortcode($content);
	$result .= '</li>';
	
	return $result;
	
}
add_shortcode('tab', 'tab');



/* Toggle Shortcode */
$shortcode_attrs["toggle"] = array( 'type' => 'm', 'group' => 'Basics', 'name' => 'Toggle', 'intro' => 'Your Content Here', 'attr' => array ( 'title' => 'Toggle', 'height' => 'auto' ) );

function toggle( $atts, $content = null ) {
	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["toggle"]["attr"], $atts ) );

	if( esc_attr($height) == "auto"){
	
		$height_css = 'style="height:auto;"';
		
	} else {
	
		$height_css = 'style="height:'.esc_attr($height).'px;"';
		
	}
	
	$result = '<div class="toggle"><h4 class="toggler"><a href="javascript:void(0);">'.esc_attr($title).'</a></h4><div class="toggle_container"><div class="block" '.$height_css.'>'.do_shortcode($content).'</div></div></div>';
	
	return $result;
	
}
add_shortcode('toggle', 'toggle');

/*********************************************************
	Register Shortcode "Columns Layouts"
**********************************************************/

/* One Half */
$shortcode_attrs["one_half"] 		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Half', 'intro' => 'YourTextHere');
$shortcode_attrs["one_half_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Half Last', 'intro' => 'YourTextHere');

function one_half( $atts, $content = null ) {			return '<div class="columns one_half">'.do_shortcode($content).'</div>'; }	
function one_half_last( $atts, $content = null ) {		return '<div class="columns one_half last">'.do_shortcode($content).'</div>'; }	
add_shortcode('one_half', 'one_half');
add_shortcode('one_half_last', 'one_half_last');


/* One Third */
$shortcode_attrs["one_third"]		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Third', 'intro' => 'YourTextHere');
$shortcode_attrs["one_third_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Third Last', 'intro' => 'YourTextHere');

function one_third( $atts, $content = null ) {			return '<div class="columns one_third">'.do_shortcode($content).'</div>'; }	
function one_third_last( $atts, $content = null ) {		return '<div class="columns one_third last">'.do_shortcode($content).'</div>'; }	
add_shortcode('one_third', 'one_third');
add_shortcode('one_third_last', 'one_third_last');	


/* One Fourth */
$shortcode_attrs["one_fourth"] 		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Fourth', 'intro' => 'YourTextHere');
$shortcode_attrs["one_fourth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Fourth Last', 'intro' => 'YourTextHere');

function one_fourth( $atts, $content = null ) {			return '<div class="columns one_fourth">'.do_shortcode($content).'</div>'; }	
function one_fourth_last( $atts, $content = null ) {	return '<div class="columns one_fourth last">'.do_shortcode($content).'</div>'; }

add_shortcode('one_fourth', 'one_fourth');
add_shortcode('one_fourth_last', 'one_fourth_last');	


/* One Fifth */
$shortcode_attrs["one_fifth"] 		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Fifth', 'intro' => 'YourTextHere');
$shortcode_attrs["one_fifth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Fifth Last', 'intro' => 'YourTextHere');

function one_fifth( $atts, $content = null ) {			return '<div class="columns one_fifth">'.do_shortcode($content).'</div>'; }	
function one_fifth_last( $atts, $content = null ) {		return '<div class="columns one_fifth last">'.do_shortcode($content).'</div>'; }	

add_shortcode('one_fifth', 'one_fifth');
add_shortcode('one_fifth_last', 'one_fifth_last');	


/* One Sixth */
$shortcode_attrs["one_sixth"] 		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Sixth', 'intro' => 'YourTextHere');
$shortcode_attrs["one_sixth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'One Sixth Last', 'intro' => 'YourTextHere');

function one_sixth( $atts, $content = null ) {			return '<div class="columns one_sixth">'.do_shortcode($content).'</div>'; }	
function one_sixth_last( $atts, $content = null ) {		return '<div class="columns one_sixth last">'.do_shortcode($content).'</div>'; }	

add_shortcode('one_sixth', 'one_sixth');
add_shortcode('one_sixth_last', 'one_sixth_last');	


/* Two Third */
$shortcode_attrs["two_third"]		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Two Third', 'intro' => 'YourTextHere');
$shortcode_attrs["two_third_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Two Third Last', 'intro' => 'YourTextHere');

function two_third( $atts, $content = null ) {			return '<div class="columns two_third">'.do_shortcode($content).'</div>'; }	
function two_third_last( $atts, $content = null ) {		return '<div class="columns two_third last">'.do_shortcode($content).'</div>'; }	

add_shortcode('two_third', 'two_third');
add_shortcode('two_third_last', 'two_third_last');


/* Three Fourth */
$shortcode_attrs["three_fourth"]		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Three Fourth', 'intro' => 'YourTextHere');
$shortcode_attrs["three_fourth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Three Fourth Last', 'intro' => 'YourTextHere');

function three_fourth( $atts, $content = null ) {		return '<div class="columns three_fourth">'.do_shortcode($content).'</div>'; }	
function three_fourth_last( $atts, $content = null ) {	return '<div class="columns three_fourth last">'.do_shortcode($content).'</div>'; }	

add_shortcode('three_fourth', 'three_fourth');
add_shortcode('three_fourth_last', 'three_fourth_last');


/* Four Fifth */
$shortcode_attrs["four_fifth"]		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Four Fifth', 'intro' => 'YourTextHere');
$shortcode_attrs["four_fifth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Four Fifth Last', 'intro' => 'YourTextHere');

function four_fifth( $atts, $content = null ) {			return '<div class="columns four_fifth">'.do_shortcode($content).'</div>'; }	
function four_fifth_last( $atts, $content = null ) {	return '<div class="columns four_fifth last">'.do_shortcode($content).'</div>'; }	

add_shortcode('four_fifth', 'four_fifth');	
add_shortcode('four_fifth_last', 'four_fifth_last');


/* Fie Sixth */
$shortcode_attrs["five_sixth"]		= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Five Sixth', 'intro' => 'YourTextHere');
$shortcode_attrs["five_sixth_last"]	= array( 'type' => 'm', 'group' => 'Columns', 'name' => 'Five Sixth Last', 'intro' => 'YourTextHere');

function five_sixth( $atts, $content = null ) {			return '<div class="columns five_sixth">'.do_shortcode($content).'</div>'; }	
function five_sixth_last( $atts, $content = null ) {	return '<div class="columns five_sixth last">'.do_shortcode($content).'</div>'; }

add_shortcode('five_sixth', 'five_sixth');	
add_shortcode('five_sixth_last', 'five_sixth_last');



/*********************************************************
	Register Shortcode "Clear"
**********************************************************/
$shortcode_attrs["cleardiv"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Clear Div' );
function cleardiv( $atts, $content = null ) {	return '<span class="cleardiv"></span>'; }
add_shortcode('cleardiv', 'cleardiv');


/*********************************************************
	Register Shortcode "Padding"
**********************************************************/
$shortcode_attrs["padding"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Padding Top & Bottom', 'attr' => array ( 'top' => '10', 'bottom' => '10'  ));

function padding( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["padding"]["attr"], $atts ) );
	
	$shortcode = '<span class="paddingdiv" style="padding-top:'.esc_attr($top).'px; padding-bottom:'.esc_attr($bottom).'px;"></span>';
	return $shortcode;
}
add_shortcode('padding', 'padding');	


/*********************************************************
	Register Shortcode "DEL"
**********************************************************/
$shortcode_attrs["del"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'Del', 'attr' => array ( 'color' => '#ff0000' ));

function del( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["del"]["attr"], $atts ) );
	
	$shortcode = '<del style="color:'.esc_attr($color).';">'.do_shortcode($content).'</del>';
	return $shortcode;
}
add_shortcode('del', 'del');






/*********************************************************
	Register Shortcode "CODE"
**********************************************************/
$shortcode_attrs["code"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'Code & Pre', 'intro' => 'YourTextHere', 'attr' => array ( 'width' => '100%' ));

function code( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["code"]["attr"], $atts ) );
	
	
	$shortcode = '<pre class="code" style="width:'.esc_attr($width).'"><code>'.do_shortcode($content).'</code></pre>';
	return $shortcode;
}
add_shortcode('code', 'code');



/*********************************************************
	Register Shortcode "HIGHLIGHT"
**********************************************************/
$shortcode_attrs["highlight"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'Highlight Text', 'intro' => 'YourTextHere', 'attr' => array ( 'bgcolor' => '#000', 'text_color' => '#fff' ));

function highlight( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["highlight"]["attr"], $atts ) );
	
	
	$shortcode = '<span class="highlight" style="color:'.esc_attr($text_color).'; background-color:'.esc_attr($bgcolor).';">'.do_shortcode($content).'</span>';
	return $shortcode;
}
add_shortcode('highlight', 'highlight');


/*********************************************************
	Register Shortcode "Divider"
**********************************************************/
$shortcode_attrs["divider_hr"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Divider HR', 'attr' => array ( 'top' => '20', 'bottom' => '20'  ));

function divider_hr( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["divider_hr"]["attr"], $atts ) );
	
	$shortcode = '<div class="divider_hr" style="padding-top:'.esc_attr($top).'px; margin-bottom:'.esc_attr($bottom).'px;">&nbsp;</div>';
	return $shortcode;
}
add_shortcode('divider_hr', 'divider_hr');


/*********************************************************
	Register Shortcode "Padding"
**********************************************************/
$shortcode_attrs["divider_hr_top"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Divider HR with TOP', 'attr' => array ( 'top' => '20', 'bottom' => '20'  ));

function divider_hr_top( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["divider_hr_top"]["attr"], $atts ) );
	
	$shortcode = '<div class="divider_with_top" style="padding-top:'.esc_attr($top).'px; margin-bottom:'.esc_attr($bottom).'px;"><a class="top" href="#top" style="top:'.esc_attr($top-6).'px;">[ &uarr; ]</a></div>';
	return $shortcode;
}
add_shortcode('divider_hr_top', 'divider_hr_top');


/*********************************************************
	Register Shortcode "UL Lists"
**********************************************************/
$shortcode_attrs["ul_list"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'UL List', 'attr' => array ( 'type' => 'arrow' ));

function ul_list( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["ul_list"]["attr"], $atts ) );
	
	$content = str_replace('<ul>', '<ul class="ul_list '.esc_attr($type).'">', do_shortcode($content)); 
	return $content;
}
add_shortcode('ul_list', 'ul_list');


$shortcode_attrs["ul_list_custom"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'UL List Custom', 'attr' => array ( 'icon_url' => 'http://..../icon16x16.png' ) );

function ul_list_custom( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["ul_list_custom"]["attr"], $atts ) );
	
	$content = str_replace( array('<ul>', '<li>'), array('<ul class="ul_list">', '<li style="background:url('.esc_attr($icon_url).') left center no-repeat;">'), do_shortcode($content)); 
	return $content;
}
add_shortcode('ul_list_custom', 'ul_list_custom');



/*********************************************************
	Register Shortcode "Image Frames"
**********************************************************/
$shortcode_attrs["image"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Image Frames', 'attr' => array ( 'thumbnail' => '', 'fullsize' => '', 'align' => 'left', 'alt' => '', 'border' => 'on', 'width' => '', 'height' => '' ));

function image( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["image"]["attr"], $atts ) );
	
	if( esc_attr($fullsize) != "" ) {
		$link_open	= '<a href="'.esc_attr($fullsize).'" class="image_frame_effect">';
		$link_close	= '</a>';
	}
	
	if(esc_attr($width) != "" && esc_attr($width) != "auto"){
	
		$css_w = " width:" . esc_attr($width) . "px;";

	}
	
	if(esc_attr($height) != "" && esc_attr($height) != "auto"){
	
		$css_h = " height:" . esc_attr($height) . "px;";

	}
	
	if(esc_attr($border) == "on"){
	
		$css_b = "image_frame";

	}
	
	
	return $link_open . '<span class="' . $css_b . ' image_frame_'.esc_attr($align).'"><img src="'.esc_attr($thumbnail).'" alt="'.esc_attr($alt).'" style="' . $css_w . $css_h . '"/></span>' . $link_close; 
	
}
add_shortcode('image', 'image');




/*********************************************************
	Register Shortcode "Position Frames"
**********************************************************/
$shortcode_attrs["position"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'Position', 'intro' => 'YourTextHere', 'attr' => array ( 'align' => 'center' ));

function position( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["position"]["attr"], $atts ) );
	
	return '<div class="align'.esc_attr($align).'">'.do_shortcode($content).'</div>'; 
	
}
add_shortcode('position', 'position');


/*********************************************************
	Register Shortcode "Contact Form"
**********************************************************/
$shortcode_attrs["contact_form"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Contact Form', 'attr' => array ( 'type' => 'vertical', 'width' => '500' ) );

function contact_form( $atts, $content = null ) {	

	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["contact_form"]["attr"], $atts ) );
	
	if( esc_attr($width) != "" ){
		
		$css_w = 'width:'.esc_attr($width).'px;';
		
	}
	
	if( esc_attr($type) == "vertical" ){
	
	$form_rand = rand(10, 100);
	
ob_start();
?>
<div class="contact_submit_form form_<?php echo $form_rand; ?>" style="<?php echo $css_w; ?>">
<div class="quickly_elements_label">
<input type="text" name="name" class="input" value="<?php echo get_option("field_name", "Name *"); ?>">
</div>
<div class="quickly_elements_label">
<input type="text" name="email" class="input" value="<?php echo get_option("field_email", "Email *"); ?>">
</div>
<div class="quickly_elements_label">
<input type="text" name="telephone" class="input" value="<?php echo get_option("field_telephone", "Telephone"); ?>">
</div>
<div class="quickly_elements_label">
<input type="text" name="subject" class="input" value="<?php echo get_option("field_subject", "Subject *"); ?>">
</div>
<div class="quickly_elements_label">
<textarea class="textarea" name="message" style="width:90%; height:65px;"><?php echo get_option("field_message", "Message *"); ?></textarea>
</div>
<div class="quickly_elements_label">
<div class="form_<?php echo $form_rand; ?> contact_submit">
<h6><span class="small_buttons"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_l small_left"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_r small_right"><?php echo get_option("button_send", "Send Message"); ?></span></span></span></h6>
</div>
<div class="quickly_form_message alignleft">&nbsp;</div>
</div>
</div>
<div class="cleardiv"></div>
<?php
	}

	if( esc_attr($type) == "horizontal" ){
	
	$form_rand = rand(10, 100);
	
ob_start();
?>
<div class="contact_submit_form form_<?php echo $form_rand; ?>" style="<?php echo $css_w; ?>">
<div class="quickly_elements_left">
<div class="quickly_elements_label">
<input type="text" name="name" class="input" value="<?php echo get_option("field_name", "Name *"); ?>">
</div>
<div class="quickly_elements_label">
<input type="text" name="email" class="input" value="<?php echo get_option("field_email", "Email *"); ?>">
</div>
<div class="quickly_elements_label">
<input type="text" name="telephone" class="input" value="<?php echo get_option("field_telephone", "Telephone"); ?>">
</div>
</div>
<div class="quickly_elements_right">
<div class="quickly_elements_label">
<input type="text" name="subject" class="input" value="<?php echo get_option("field_subject", "Subject *"); ?>">
</div>
<div class="quickly_elements_label">
<textarea class="textarea" name="message" style="width:90%; height:65px;"><?php echo get_option("field_message", "Message *"); ?></textarea>
</div>
</div>
<div class="quickly_elements_label">
<div class="form_<?php echo $form_rand; ?> contact_submit alignright">
<h6><span class="small_buttons"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_l small_left"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_r small_right"><?php echo get_option("button_send", "Send Message"); ?></span></span></span></h6>
</div>
<div class="quickly_form_message alignright">&nbsp;</div>
</div>
</div>
<div class="cleardiv"></div>
<?php
	}
	return ob_get_clean();
	
}
add_shortcode('contact_form', 'contact_form');



/*********************************************************
	Register Shortcode "Image Frames"
**********************************************************/
$shortcode_attrs["white_border"] = array( 'type' => 'm', 'group' => 'Others', 'name' => 'White Border' );

function white_border( $atts, $content = null ) {

	return '<div class="blog_boxes"><div class="box_image_shadow"><div class="box_image_inside">' . do_shortcode($content) . '</div></div></div>'; 
	
}
add_shortcode('white_border', 'white_border');



/*********************************************************
	Register Shortcode "Image Frames"
**********************************************************/
$shortcode_attrs["sitemap"] = array( 'type' => 's', 'group' => 'Others', 'name' => 'Sitemap', 'attr' => array ( 'pages' => 'true', 'posts' => 'true', 'posts_limit' => '30', 'categories' => 'true' ) );

function sitemap( $atts, $content = null ) {
	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["sitemap"]["attr"], $atts ) );
	
	$str 	= '<div class="sitemap">';
	
	if( esc_attr($pages) == "true" ){
	
		$str	.= '<h4>All the Pages</h4>';
		$str	.= '<ul class="ul_list arrow">';
		$mypages = get_pages('sort_order=asc');
		
		foreach($mypages as $page){
		
			$str	.= '<li><a href="' . get_page_link($page->ID) . '">' . $page->post_title . '</a></li>';
			
		}
		
		$str	.= '</ul>';
	
	}
	
	
	if( esc_attr($posts) == "true" ){
	
		$str	.= '<div class="maps">';
		$str	.= '<h4>All the Posts</h4>';
		$str	.= '<ul class="ul_list arrow">';
		
		$args['suppress_filters']	= true;
		$args['posts_per_page']		= esc_attr($posts_limit);
		$args['post_type'] 			= array("blog_mod", "portfolio_mod", "post");
		
		$the_query = new WP_Query( $args );

		while ( $the_query->have_posts() ) : $the_query->the_post();
		
			$str	.= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
			
		endwhile;
		
		wp_reset_postdata();
		
		$str	.= '</ul>';
		$str	.= '</div>';
	
	}
	
	
	if( esc_attr($categories) == "true" ){
	
		$str	.= '<div class="maps">';
		$str	.= '<h4>All the Categories</h4>';
		$str	.= '<ul class="ul_list arrow">';
		

		$terms	= get_terms("blog_type");
		
		foreach ($terms as $term) {
		
			$str	.= '<li><a href="' . get_term_link($term->slug, "blog_type") . '">' . $term->name . '</a></li>';
			
		}

		$terms	= get_terms("portfolio_type");
		
		foreach ($terms as $term) {
		
			$str	.= '<li><a href="' . get_term_link($term->slug, "portfolio_type") . '">' . $term->name . '</a></li>';
			
		}

		$terms	= get_terms("category");
		
		foreach ($terms as $term) {
		
			$str	.= '<li><a href="' . get_term_link($term->slug, "category") . '">' . $term->name . '</a></li>';
			
		}
		
		$str	.= '</ul>';
		$str	.= '</div>';

	}
	
	
	
	
	$str	.= "</div>";
	
	return $str; 
	
}
add_shortcode('sitemap', 'sitemap');



/*********************************************************
	Register Shortcode "BOXES"
**********************************************************/
$shortcode_attrs["the_box"] = array( 'type' => 'm', 'group' => 'Boxes', 'name' => 'The Box', 'intro' => 'YourTextHere', 'attr' => array ( 'width' => '100%', 'type' => 'simple' ));

function the_box( $atts, $content = null ) {	
	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["the_box"]["attr"], $atts ) );

	return '<span class="info_boxes '.esc_attr($type).'_box" style="width:'.esc_attr($width).';"><span class="info_text">'.do_shortcode($content).'</span></span>';
	
}
add_shortcode('the_box', 'the_box');


$shortcode_attrs["custom_box"] = array( 'type' => 'm', 'group' => 'Boxes', 'name' => 'Custom Box', 'intro' => 'YourTextHere', 'attr' => array ( 'width' => '100%', 'bgcolor' => '#fcfcfc', 'border_color' => '#f1f1f1', 'text_color' => '#747474', 'icon_url' => 'http://...../icon32x32.png' ));

function custom_box( $atts, $content = null ) {	
	global $shortcode_attrs;
	extract( shortcode_atts($shortcode_attrs["custom_box"]["attr"], $atts ) );
	
	if(esc_attr($icon_url) != "") {
		$background = 'background:url('.esc_attr($icon_url).') no-repeat scroll 15px center '.esc_attr($bgcolor).';';
	}else{
		$background = 'background-color:'.esc_attr($bgcolor).';';
		$padding	= 'style="padding-left:20px !important;"';
	}
	
	$shortcode = '<span class="info_boxes custom_box" style="'. $background .' width:'.esc_attr($width).'; border:1px solid '.esc_attr($border_color).'; color:'.esc_attr($color).';"><span class="info_text" '.$padding.'>'.do_shortcode($content).'</span></span>';
	return $shortcode;
}
add_shortcode('custom_box', 'custom_box');

?>