<?php


function twentyten_widgets_init() {

	$get_sidebars = get_option("option_sidebars");
	
	
	if($get_sidebars != ""){
	
		preg_match_all("/\{(.*?)\}/", $get_sidebars, $sidebars, PREG_SET_ORDER);
		
		foreach($sidebars as $key => $sidebar){
			
			$pid = $sidebars[$key][1];
			
			if ( get_post( $pid ) != "") { 

				register_sidebar( array(
					'name' => __( "( ". get_the_title($sidebars[$key][1])." ) Sidebar", 'fringe_tech' ),
					'id' => 'sidebar_'.$sidebars[$key][1],
					'description' => __( 'Drag widgets for '.get_the_title($sidebars[$key][1]). ' sidebar.', 'fringe_tech' ),
					'before_widget' => '<div class="page_sidebar_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h4>',
					'after_title' => '</h4>',
				) );
			
			}
		}
	
	}

	register_sidebar( array(
		'name' => __( "Global Sidebar (All of Pages)", 'fringe_tech' ),
		'id' => 'sidebar_all',
		'description' => __( 'Drag widgets for all of pages sidebar.', 'fringe_tech' ),
		'before_widget' => '<div class="page_sidebar_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( "Single Page Sidebar", 'fringe_tech' ),
		'id' => 'single_sidebar_all',
		'description' => __( 'Drag widgets for all of single page sidebar.', 'fringe_tech' ),
		'before_widget' => '<div class="page_sidebar_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
	
	
	register_sidebar( array(
		'name' => __( "Shortcoce Blog Sidebar", 'fringe_tech' ),
		'id' => 'shortcode_blog',
		'description' => __( 'Drag widgets for all of Blog Shortcode Sidebar.', 'fringe_tech' ),
		'before_widget' => '<div class="page_sidebar_widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

	
	
	$footer_type = get_clean_option("footer_type", "model1");
	
	if($footer_type == ""){ $footer_type = "model1"; }
	
	switch ($footer_type) {
	
		case "model1":
			$num = 4;
		break;
		
		case "model2":
		case "model4":
		case "model9":
		case "model10":
			$num = 3;
		break;
		
		case "model3":
		case "model5":
		case "model6":
			$num = 2;
		break;
		
		case "model7":
			$num = 1;
		break;
		
		case "model8":
			$num = 5;
		break;
		
	}
	
	for($i = 1; $i < $num+1; $i++){
		register_sidebar( array(
			'name' => __( "Footer Widget (" . $i . ")", 'fringe_tech' ),
			'id' => 'sidebar_footer_widget_'.$i,
			'description' => __( 'Drag widgets for footer widget (' . $i . ')', 'fringe_tech' ),
			'before_widget' => '<div class="footer_list %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="title"><h5>',
			'after_title' => '</h5></div>',
		) );
	}
	

}

add_action( 'widgets_init', 'twentyten_widgets_init' );
?>