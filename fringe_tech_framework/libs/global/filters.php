<?php
function my_wp_nav_menu_args( $args = '' ){

	$args['show_home'] = get_option("home_link_name", "Home");
	$args['sort_column'] = "menu_order";
	$args['menu_class'] = "header_menu";
	$args['container_class'] = "header_menu";
	
	return $args;
	
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

function wp_nav_menu_remove_container( $menu ){
   return $menu = preg_replace( array('/header_menu/', '/<div class="(.*?)">/'), array('','<div class="header_menu">'), $menu );
}
add_filter( 'wp_nav_menu', 'wp_nav_menu_remove_container' );



function wp_nav_remove_title( $menu ){

	if( get_option("link_titles", "off") == "off"){
		$menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );
	}
	return $menu;
}
add_filter( 'wp_page_menu', 'wp_nav_remove_title' );


function media_shortcode_button($id) {
   echo "<a href=\"". get_template_directory_uri() ."/". F_PATH . "/libs/global/insert_shortcodes.php?TB_iframe=true&amp;width=640&amp;height=500\" title=\"Insert a Shortcode\" class=\"thickbox\"><img alt=\"Insert a Shortcode\" src=\"". get_template_directory_uri() ."/". F_PATH ."/libs/images/shortcode.png\"></a>";
}
add_action('media_buttons', 'media_shortcode_button', 15);


function remove_footer_admin () {
	echo get_option("admin_copyright", "Custom text | Copyright &copy; message");
}
add_filter('admin_footer_text', 'remove_footer_admin');


function change_wp_login_title() {
	echo 'Powered by ' . get_option('blogname');
}
add_filter('login_headertitle', 'change_wp_login_title');



function change_wp_login_url() {
	echo home_url();
}

add_filter('login_headerurl' , 'change_wp_login_url');



function change_login_head(){
	echo '<style type="text/css">h1 a{background:url('.get_option("admin_logo", T_URI."/images/dummy/admin_logo.png").') no-repeat scroll 50% 50% transparent; display:block; text-indent:-9999px;  margin: 20px auto}</style>';
}
add_action("login_head", "change_login_head");



function code_get_calendar($initial = true, $echo = true) {

	global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;

	$post_types = "'post', 'blog_mod', 'portfolio_mod'";

	$cache = array();
	$key = md5( $m . $monthnum . $year . "blog_mod portfolio_mod post" );

	if ( !is_array($cache) )
		$cache = array();

	if ( !$posts ) {
		$gotsome = $wpdb->get_var("SELECT 1 as test FROM $wpdb->posts WHERE post_type IN ( $post_types ) AND post_status = 'publish' LIMIT 1");
		if ( !$gotsome ) {
			$cache[ $key ] = '';
			wp_cache_set( 'get_calendar', $cache, 'calendar' );
			return;
		}
	}

	if ( isset($_GET['w']) )
		$w = ''.intval($_GET['w']);

	$week_begins = intval(get_option('start_of_week'));

	if ( !empty($monthnum) && !empty($year) ) {
		$thismonth = ''.zeroise(intval($monthnum), 2);
		$thisyear = ''.intval($year);
	} elseif ( !empty($w) ) {
		$thisyear = ''.intval(substr($m, 0, 4));
		$d = (($w - 1) * 7) + 6;
		$thismonth = $wpdb->get_var("SELECT DATE_FORMAT((DATE_ADD('{$thisyear}0101', INTERVAL $d DAY) ), '%m')");
	} elseif ( !empty($m) ) {
		$thisyear = ''.intval(substr($m, 0, 4));
		if ( strlen($m) < 6 )
				$thismonth = '01';
		else
				$thismonth = ''.zeroise(intval(substr($m, 4, 2)), 2);
	} else {
		$thisyear = gmdate('Y', current_time('timestamp'));
		$thismonth = gmdate('m', current_time('timestamp'));
	}

	$unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
	$last_day = date('t', $unixmonth);

	$previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date < '$thisyear-$thismonth-01'
		AND post_type IN ( $post_types ) AND post_status = 'publish'
			ORDER BY post_date DESC
			LIMIT 1");
	$next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date > '$thisyear-$thismonth-{$last_day} 23:59:59'
		AND post_type IN ( $post_types ) AND post_status = 'publish'
			ORDER BY post_date ASC
			LIMIT 1");

	$calendar_caption = _x('%1$s %2$s', 'calendar caption');
	$calendar_output = '<table id="wp-calendar" summary="' . esc_attr__('Calendar') . '">
	<caption>' . sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y', $unixmonth)) . '</caption>
	<thead>
	<tr>';

	$myweek = array();

	for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
	}

	foreach ( $myweek as $wd ) {
		$day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
		$wd = esc_attr($wd);
		$calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
	}

	$calendar_output .= '
	</tr>
	</thead>

	<tfoot>
	<tr>';

	if ( $previous ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev"><a href="' . get_month_link($previous->year, $previous->month) . '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($previous->month), date('Y', mktime(0, 0 , 0, $previous->month, 1, $previous->year)))) . '">&laquo; ' . $wp_locale->get_month_abbrev($wp_locale->get_month($previous->month)) . '</a></td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
	}

	$calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';

	if ( $next ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next"><a href="' . get_month_link($next->year, $next->month) . '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($next->month), date('Y', mktime(0, 0 , 0, $next->month, 1, $next->year))) ) . '">' . $wp_locale->get_month_abbrev($wp_locale->get_month($next->month)) . ' &raquo;</a></td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
	}

	$calendar_output .= '
	</tr>
	</tfoot>

	<tbody>
	<tr>';

	$dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
		FROM $wpdb->posts WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00'
		AND post_type IN ( $post_types ) AND post_status = 'publish'
		AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59'", ARRAY_N);
	if ( $dayswithposts ) {
		foreach ( (array) $dayswithposts as $daywith ) {
			$daywithpost[] = $daywith[0];
		}
	} else {
		$daywithpost = array();
	}

	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'camino') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false)
		$ak_title_separator = "\n";
	else
		$ak_title_separator = ', ';

	$ak_titles_for_day = array();
	$ak_post_titles = $wpdb->get_results("SELECT ID, post_title, DAYOFMONTH(post_date) as dom "
		."FROM $wpdb->posts "
		."WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00' "
		."AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59' "
		."AND post_type IN ( $post_types ) AND post_status = 'publish'"
	);
	if ( $ak_post_titles ) {
		foreach ( (array) $ak_post_titles as $ak_post_title ) {

				$post_title = esc_attr( apply_filters( 'the_title', $ak_post_title->post_title, $ak_post_title->ID ) );

				if ( empty($ak_titles_for_day['day_'.$ak_post_title->dom]) )
					$ak_titles_for_day['day_'.$ak_post_title->dom] = '';
				if ( empty($ak_titles_for_day["$ak_post_title->dom"]) ) 
					$ak_titles_for_day["$ak_post_title->dom"] = $post_title;
				else
					$ak_titles_for_day["$ak_post_title->dom"] .= $ak_title_separator . $post_title;
		}
	}


	$pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
	if ( 0 != $pad )
		$calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

	$daysinmonth = intval(date('t', $unixmonth));
	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
		if ( isset($newrow) && $newrow )
			$calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
		$newrow = false;

		if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) )
			$calendar_output .= '<td id="today">';
		else
			$calendar_output .= '<td>';

		if ( in_array($day, $daywithpost) ) 
				$calendar_output .= '<a href="' . get_day_link($thisyear, $thismonth, $day) . "\" title=\"" . esc_attr($ak_titles_for_day[$day]) . "\">$day</a>";
		else
			$calendar_output .= $day;
		$calendar_output .= '</td>';

		if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
			$newrow = true;
	}

	$pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
	if ( $pad != 0 && $pad != 7 )
		$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

	$calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table>";

	$cache[ $key ] = $calendar_output;
	wp_cache_set( 'get_calendar', $cache, 'calendar' );

	remove_filter( 'get_calendar' , 'code_get_calendar_filter' );
	$output = apply_filters( 'get_calendar',  $calendar_output );
	add_filter( 'get_calendar' , 'code_get_calendar_filter' );

	if ( $echo )
		echo $output;
	else
		return $output;

}



function code_get_calendar_filter( $content ) {
  $output = code_get_calendar( true , false );
  return $output;
}
add_filter( 'get_calendar' , 'code_get_calendar_filter' , 10 , 2 );


add_filter('the_content', 'add_lightbox_rel', 12);
add_filter('widget_text', 'add_lightbox_rel', 12);
add_filter('get_comment_text', 'add_lightbox_rel');
function add_lightbox_rel ($content){  
	global $post;
	$pattern = array("/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i", "/\?type=link&amp;target=(_blank|_self)/i");
    $replacement = array('<a$1href=$2$3.$4$5 rel="prettyPhoto['.$post->ID.']"$6>$7</a>', '');
    $content = preg_replace($pattern, $replacement, $content);
	
    return $content;
}
?>