<?php

if( $home_models != "") {

	add_action('admin_menu', 'add_home_design_menu');
	function add_home_design_menu() {

		global $menu;
		
		$design_position = 9;
		if( !empty ($menu[$design_position]) ){ $design_position = null; }
		
		$design_icon = get_template_directory_uri().'/'.F_PATH.'/libs/images/design_icon.png';
		
		add_menu_page('Home Design', 'Home Design', 'manage_options', 'home_design', 'home_design', $design_icon, $design_position);
		
	}

}


$action = @$_REQUEST["do"];

switch($action){

	
	case 'update_design':
		
		
		$s_value	= trim( get_the_requests($_REQUEST["options"]) );
		
		$old_model	= get_the_value("home_model", get_option("home_types") );
		$new_model	= get_the_value("home_model", $s_value );
		
	
		
		if($old_model == $new_model){
		
			update_option("home_types", $s_value);
			
		}else{
		
			if( $new_model == "boxes" ){
			
				$c_val = "{home_model=boxes}{w_width=300}{w_column=3}{w_padding=30}{w_limit=3}";
				
			} else if( $new_model == "recently" ){
			
				$c_val = "{home_model=recently}{wb_all=on}{wb_recently_intro=on}{wb_width=170}{wb_height=110}{wb_column=4}{wb_padding=13}{wb_limit=4}{wb_id=}{wb_recently_added=Recently Added}{wb_recently_content=Some Text}";
				
			}else if( $new_model == "recently_and_boxes" ){
			
				$c_val = "{home_model=recently_and_boxes}{wb_all=on}{wb_recently_intro=on}{w_width=300}{w_column=3}{w_padding=30}{w_limit=3}{wb_width=170}{wb_height=110}{wb_column=4}{wb_padding=13}{wb_limit=4}{wb_id=}{wb_recently_added=Recently Added}{wb_recently_content=Some Text}";
				
			} else {
			
				$c_val = "{home_model=" . $new_model . "}";
				
			}
			
			update_option("home_types", $c_val);
		
		}
		
		$mysqlresult = "Home Design Settings has been updated.";
		
		if($_REQUEST["home_custom"] != ""){
		
			$mysqlresult = "Custom Home has been updated.";
		
		}
		
	break;	
	

	case 'edit_box':
		
		$s_value	= trim( get_the_requests($_REQUEST["options"]) );
		
		$w_id = $_REQUEST["box_id"];
		update_option("home_box_" .$w_id, $s_value);
		
		$mysqlresult = "Box <strong>(" . $w_id . ")</strong> has been updated.";
		
	break;
	
	case 'home_custom':
		
		$s_value	= trim( get_the_requests($_REQUEST["options"]) );
		update_option("home_custom", $s_value);
		$mysqlresult = "Custom Home has been updated.";
		
	break;
	
}

function home_design(){

	global $mysqlresult, $home_models;

	echo '<div class="wrap"><div class="icon32" id="icon-edit"><br /></div>';
	
	$page_id = $_GET["id"];
	
	if($page_id != ""){
	
		echo "<h2>Box (" . $page_id . ") Editing</h2>";
		
	}else{
	
		echo "<h2>Home Page Design Manager</h2>";
		
	}

	if ($mysqlresult != ""){
		echo '<div class="updated"><p>'. $mysqlresult .'</p></div>';
	}
	

	$home_model_first = explode(",", $home_models);
	$w_value = get_option("home_types", "{home_model=" . $home_model_first[0] . "}");
?>


<div class="has-right-sidebar" id="poststuff" style="margin-top:18px">

	<div class="inner-sidebar">
	
		<form method="post" action="?page=home_design">
	
			<div class="postbox" id="submitdiv">
			
				<h3 style="cursor:default;"><span>Choose a Home Page Model</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv">

						<?php 
							$optionName = "home_model";
							$model = htmlSafe( get_the_value($optionName, $w_value ) );
							render_item('checkbox', $optionName, $home_models, $model, false, false, 'home_model' ); 
						?>

					</div>

				</div>
				
			</div>
			
			<?php if( $model == "boxes" || $model == "recently_and_boxes"){ ?>
			
			
			<div class="postbox" id="submitdiv">
			
				<h3 style="cursor:default;"><span>Boxes Settings</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv">
					
						<div class="option_holder">
							
							<div class="option_label"><strong>Box width</strong></div>
							<?php 
								$optionName = "w_width";
								render_item('slider_ui', $optionName, '300,960,1,px,width', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">
							
							<div class="option_label"><strong>Box Columns</strong></div>
							<?php 
								$optionName = "w_column";
								render_item('slider_ui', $optionName, '3,10,1,col.', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">

							<div class="option_label"><strong>Box Padding</strong></div>
							<?php 
								$optionName = "w_padding";
								render_item('slider_ui', $optionName, '30,100,1,px', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">

							<div class="option_label"><strong>Box Limit</strong></div>
							<?php 
								$optionName = "w_limit";
								render_item('slider_ui', $optionName, '3,25,1,item', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<?php } ?>
			
			<?php if ( $model == "recently_and_boxes" || $model == "recently"){ ?>
			<div class="postbox" id="submitdiv">
			
				<h3 style="cursor:default;"><span>Recently Boxes Settings</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv">
					
						<div class="option_holder">
							
							<div class="option_label"><strong>Box width</strong></div>
							<?php 
								$optionName = "wb_width";
								render_item('slider_ui', $optionName, '170,960,1,px,width', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">
							
							<div class="option_label"><strong>Box height</strong></div>
							<?php 
								$optionName = "wb_height";
								render_item('slider_ui', $optionName, '110,960,1,px,height', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">
							
							<div class="option_label"><strong>Box Columns</strong></div>
							<?php 
								$optionName = "wb_column";
								render_item('slider_ui', $optionName, '4,10,1,col.', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">

							<div class="option_label"><strong>Box Padding</strong></div>
							<?php 
								$optionName = "wb_padding";
								render_item('slider_ui', $optionName, '13,100,1,px', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

						<div class="option_holder">

							<div class="option_label"><strong>Box Limit</strong></div>
							<?php 
								$optionName = "wb_limit";
								render_item('slider_ui', $optionName, '4,25,1,item', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
							
						</div>

					</div>

				</div>

			</div>
			
			<div class="postbox" id="submitdiv">
				<h3 style="cursor:default;"><span>Choose a recently mod categories</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv"  style="display:table;">
						<?php 
							$optionName = "wb_id";
							render_item('select', $optionName, '[categories],[all],[desc],[id]', htmlSafe( get_the_value($optionName, $w_value ) ), "multiple=\"multiple\" style=\"width:200px; height:100px;\"" ); 
						?>
						
						
						<div class="cleardiv"></div>
						
						<div class="option_holder">

								<div class="option_label"><strong>Do you want to Recently "ALL" ?</strong></div>
								<?php 
									$optionName = "wb_all";
									render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
								?>
								
						</div>
					
					</div>

				</div>
				
			</div>
			
			
			
			<div class="postbox" id="submitdiv">
				<h3 style="cursor:default;"><span>Recently added & Intro settings</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv"  style="display:table;">
					
						<div class="option_holder">
							<div class="option_label"><strong>Title "Recently added"</strong></div>
							<?php 
								$optionName = "wb_recently_added";
								render_item('input', $optionName, 'Recently Added', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
							?>
						</div>

						<div class="option_holder">
							<div class="option_label"><strong>Content of "Recently added"</strong></div>
							<?php 
								$optionName = "wb_recently_content";
								render_item('textarea', $optionName, 'Some Text', htmlSafe( get_the_value($optionName, $w_value ) ), "style=\"height:250px;\"" ); 
							?>
						</div>
						
						<div class="option_holder">

								<div class="option_label"><strong>Do you want to Recently added column ?</strong></div>
								<?php 
									$optionName = "wb_recently_intro";
									render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $w_value ) ) ); 
								?>
								
						</div>
					
					</div>

				</div>
				
			</div>
			<?php } ?>
			
			
			<?php if( $model == "portfolio" ){ ?>
			
			<div class="postbox" id="submitdiv">
			
				<h3 style="cursor:default;"><span>Choose a portfolio category</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv"  style="display:table;">
					<?php 
						$optionName = "blog";
						render_item('select', $optionName, '[categories],[portfolio_type],[desc],[id]', htmlSafe( get_the_value($optionName, $w_value ) ), "multiple=\"multiple\" style=\"width:200px; height:100px;\"" ); 
					?>
					</div>

				</div>
				
			</div>
			
			<?php } ?>
			
			<?php if( $model == "blog" ){ ?>
			
			<div class="postbox" id="submitdiv">
			
				<h3 style="cursor:default;"><span>Choose a blog category</span></h3>
				
				<div class="inside">
				
					<div class="categorydiv"  style="display:table;">
					<?php 
						$optionName = "blog";
						render_item('select', $optionName, '[categories],[blog_type],[desc],[id]', htmlSafe( get_the_value($optionName, $w_value ) ), "multiple=\"multiple\" style=\"width:200px; height:100px;\"" ); 
					?>
					</div>

				</div>
				
			</div>
			
			<?php } ?>
			<!-- End Home Model Settings Panels -->
			
			<!-- Begin Submit Buttons -->
			<div class="postbox" id="submitdiv">
				
				<h3 style="height:25px; cursor:default;">
				
					<?php if($_GET["id"] != ""){ ?>
						<span class="go_back_save"><a href="javascript:history.back();" class="button">Go Back</a></span>		
					<?php } ?>
					
					<input type="hidden" value="update_design" name="do">
					
					<span style="float:right;">
						<input type="submit" value="Save Changes" class="button-primary" name="save">
					</span>
					
				</h3>
				
			</div>
		
		</form>
		
	</div>
	
	
	
	<?php if( $model == "" || $model == "nothing") { ?>
	
	No selected any model, <strong>huh, why ?</strong>
	
	<?php } ?> 

	
	<?php if( ( $model == "boxes" || $model == "recently_and_boxes" ) && $_GET["id"] == "" ){ ?>
	
	<div id="post-body">
		
		<div id="post-body-content">
			
			<div style="width:100%; margin:-10px; margin-bottom:20px;">
				
				<table width="100%" cellspacing="10" cellpadding="0" class="table_home_design">
				
					<tr>
						<?php

							$total_boxes	= htmlSafe( get_the_value("w_limit", $w_value) );
							$column_boxes	= htmlSafe( get_the_value("w_column", $w_value) );
							
							$column_boxes	== "" ? $column_boxes = 3 : "";
							$total_boxes	== "" ? $total_boxes = 3 : "";
							$column_boxes	== 0  ? $column_boxes = 1 : "";
							
							$col = 0;
							for($i = 1; $i < ( $total_boxes+1 ); $i++){
							
							if($col == $column_boxes){ echo "</tr><tr>"; $col = 0; }
						?>
						<td class="option_rows">
							<h2>Box (<?php echo $i; ?>)</h2>
							<a href="?page=home_design&id=<?php echo $i; ?>" class="icon_edit">&nbsp;</a>
						</td>
						<?php $col++; } ?>
					</tr>
					
				</table>
				
			</div>

			<div class="option_holder">

				<span class="available_formats"><strong>Click for columns settings (how-to):</strong></span>
				
				<div style="clear:both; margin-top:10px; float:left; display:table;">
				
					<ul class="hows_to">
						<li>&bull; 1 Box 1 Column<span>Pre-Configured Settings<br /><strong>width:</strong> 960 <strong>column(s):</strong> 1 <strong>padding:</strong> 0 <strong>limit:</strong> 1 <br /><a href="javascript:void(0);" class="set_auto_box" title="1-box">Click here to use these settings</a></span></li>
						<li>&bull; 2 Boxes 2 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 460  <strong>column(s):</strong> 2 <strong>padding:</strong> 40 <strong>limit:</strong> 2 <br /><a href="javascript:void(0);" class="set_auto_box" title="2-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 3 Boxes 3 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 300  <strong>column(s):</strong> 3 <strong>padding:</strong> 30 <strong>limit:</strong> 3 <br /><a href="javascript:void(0);" class="set_auto_box" title="3-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 4 Boxes 2 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 460  <strong>column(s):</strong> 2 <strong>padding:</strong> 40 <strong>limit:</strong> 4 <br /><a href="javascript:void(0);" class="set_auto_box" title="4-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 5 Boxes 5 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 180  <strong>column(s):</strong> 5 <strong>padding:</strong> 15 <strong>limit:</strong> 5 <br /><a href="javascript:void(0);" class="set_auto_box" title="5-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 6 Boxes 3 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 300  <strong>column(s):</strong> 3 <strong>padding:</strong> 30 <strong>limit:</strong> 6 <br /><a href="javascript:void(0);" class="set_auto_box" title="6-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 9 Boxes 3 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 300  <strong>column(s):</strong> 3 <strong>padding:</strong> 30 <strong>limit:</strong> 9 <br /><a href="javascript:void(0);" class="set_auto_box" title="9-boxes">Click here to use these settings</a></span></li>
						<li>&bull; 12 Boxes 3 Columns<span>Pre-Configured Settings<br /><strong>width:</strong> 300 <strong>column(s):</strong> 3 <strong>padding:</strong> 30 <strong>limit:</strong> 12 <br /><a href="javascript:void(0);" class="set_auto_box" title="12-boxes">Click here to use these settings</a></span></li>
					</ul>
					
					
				</div>

				<div style="clear:both; float:left; font-size:11px;">
					<em>Note: You can add unlimited box, also you can change settings 100% flexible, just you don't forget your max-width 960px :)</em>
				</div>
				
				<div class="hows_to_infos">
					<div class="hows_to_infos_bg"></div>
				</div>

			</div>
		
		</div>
		
	</div>
	<?php } ?>

	<?php 
	if( ( $model == "boxes" || $model == "recently_and_boxes" ) && $_GET["id"] != "" ) {
		$get_values = get_option("home_box_".$_GET["id"]);
	?>
	<form method="post" action="?page=home_design">
	
		<div id="post-body">
		
			<div id="post-body-content">
			  
				<div id="titlediv">
					<div id="titlewrap">
						<div class="option_label"><strong>Box Title</strong> <em>(optional)</em></div>
						<?php 
							$optionName = "w_name";
							render_item("input", $optionName, htmlSafe( get_the_value($optionName, $get_values ) ) ); 
						?>
					</div>
				</div>
			   
				<div class="option_label"><strong>Box Description</strong> <em>(optional)</em></div>
				<?php 
					$optionName = "w_desc";
					render_item("tinymce", "w_desc", htmlSafe( get_the_value($optionName, $get_values ) ), false, "style=\"height:250px;\"");
				?>

				<div class="postbox" style="margin-top:20px;">
				  
					<h3><span>Box Custom Fields</span></h3>
					
					<div class="inside">

						<div class="option_holder">
						
							<div class="option_label"><strong>Would you like "Title Icon" ?</strong></div>
							<?php 
								$optionName = "w_icon";
								render_item("checkbox", $optionName, "box_icon1,box_icon2,box_icon3,box_icon4,box_icon5,box_icon6,box_icon7,box_icon8,box_icon9,box_icon10,box_icon11,box_icon12,box_icon13,box_icon14,box_icon15,box_icon16,box_icon17,box_icon18,box_icon19,box_icon20,box_icon21,box_icon22,box_icon23,box_icon24,box_icon25,box_icon26,box_icon27,box_icon28", get_the_value($optionName, $get_values), false, false, "w_icon"); 
							?>
						
						</div>
				
					</div>
				  
				</div>
				
				<div class="postbox" style="margin-top:20px;">
					
					<h3 style="height:25px; cursor:default;">
					
						<span class="go_back_save"><a href="javascript:history.back();" class="button">Go Back</a></span>		
						
						<input type="hidden" value="edit_box" name="do">
						<input type="hidden" value="<?php echo $_GET["id"]; ?>" name="box_id">

						<span style="float:right;">
							<input type="submit" value="Save Changes" class="button-primary" name="save">
						</span>
						
					</h3>
					
				</div>
				

			</div>
	  
		</div>
	
	</form>
	<?php } ?>
	
	<?php if( $model == "recently" ) { ?>
	
	Your choose is <strong>"just recently"</strong> model. Choose your settings from right side :)
	
	<?php } ?>

	<?php if( $model == "blog" ) { ?>
	
	Your choose is <strong>"blog on home"</strong> model. Just choose a blog category from right side :)
	
	<?php } ?>
	
	<?php if( $model == "portfolio" ) { ?>
	
	Your choose is <strong>"portfolio on home"</strong> model. Just choose a portfolio category from right side :)
	
	<?php } ?>
	
	<?php if( $model == "custom" ) {
		$get_home_custom = get_option("home_custom");
	?>
	<form method="post" action="?page=home_design">
	
		<div id="post-body">
		
			<div id="post-body-content">
			  
				<div id="titlediv">
					<div id="titlewrap">
						<div class="option_label"><strong>Custom Home Title</strong> <em>(optional)</em></div>
						<?php 
							$optionName = "w_name";
							render_item("input", $optionName, htmlSafe( get_the_value($optionName, $get_home_custom ) ) ); 
						?>
					</div>
				</div>
			   
				<div class="option_label"><strong>Custom Home Content</strong> <em>(optional)</em></div>
				<?php 
					$optionName = "w_desc";
					render_item("tinymce", "w_desc", htmlSafe( get_the_value($optionName, $get_home_custom ) ), false, "style=\"height:400px;\"");
				?>
				
				
				<div class="postbox" style="margin-top:20px;">
			
					<h3 style="height:25px; cursor:default;">
					
						<span class="go_back_save"><a href="javascript:history.back();" class="button">Go Back</a></span>		
						
						<input type="hidden" value="home_custom" name="do">
					
						<span style="float:right;">
							<input type="submit" value="Save Changes" class="button-primary" name="save">
						</span>
						
					</h3>
						
				</div>
			
			</div>
			
		</div>
	
	</form>
	<?php } ?>

</div>

</div>
<?php } ?>