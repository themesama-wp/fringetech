<?php
function install_cp_db(){

	global $wpdb, $category_table, $item_table, $taxonomy_table, $cp_db_version;


	if( $wpdb->get_var("SHOW TABLES LIKE '". $category_table ."'") != $category_table ){
		
		$sql_exec = "CREATE TABLE " . $category_table . " (
		  id bigint(9) NOT NULL AUTO_INCREMENT,
		  s_id int(11) NOT NULL,
		  c_name varchar(255) NOT NULL,
		  c_desc text NOT NULL,
		  c_value text NOT NULL,
		  orderby int(11) NOT NULL,
		  PRIMARY KEY (id),
		  UNIQUE KEY id (id)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql_exec);
	
	}
	

	if( $wpdb->get_var("SHOW TABLES LIKE '". $item_table ."'") != $item_table ){
		
		$sql_exec = "CREATE TABLE " . $item_table . " (
		  id bigint(9) NOT NULL AUTO_INCREMENT,
		  c_id varchar(255) NOT NULL,
		  s_id int(11) NOT NULL,
		  s_name varchar(255) NOT NULL,
		  s_desc text NOT NULL,
		  s_value text NOT NULL,
		  orderby int(11) NOT NULL,
		  PRIMARY KEY (id),
		  UNIQUE KEY id (id)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql_exec);
		
	}
	

	if( $wpdb->get_var("SHOW TABLES LIKE '". $taxonomy_table ."'") != $taxonomy_table ){
		
		$sql_exec = "CREATE TABLE " . $taxonomy_table . " (
		  id bigint(9) NOT NULL AUTO_INCREMENT,
		  t_id int(11) NOT NULL,
		  t_value text NOT NULL,
		  PRIMARY KEY (id),
		  UNIQUE KEY id (id)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql_exec);
	
	}
	

	update_option("cp_db_version", $cp_db_version);
}


if( get_option("cp_db_version") != $cp_db_version ) { install_cp_db(); };



function makeSafe($text, $mysql=false) {

	if(get_magic_quotes_gpc()) {
		$text = stripslashes($text);
	}

	if($mysql) {
		$text = mysql_real_escape_string($text);
	} else {
		$text = addslashes($text);
	}

	return $text;

}


function htmlSafe($text) {

	$text = stripslashes($text);
	return $text;

}

function get_the_value($name, $content, $default=null, $print=null){

	if($content != ""){
	
		preg_match_all("~{". $name ."=(.*?)}~is", $content, $matches);
		$data = $matches[1][0];
		
	}else{
	
		$data = $default;
		
	}
	
	if($print == true) { echo $data; } else { return $data; }
	
}


function get_the_requests($requests, $checks = array()){

	$data = "";
	foreach($requests as $key => $option) {
		
		if ( !in_array( $key, $checks ) ) {
		
			if(!is_array($option)){
				
				$data	.=  "{" . $key . "=" . $option . "}";
				
			} else {
				
				foreach($requests[$key] as $option_name => $op) {
				
					if(is_array($option)) {
						
						$vo = array();
						foreach($op as $k => $v){
							
							if($v != ""){
								
								$vo[] = $v;
								
							}
							
						}
						
						$data	.= "{". $option_name . "=" . implode(",", $vo)."}";
						
					}else{
					
						$data	.= "{". $option_name . "=" . $op."}";
						
					}					
					
					
				}
				
			}
			
		}
		
	}

	return $data;
	
}




function implode_categories($requests){
	
	$data = "";
	foreach($requests as $option_name => $op) {

		$categories = array();
		
		foreach($op as $k => $v){
			
			if($v != ""){
				$categories[]	=  $v;
			}
			
		}
	
	}
	
	$data =  implode(",", $categories);
	
	return $data;
	
}


function move_item($id, $mode, $table, $column = null, $order, $filter_id) {

	global $wpdb;
	
	$id		= makeSafe($id);
	$mode	= makeSafe($mode);
	$table	= makeSafe($table);
	$column	= makeSafe($column);

	if($order == "DESC"){
	
		$order_up = "ASC";
		$icon_up = ">";
		$order_down = "DESC";
		$icon_down = "<";
		
	}else{
	
		$order_up = "DESC";
		$icon_up = "<";
		$order_down = "ASC";
		$icon_down = ">";
		
	}

	if($mode != ""){

		$count	= $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ". $table ." WHERE id = %d", $id));
		$f_id	= $wpdb->get_var($wpdb->prepare("SELECT ". $filter_id ." FROM ". $table ." WHERE id = %d", $id));
		
		if($count > 0) {
	
			$thisone = $wpdb->get_row("SELECT id, " . $column . " FROM " . $table . " WHERE id = '" . $id . "' LIMIT 1");

			
			switch($mode) {

				case "up":

					
					$count = $wpdb->get_var($wpdb->prepare("SELECT id, " . $column . " FROM ". $table ." WHERE " . $column . " " . $icon_up . " ".$thisone->$column." ORDER BY " . $column . " " . $order_up . " LIMIT 1", ''));

					if($count > 0) {

						$upper = $wpdb->get_row("SELECT id, " . $column . " FROM ". $table ." WHERE ". $filter_id ." = '" . $f_id . "' AND " . $column . " " . $icon_up . " ".$thisone->$column." ORDER BY " . $column . " " . $order_up . " LIMIT 1");

						$update = $wpdb->query("UPDATE ". $table ." SET " . $column . " = ".$upper->$column." WHERE id = ".$thisone->id." LIMIT 1");
						$update = $wpdb->query("UPDATE ". $table ." SET " . $column . " = ".$thisone->$column." WHERE id = ".$upper->id." LIMIT 1");
						
					}

				break;

				case "down":

					$count = $wpdb->get_var($wpdb->prepare("SELECT id, " . $column . " FROM ". $table ." WHERE " . $column . " " . $icon_down . " ".$thisone->$column." ORDER BY " . $column . " " . $order_down . " LIMIT 1", ''));
					if($count > 0) {

						$upper = $wpdb->get_row("SELECT id, " . $column . " FROM ". $table ." WHERE ". $filter_id ." = '" . $f_id . "' AND " . $column . " " . $icon_down . " ".$thisone->$column." ORDER BY " . $column . " " . $order_down . " LIMIT 1");
						
						$update = $wpdb->query("UPDATE ". $table ." SET " . $column . " = ".$upper->$column." WHERE id = ".$thisone->id." LIMIT 1");
						$update = $wpdb->query("UPDATE ". $table ." SET " . $column . " = ".$thisone->$column." WHERE id = ".$upper->id." LIMIT 1");

					}

				break;

			}
			

		}
		
	}
	
}


function get_clean_option($option, $default = null){
	
	if($default != null){
		return str_replace( ",~-~", "", get_option($option, $default) );
	}else{
		return str_replace( ",~-~", "", $option );
	}
	
}



function get_taxonomy_posts($ids, $limit = 10, $order = "DESC", $paged = false){
	
	if($ids == "")
		return array();
	
	global $wpdb, $wp_query;
	
	$p_status	= "'publish'"; 
	$p_order	= $order; 
	$p_limit	= $limit;
	$term_ids	= "(" . $ids . ")"; 

	$tax_query = "SELECT $wpdb->posts.* FROM $wpdb->posts
	INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)
	INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
	WHERE 1=1
	AND $wpdb->term_taxonomy.term_id IN $term_ids
	AND $wpdb->posts.post_status = $p_status
	GROUP BY $wpdb->posts.ID
	ORDER BY $wpdb->posts.menu_order, $wpdb->posts.post_date $p_order";

		

	if($paged == false){
		
		return $wpdb->get_results("$tax_query LIMIT $p_limit", OBJECT);
		
	
	}else{
		
		$get_totalposts = $wpdb->get_results($tax_query, OBJECT);
		

		$per_page = intval( $p_limit );
		$wp_query->found_posts = count($get_totalposts);
		$wp_query->max_num_pages = ceil($wp_query->found_posts / $per_page);


		if ( get_query_var('paged') ) {
			$on_page = get_query_var('paged');
		} else if ( get_query_var('page') ) {
			$on_page = get_query_var('page');
		} else {
			$on_page = 1;
		}

		$offset = ($on_page-1) * $per_page;

		$wp_query->request = "$tax_query LIMIT $per_page OFFSET $offset";
		return $wpdb->get_results($wp_query->request, OBJECT);
		
	}
	
}

function get_taxonomy_name_by_id ($ids){
	
	global $wpdb;
	
	return $wpdb->get_results("
			SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->term_taxonomy.taxonomy
			FROM $wpdb->terms
			JOIN $wpdb->term_taxonomy
			ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
			WHERE $wpdb->terms.term_id IN (".$ids.")
			ORDER by $wpdb->terms.term_id ASC");
}



function get_tags_name_by_id($ids){

	global $wpdb;
	
	return $wpdb->get_results("
			SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->terms.slug, $wpdb->term_taxonomy.term_id, $wpdb->term_taxonomy.taxonomy, $wpdb->term_relationships.object_id, $wpdb->term_relationships.term_taxonomy_id
			FROM $wpdb->terms
			LEFT JOIN $wpdb->term_taxonomy
			ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
			LEFT JOIN $wpdb->term_relationships
			ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id AND $wpdb->term_taxonomy.taxonomy = 'post_tag'
			WHERE $wpdb->term_relationships.object_id IN (".$ids.")
			ORDER by $wpdb->terms.term_id ASC");

}


function get_sidebar_type($post_id){

	$sidebar_type = get_post_meta($post_id, "_sidebar_type", true);

	switch($sidebar_type){
		
		case "left":
			$class = "sidebar";
			$position = "right";
		break;
		
		case "right":
			$class = "sidebar";
			$position = "left";
		break;
		
		default:
		case "full":
			$class = "full";
			$position = "left";
		break;
		
	}
	
	return array($class, $position, $sidebar_type);
	
}



function get_sidebar_single($sidebar_type){

	switch($sidebar_type){
		
		case "left":
			$class = "sidebar";
			$position = "right";
		break;
		
		case "right":
			$class = "sidebar";
			$position = "left";
		break;
		
		default:
		case "full":
			$class = "full";
			$position = "left";
		break;
		
	}
	
	return array($class, $position, $sidebar_type);
	
}



function get_breadcrumb(){

	global $post;
	$parents = array();
	$p_id = $post->post_parent;
	
	$output .= '<div class="breadcrumb">';
	$output .= '<a href="' . get_bloginfo( 'url' ) . '" title="' . get_bloginfo( 'name' ) . '" rel="home" class="trail-begin">Home</a> &raquo; ';
	
	while ( $p_id ) :
		$page = get_page( $p_id );
		$parents[]  = '<a href="' . get_permalink( $page->ID ) . '" title="' . get_the_title( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a> &raquo; ';
		$p_id  = $page->post_parent;
	endwhile;
	
	if( is_single () ){
		$p_type = $post->post_type;
		
		if ( $p_type == "post" ) {
			
			$term_type = "category";
			
		} else if ( $p_type == "portfolio_mod" ) {
		
			$term_type = "portfolio_type";

		} else if ( $p_type == "blog_mod" ) {
		
			$term_type = "blog_type";

		}
		
		$output .= get_the_term_list($post->ID, $term_type, '', ', ') . " &raquo; ";
	}
	
	$parents = array_reverse( $parents );
	$output .= implode('',$parents );
	$output .= get_the_title();
	$output .= '</div>';

	return $output;

}
?>