<?php
function print_scripts(){
	wp_enqueue_script('jquery');
}

function framework_scripts(){
   wp_enqueue_script('media-upload');
   wp_enqueue_script('jquery-ui-slider');
   wp_enqueue_script('jquery-ui-widget');
	wp_enqueue_script('framework_js', get_template_directory_uri().'/'.F_PATH.'/libs/js/libs.js', false, '1.0');
}

function framework_styles(){
   wp_enqueue_style('thickbox');
	wp_enqueue_style('framework_css', get_template_directory_uri().'/'.F_PATH.'/libs/css/libs.css');
}

add_action('admin_print_scripts', 'framework_scripts');
add_action('admin_print_styles', 'framework_styles');
add_action('wp_print_scripts', 'print_scripts');

function post_set($req){
	
   global $wpdb;
	if( !is_admin () ){
		$req = str_replace("AND ".$wpdb->prefix."posts.post_type = 'post'", "", $req);
	}
	
	return $req;
	
}
add_action("posts_where_request", "post_set");

function archive_widget_set($req){
	
	if( !is_admin () ){
	
		$req = str_replace("post_type = 'post' AND", "post_type IN ('blog_mod' , 'portfolio_mod' , 'post') AND", $req);
	}
	
	return $req;
	
}

add_action("getarchives_where", "archive_widget_set");

function set_dashboard_widgets() {
	global $wp_meta_boxes;
	
	if( get_option("dashboard_plugins", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	}
	
	if( get_option("dashboard_recent_comments", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	}

	if( get_option("dashboard_incoming_links", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	}
	
	if( get_option("dashboard_secondary", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	}

	if( get_option("dashboard_primary", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	}

	if( get_option("dashboard_recent_drafts", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	}

	if( get_option("dashboard_quick_press", "on") == "off"){
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	}
	
}
add_action('wp_dashboard_setup', 'set_dashboard_widgets' );


function custom_get_terms_args($args){
	$args["hide_empty"] = 0;
	return $args;
}
add_filter('get_terms_args', 'custom_get_terms_args');


add_action('edited_term_taxonomy','edited_term_count',10,2);
function edited_term_count($term,$taxonomy) {
	global $wpdb,$post;

	$post_type = $post->post_type;

	if ($post_type == "portfolio_mod" || $post_type == "blog_mod") {
   
		$sql_exec = "UPDATE $wpdb->term_taxonomy tt
		SET count = 
		(SELECT count(p.ID) FROM  $wpdb->term_relationships tr
		LEFT JOIN $wpdb->posts p
		ON (p.ID = tr.object_id AND p.post_type IN( 'portfolio_mod', 'blog_mod', 'post') AND p.post_status  = 'publish')
		WHERE tr.term_taxonomy_id = tt.term_taxonomy_id)
		WHERE tt.taxonomy = 'post_tag'";

		$wpdb->query($sql_exec);
	}
}
?>