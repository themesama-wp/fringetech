<?php

add_filter( 'manage_portfolio_mod_posts_columns', 'do_custom_colrows');
add_filter( 'manage_blog_mod_posts_columns', 'do_custom_colrows');


function do_custom_colrows($colrows) {

	unset($colrows['comments']);
	unset($colrows['date']);
	
	$colrows['author'] = __('Author');
	
	$colrows["item_category"] = "Categories"; 
	
	$colrows['tags'] = "Tags";

	$colrows['date'] = __('Date');

	$colrows['comments'] = __('<div class="vers"><img src="images/comment-grey-bubble.png" alt="Comments"></div>');
	
	$colrows['column_preview'] = __('Preview');
	
	return $colrows;
}



add_action( 'manage_posts_custom_column', 'custom_colrows_actions', 10, 2);



function custom_colrows_actions($column_name, $id) {


	global $post, $current_screen;
	
	switch($column_name){
		
		case 'item_category':
		
			$clean_post_type = str_replace('_mod', '', $current_screen->post_type);
				
			$speakers = get_the_terms(0, $clean_post_type."_type");
			$speakers_html = array();
			if(is_array($speakers)) {
				foreach ($speakers as $speaker){	
					$clean_post = str_replace('_mod', '', $post->post_type);
					$speakers_html[] = '<a href="edit.php?'.$clean_post.'_type='.$speaker->term_id.'&amp;post_type='.$post->post_type.'">'.$speaker->name.'</a>';
				}
				echo implode($speakers_html, ", ");
			}
		
		break;
		
		case 'column_preview':
		
			$thumb			= get_post_meta( $post->ID , '_post_thumbnail' , true );
			$fullsize		= get_post_meta( $post->ID , '_post_fullsize' , true );
			$timthumb		= get_post_meta( $post->ID , '_timthumb' , true );
			
			if($timthumb != "on" && $thumb != ""){ ?>
			<a href="post.php?post=<?php echo $post->ID; ?>&action=edit"><span class="<?php if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $thumb) ) { echo "upload_pic"; } else { echo "external_pic"; $thumb = ""; } ?>"><?php if( $thumb != "" ) { ?><img height="70" width="70" alt="" src="<?php echo $thumb; ?>" /><?php } ?></span></a>
			<?php } else if($fullsize != ""){	?>
			<a href="post.php?post=<?php echo $post->ID; ?>&action=edit"><span class="<?php if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $fullsize) ) { echo "upload_pic"; } else { echo "external_pic"; $fullsize = ""; } ?>"><?php if( $fullsize != "" ) { ?><img height="70" width="70" alt="" src="<?php echo $fullsize; ?>" /><?php } ?></span></a>
			<?php }
			
		break;
		
	}

}
?>