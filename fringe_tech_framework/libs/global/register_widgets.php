<?php
class WP_Widget_Custom_Categories extends WP_Widget {

	function WP_Widget_Custom_Categories() {
		$widget_ops = array( 'classname' => 'widget_custom_categories', 'description' => __( "A list of custom categories" ) );
		$this->WP_Widget('custom_categories', __('Custom Categories'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract( $args );

		$title 			= apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Custom Categories' ) : $instance['title'], $instance, $this->id_base);
		$c 				= $instance['count'] ? '1' : '0';
		$h				= $instance['hierarchical'] ? '1' : '0';
		$custom_posts	= $instance['custom_posts'];
		
		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;

		$cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h);
?>
<ul>
<?php
$cat_args['title_li'] = '';
if( $custom_posts == "Blog Categories") {
	$cat_args['taxonomy'] = "blog_type";
	wp_list_categories(apply_filters('widget_categories_args', $cat_args));
}else if( $custom_posts == "Portfolio Categories") {
	$cat_args['taxonomy'] = "portfolio_type";
	wp_list_categories(apply_filters('widget_categories_args', $cat_args));
} else {
	$cat_args['taxonomy'] = "portfolio_type";
	wp_list_categories(apply_filters('widget_categories_args', $cat_args));
	$cat_args['taxonomy'] = "blog_type";
	wp_list_categories(apply_filters('widget_categories_args', $cat_args));
}
?>
</ul>
<?php

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;

		$request_options = $_REQUEST["options"];
		
		foreach($request_options as $key => $option) {
			
				
			if (empty($option)) {
				unset( $instance[$key] );
				continue;
			}
					
			if(!is_array($option)){
				
				$instance[$key] = strip_tags( $option );
				
			}else{
			
				foreach($request_options[$key] as $option_name => $op) {
				
					$values = array();
					
					foreach($op as $k => $v){
						if($v != ""){
							$values[] = $v;
						}
					}
				
					$values = implode(",", $values);
					
					if (empty($values)) {
						
						unset( $instance[$option_name] );
						continue;
						
					}
					
					$instance[$option_name] = $values;
					
				}
				
			}
		
		}
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = esc_attr( $instance['title'] );
		$custom_posts = esc_attr( $instance['custom_posts'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		
?>
		<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
		<label for="<?php echo $this->get_field_id('custom_posts'); ?>"><?php _e('Choose Category:'); ?></label>
		<div class="custom_categories">
		<?php render_item("select", 'custom_posts', "Blog Categories,Portfolio Categories", $custom_posts, "multiple=multiple style=\"width:100%; height:100px;\""); ?>
		</div>
		
		<div class="clear" style="margin-bottom:10px;"></div>
		
		<p>
		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label>
		</p>
		
		<p>
		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label>
		</p>
<?php
	}

}
register_widget("WP_Widget_Custom_Categories");


/*
	Custom Recent Posts
*/
class WP_Widget_Custom_Recent_Posts extends WP_Widget {

	function WP_Widget_Custom_Recent_Posts() {
		$widget_ops = array('classname' => 'widget_custom_recent_entries', 'description' => __( "The most custom recent posts on your site") );
		$this->WP_Widget('custom-recent-posts', __('Custom Recent Posts'), $widget_ops);
		$this->alt_option_name = 'widget_custom_recent_entries';

		add_action( 'save_post', array(&$this, 'flush_widget_cache') );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('widget_custom_recent_posts', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( isset($cache[$args['widget_id']]) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract($args);

		$title = apply_filters('widget_title', empty($instance['title']) ? __('Custom Recent Posts') : $instance['title'], $instance, $this->id_base);
		if ( ! $number = absint( $instance['number'] ) )
 			$number = 10;
			
?>
<?php echo $before_widget; ?>
<?php if ( $title ) echo $before_title . $title . $after_title; ?>
<ul>
<?php
$p_items = get_taxonomy_posts($instance['custom_posts'], $number, "DESC", false);
global $post;
foreach ( $p_items as $post ) {
setup_postdata($post);
?>
<li <?php if ( $instance['thumbnail'] == 1) { echo 'class="recent_post"'; } ?>>
	
	<?php if ( $instance['thumbnail'] == 1) { ?>
	<a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>">

		<?php 
			$thumb = get_post_meta($post->ID, '_post_thumbnail', true);
			if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $thumb) ){
		?>
		<span class="recent_post_thumb image_frame_effect">
			<img src="<?php echo get_post_meta($post->ID, '_post_thumbnail', true); ?>" width="60" height="60" alt=""/>
		</span>
		<?php } ?>
		
		<span class="recent_post_title">
			<h6><?php echo $post->post_title; ?></h6>
		</span>
		
	</a>
	<span class="recent_post_date">
		<?php echo get_the_date(); ?>
	</span>
	<?php } else { ?>
	<a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>">
		<span class="recent_post_title">
			<?php echo $post->post_title; ?>
		</span>
	</a>
	<?php } ?>
</li>
<?php } ?>
</ul>
<?php echo $after_widget; ?>
<?php
		wp_reset_postdata();

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_custom_recent_posts', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] 			= strip_tags($new_instance['title']);
		$instance['thumbnail'] = !empty($new_instance['thumbnail']) ? 1 : 0;
		$instance['number']			= (int) $new_instance['number'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_custom_recent_entries']) )
			delete_option('widget_custom_recent_entries');

		
		$request_options = $_REQUEST["options"];
		
		foreach($request_options as $key => $option) {
			
				
			if (empty($option)) {
				unset( $instance[$key] );
				continue;
			}
					
			if(!is_array($option)){
				
				$instance[$key] = strip_tags( $option );
				
			}else{
			
				foreach($request_options[$key] as $option_name => $op) {
				
					$values = array();
					
					foreach($op as $k => $v){
						if($v != ""){
							$values[] = $v;
						}
					}
				
					$values = implode(",", $values);
					
					if (empty($values)) {
						
						unset( $instance[$option_name] );
						continue;
						
					}
					
					$instance[$option_name] = $values;
					
				}
				
			}
		
		}
		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_custom_recent_posts', 'widget');
	}

	function form( $instance ) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
		$custom_posts = $instance['custom_posts'];
		$thumbnail = isset( $instance['thumbnail'] ) ? (bool) $instance['thumbnail'] : false;
?>
<p>
<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</p>

<p>
<div class="custom_categories"><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Choose Category:'); ?></label>
<?php render_item("select", 'custom_posts', "[categories],[all],[DESC],[id]", $custom_posts, "multiple=multiple style=\"height:175px;\""); ?>
</div>
<div class="clear" style="margin-bottom:10px;"></div>
<p>
<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:'); ?></label>
<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
</p>
<div class="clear" style="margin-bottom:10px;"></div>

<p>
<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('thumbnail'); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>"<?php checked( $thumbnail ); ?> />
<label for="<?php echo $this->get_field_id('thumbnail'); ?>"><?php _e( 'Show post thumbnail' ); ?></label>
</p>
<?php
	}
}
register_widget("WP_Widget_Custom_Recent_Posts");

class WP_Widget_Custom_Tag_Cloud extends WP_Widget {

	function WP_Widget_Custom_Tag_Cloud() {
		$widget_ops = array( 'description' => __( "Your most used tags in cloud format") );
		$this->WP_Widget('custom_tag_cloud', __('Custom Tag Cloud'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$current_taxonomy = $this->_get_current_taxonomy($instance);
		if ( !empty($instance['title']) ) {
			$title = $instance['title'];
		} else {
			if ( 'post_tag' == $current_taxonomy ) {
				$title = __('Tags');
			} else {
				$tax = get_taxonomy($current_taxonomy);
				$title = $tax->labels->name;
			}
		}
		$title = apply_filters('widget_title', $title, $instance, $this->id_base);

		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;
		
		echo '<div class="custom_tagcloud">';
		
			$smallest = $instance['smallest'];
			$largest = $instance['largest'];
			$limit = $instance['limit'];
		
			wp_tag_cloud( array('taxonomy' => $current_taxonomy, 'smallest' => $smallest, 'largest' => $largest,'number' => $limit) );
		
		echo "</div>\n";
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance['title'] = strip_tags(stripslashes($new_instance['title']));
		$instance['smallest'] = stripslashes($new_instance['smallest']);
		$instance['largest'] = stripslashes($new_instance['largest']);
		$instance['limit'] = stripslashes($new_instance['limit']);
		$instance['taxonomy'] = stripslashes($new_instance['taxonomy']);
		return $instance;
	}

	function form( $instance ) {
		$current_taxonomy = $this->_get_current_taxonomy($instance);
?>
	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
	<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php if (isset ( $instance['title'])) {echo esc_attr( $instance['title'] );} ?>" /></p>
	
	
	<p>
		<label for="<?php echo $this->get_field_id('smallest'); ?>"><?php _e( 'Smallest (font-size):' ); ?></label>
		<input type="text" class="widefat" id="<?php echo $this->get_field_id('smallest'); ?>" name="<?php echo $this->get_field_name('smallest'); ?>" value="<?php if (isset ( $instance['smallest'])) {echo esc_attr( $instance['smallest'] );} else { echo "9"; } ?>" /></p>
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('smallest'); ?>"><?php _e( 'Largest (font-size):' ); ?></label>
		<input type="text" class="widefat" id="<?php echo $this->get_field_id('largest'); ?>" name="<?php echo $this->get_field_name('largest'); ?>" value="<?php if (isset ( $instance['largest'])) {echo esc_attr( $instance['largest'] );} else { echo "22"; } ?>" /></p>
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e( 'Tag Limit:' ); ?></label>
		<input type="text" class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" value="<?php if (isset ( $instance['limit'])) {echo esc_attr( $instance['limit'] );} else { echo "50"; } ?>" /></p>
	</p>

	<p><label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Taxonomy:') ?></label>
	<select class="widefat" id="<?php echo $this->get_field_id('taxonomy'); ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>">
	<?php foreach ( get_object_taxonomies(array('post', 'blog_mod', 'portfolio_mod')) as $taxonomy ) :
				$tax = get_taxonomy($taxonomy);
				if ( !$tax->show_tagcloud || empty($tax->labels->name) )
					continue;
	?>
		<option value="<?php echo esc_attr($taxonomy) ?>" <?php selected($taxonomy, $current_taxonomy) ?>><?php echo $tax->labels->name; ?></option>
	<?php endforeach; ?>
	</select></p><?php
	}

	function _get_current_taxonomy($instance) {
		if ( !empty($instance['taxonomy']) && taxonomy_exists($instance['taxonomy']) )
			return $instance['taxonomy'];

		return 'post_tag';
	}
}
register_widget("WP_Widget_Custom_Tag_Cloud");

?>