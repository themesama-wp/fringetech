<?php
$cp_db_version		= "1.1";
$category_table		= $wpdb->prefix . "slider_categories";
$item_table			= $wpdb->prefix . "slider_items";
$taxonomy_table		= $wpdb->prefix . "term_data";

add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'fringe_tech' ),
	'sidebar' => __( 'Sidebar Navigation', 'fringe_tech' ),
	'footer' => __( 'Footer Navigation', 'fringe_tech' ),
) );

add_custom_background();


define("last_pid" , $wpdb->get_var("SELECT ID FROM $wpdb->posts LIMIT 1")); 


define("T_URI", get_template_directory_uri());
define("T_CSS", get_template_directory_uri()."/css");
define("T_JS", get_template_directory_uri()."/js");
?>