<?php
add_action( 'add_meta_boxes', 'custom_post_metaboxes', 0 );
function custom_post_metaboxes(){
	
	global $register_types, $sidebars;
	
	if( $register_types[0] != "") {
	
		foreach($register_types as $type){
			add_meta_box('tagsdiv-post_tag', __('Tags', 'fringe_tech'), 'post_tags_meta_box', $type, 'side', 'core', array( 'taxonomy' => 'post_tag' ));
			add_meta_box('custom_fields_post', __('Helpful Custom Fields', 'fringe_tech'), 'custom_fields_post', $type, 'normal', 'core');
			add_meta_box('authordiv', __('Author', 'fringe_tech'), 'post_author_meta_box', $type, 'side', 'core');
			add_meta_box('commentstatusdiv', __('Discussion', 'fringe_tech'), 'post_comment_status_meta_box', $type, 'side', 'low');
			add_meta_box('postexcerpt', __('Excerpt', 'fringe_tech'), 'post_excerpt_meta_box', $type, 'side', 'core');
			add_meta_box('commentsdiv', __('Comments', 'fringe_tech'), 'post_comment_meta_box', $type, 'normal', 'core');
			add_meta_box('postcustom', __('Custom Fields', 'fringe_tech'), 'post_custom_meta_box', $type, 'normal', 'low');
			add_meta_box('trackbacksdiv', __('Send Trackbacks', 'fringe_tech'), 'post_trackback_meta_box', $type, 'normal', 'low');
		}
	
	}
	
	add_meta_box('portfolio_page_custom_fields', __('Portfolio Page Custom Fields', 'fringe_tech'), 'portfolio_page_custom_fields', 'page', 'normal', 'high');
	add_meta_box('blog_page_custom_fields', __('Blog Page Custom Fields', 'fringe_tech'), 'blog_page_custom_fields', 'page', 'normal', 'high');
	
	if($sidebars != ""){
		add_meta_box('sidebar_layouts', __('Page Sidebar Layouts', 'fringe_tech'), 'sidebar_layouts', 'page', 'side', 'low');
	}
	
}


function sidebar_layouts(){

	global $post;
	wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );
	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/sidebar_fields.php");
	
}

function blog_page_custom_fields(){

	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/blog_page_fields.php");
	
}

function portfolio_page_custom_fields(){

	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/portfolio_page_fields.php");
	
}

function custom_fields_post(){

	global $post;
	wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );
	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/post_fields.php");
	
}

add_action("delete_post", "delete_custom_fields");
function delete_custom_fields($post_id) {
	
	$get_options = get_option("option_sidebars");
	$get_options = str_replace("{" . $post_id . "}", "", $get_options);
	update_option( "option_sidebars", $get_options );
	
}
 

add_action("save_post", "save_custom_fields");

function save_custom_fields($post_id) {

	global $post;

	if ( !wp_verify_nonce( @$_POST['myplugin_noncename'], plugin_basename(__FILE__) ))
    	return $post_id;

  	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
    	return $post_id;
	
	if ( 'page' == $_POST['post_type'] ) {
    	if ( !current_user_can( 'edit_page', $post_id ) )
     		 return $post_id;
  	} else {
    	if ( !current_user_can( 'edit_post', $post_id ) )
      		return $post_id;
  	}
	
	if ( $parent_id = wp_is_post_revision($post_id) )
		$post_id = $parent_id;
      if ($post->post_type == "portfolio_mod" or $post->post_type == "blog_mod" or $post->post_type == "page") {

		$request_options = $_REQUEST["options"];

		$sidebar_type = $request_options["checkboxes"]["_sidebar_type"][0];
		
		if($sidebar_type == "left" OR $sidebar_type == "right") {
		
			$get_options = get_option("option_sidebars");
			
			if($request_options["checkboxes"]["custom_sidebar"][0] == "on" ){

				$get_options  = preg_replace("/\{" . $post_id . "\}/", "", $get_options);
				$get_options .= "{" . $post_id . "}";
				
				update_option( "option_sidebars", $get_options );
				

			}else if($request_options["checkboxes"]["custom_sidebar"][0] == "off" ){

				$get_options = preg_replace("/\{" . $post_id . "\}/", "", $get_options);
				update_option( "option_sidebars", $get_options );
				
			}
			
		} else {
			
			$get_options = get_option("option_sidebars");
			$get_options = preg_replace("/\{" . $post_id . "\}/", "", $get_options);
			update_option( "option_sidebars", $get_options );
			
		}
		

		foreach($request_options as $key => $option) {
			
				
			if (empty($option)) {
				delete_post_meta($post_id, $key);
				continue;
			}
					
			if(!is_array($option)){
				
				if(get_post_meta($post_id, $key) != ""){
					update_post_meta($post_id, $key, $option);
				}else{
					add_post_meta($post_id, $key, $option);
				}
				
			}else{
			
				foreach($request_options[$key] as $option_name => $op) {
				
					if($option_name != "custom_sidebar") {

						$values = array();
						
						foreach($op as $k => $v){
							if($v != ""){
								$values[] = $v;
							}
						}
					
						$values = implode(",", $values);
						
						if (empty($values)) {
							delete_post_meta($post_id, $option_name);
							continue;
						}
						
						if(get_post_meta($post_id, $option_name) != ""){
							update_post_meta($post_id, $option_name, $values);
						}else{
							add_post_meta($post_id, $option_name, $values);
						}
					
					}
					
				}
				
			}

		}
		
    }
	
}
?>