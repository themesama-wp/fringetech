<?php
global $register_types;
add_action('init', 'custom_register_types', 0);

function custom_register_types(){
	global $register_types;
	
	if( $register_types[0] != "") {
	
		foreach($register_types as $type){
		
			$clean_type = str_replace('_mod', '', $type);
			$type_text = ucwords($clean_type);

			$labels = array(
				'name' => __( $type_text."s" ),
				'singular_name' => __( $type ),
				'add_new' => __( 'Add New' ),
				'add_new_item' => __( 'Add New '. $type_text .' Item' ),
				'edit_item' => __( 'Edit '.$type_text ),
				'new_item' => __( 'New '.$type_text ),
				'view_item' => __( 'View '.$type_text ),
				'search_items' => __( 'Search '.$type_text.'s' ),
				'not_found' =>  __( 'There is not any item. Do not forget to create a <a href="edit-tags.php?taxonomy='. $clean_type .'_type&post_type='. $type .'">category</a> before you add item.', 'fringe_tech'),
				'not_found_in_trash' => __( 'There is not any item.', 'fringe_tech' ), 
				'parent_item_colon' => '',
				'menu_name' => $type_text.'s'
			);


			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true, 
				'show_in_menu' => true, 
				'query_var' => true,
				'capability_type' => 'post',
				'has_archive' => true, 
				'hierarchical' => false,
				'rewrite' => true,
				'can_export' => true,
				'menu_position' => 5,
				'menu_icon' => get_template_directory_uri().'/'.F_PATH.'/libs/images/'.$clean_type.'_icon.png',
				'supports' => array( 'title', 'editor' )
			); 
			

			$type_labels = array(
				'name' => __( 'Categories of ' . $type_text ),
				'singular_name' => __( $type_text ),
				'search_items' =>  __( 'Search '.$type_text.'s' ),
				'all_items' => __( 'All '.$type_text.'s' ),
				'parent_item' => __( 'Parent '.$type_text ),
				'parent_item_colon' => __( 'Parent '.$type_text.':' ),
				'edit_item' => __( 'Edit '.$type_text.' Category' ), 
				'update_item' => __( 'Update '.$type_text.' Category' ),
				'add_new_item' => __( 'Add New '.$type_text.' Category' ),
				'new_item_name' => __( 'New '.$type_text.' Category Name' ),
				'menu_name' => __( "Categories" )
			); 
			

			$type_args = array(
				'hierarchical' => true,
				'labels' => $type_labels,
				'show_ui' => true,
				'public' => true,
				'query_var' => true,
				'rewrite' => true
			);
			
			register_post_type($type, $args);
			register_taxonomy($clean_type.'_type', $type, $type_args);
			
		}
		
	}
	
}



add_filter('post_type_link', 'custom_post_type_permalink', 10, 3);

function custom_post_type_permalink($permalink, $post, $leavename) {

	$permalink_structure = get_option('permalink_structure');

	if( '' != $permalink_structure && ( ('blog_mod' == $post->post_type) || ('portfolio_mod' == $post->post_type) ) && '' != $permalink && !in_array($post->post_status, array('draft', 'pending', 'auto-draft')) ) {

		if( 'blog_mod' == $post->post_type ){
			$get_post_terms = wp_get_post_terms($post->ID, 'blog_type');
		}
		elseif ( 'portfolio_mod' == $post->post_type ){
			$get_post_terms = wp_get_post_terms($post->ID, 'portfolio_type');
		}

		foreach($get_post_terms as $term){
			$terms[] = $term->slug;
		}

		$rewritecode = array(
			'%year%',
			'%monthnum%',
			'%day%',
			'%hour%',
			'%minute%',
			'%second%',
			'%post_id%',
			'%author%',
			'%category%',
			$leavename? '' : '%postname%',
			$leavename? '' : '%pagename%',
		);

		

		$author = '';
		if ( strpos($permalink_structure, '%author%') !== false ) {
			$authordata = get_userdata($post->post_author);
			$author = $authordata->user_nicename;
		}
		

		if($get_post_terms[0]->slug != "") {
			$category = implode("+", $terms);
		}else{
			$category = "empty";
		}

		$unixtime = strtotime($post->post_date);
		$date = explode(" ",date('Y m d H i s', $unixtime));
   

		$rewritereplace = array(
			$date[0],
			$date[1],
			$date[2],
			$date[3],
			$date[4],
			$date[5],
			$post->ID,
			$author,
			$category,
			$post->post_name,
			$post->post_name,
		);
		

		$permalink = str_replace($rewritecode, $rewritereplace, $permalink_structure );
		$permalink = user_trailingslashit(home_url($permalink));
		
	}
	

	if( '' == $permalink_structure && ( ('blog_mod' == $post->post_type) || ('portfolio_mod' == $post->post_type) ) && '' != $permalink && !in_array($post->post_status, array('draft', 'pending', 'auto-draft')) ) {
		$permalink = home_url("?p=".$post->ID);
	}
	
	return $permalink;
	
}




function custom_post_type_shortlinks( $shortlink, $id ) {
	
	if ( '' != get_option('permalink_structure') ) {
		$shortlink = home_url('?p=' . $id);
		return $shortlink;
	}
}

add_filter( 'get_shortlink', 'custom_post_type_shortlinks', 10, 4 );




add_action('restrict_manage_posts','my_restrict_manage_posts');
function my_restrict_manage_posts() {

	global $typenow, $wp_query, $register_types;
	
	$clean_typenow = str_replace('_mod', '', $typenow);
	
	if( $register_types[0] != "") {
	
		if( in_array($typenow,  $register_types) ){
			$args = array(
				'show_option_all'	=> __("View all categories", 'fringe_tech'),
				'taxonomy'			=> $clean_typenow."_type",
				'name'				=> $clean_typenow."_type",
				'order'				=> "ASC",
				'hide_if_empty'		=> true,
				'show_count'		=> true,
				'hide_empty' 		=> 0, 
				'hierarchical'		=> 1,
				'class'				=> "portfolio_filter",
				'selected'       	=> @$_GET[$clean_typenow."_type"],
			);
			wp_dropdown_categories($args);
		}
	
	}
}


add_action( 'request', 'my_request' );
function my_request($request) {

	global $typenow, $register_types;
	$clean_typenow = str_replace('_mod', '', $typenow);
	
	if( $register_types[0] != "") {
	
		$current_url = substr( $GLOBALS['PHP_SELF'], -18);
		
		if( in_array($typenow,  $register_types) ){
		
			if (is_admin() && $current_url == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type']==$typenow) { 
				
				$term_name = get_term( @$request[$clean_typenow."_type"], $clean_typenow."_type" );
				@$request[$clean_typenow."_type"] = $term_name->name;
				
			}
		
		}
		
		return $request;
		
	}
	
}
?>