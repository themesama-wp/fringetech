<?php
/* Start WordPress */
$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $parse_uri[0].'wp-load.php';
require_once($wp_load);

if ( !current_user_can('edit_pages') && !current_user_can('edit_posts') ) 
	wp_die(__("You are not allowed to be here", "fringe_tech"));

global $shortcode_attrs;

if($_POST["shortcode"] != ""){
	
	$str	 	= "";
	$key 		= $_POST["shortcode"];
	$attr		= $shortcode_attrs[$key]["attr"];
	$type		= $shortcode_attrs[$key]["type"];
	$intro		= $shortcode_attrs[$key]["intro"];
	$attr_more	= "";
	$attrs_key	= "";
	
	if ( is_array ( $attr ) ){
	
		foreach($attr as $k => $v){
			
			$attrs_key .= " ".$k."="."\"".$v."\"";
		
		}

	}
	
	if( $type == "c" ){
		
		$str	.= $attr;
	
	} else {
	
		if ( $type == "m" ){
			$attr_more = "[/".$key."]";
		}

		$str	.= "[".$key.$attrs_key."]";
		$str	.= $intro;
		$str	.= $attr_more;
		
	}
	
	$str = str_replace("[-]", "", $str);
?>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
	self.parent.send_to_editor('<?php echo $str; ?>');
</script>
<?php exit(); } ?>

<style type="text/css">
.shortcode_select,
.shortcode_submit{	text-shadow: 0 1px 0 #FFFFFF;	-moz-border-radius: 5px;	border-style: solid;	border-width: 1px;	cursor: pointer;	font-size: 12px !important;	line-height: 13px;	padding: 10px;	margin:10px;	width:150px;	font-weight:bold;	text-decoration: none; }
.shortcode_select{	margin:10px;	width: 300px;	height:40px;	text-shadow:none;	}
.shortcode_title{	margin:10px auto; 	font-size:25px;	line-height:25px;	font-family:"Trebuchet Ms";	width:340px;	}
.shortcodes{		margin:10px auto; 	width:340px;	text-align:center;	}
.shortcode_select optgroup option{	padding:5px 10px;}

</style>

<form action="" method="post">

	<div class="shortcode_title">Choose a shortcode for insert</div>

	<div class="shortcodes">
	
		<?php
		$options = array();
		foreach ($shortcode_attrs as $key => $value) {
		
			$options[$shortcode_attrs[$key]["group"]][] = '<option value="'.$key.'"> &raquo; '.$shortcode_attrs[$key]["name"].'</button>';
		
		}
		

		?>	

		<select class="shortcode_select" name="shortcode">
			<option value="-"> --Choose One-- </option>
			<?php 
				foreach($options as $key => $option){
					echo '<optgroup label="' . $key . '">';
					
					foreach ( $option as $opt ) {
					
						echo $opt;

					}

					
				}
			?>
		</select>
		<br />
		<input class="shortcode_submit" type="submit" value="Insert the shortcode"/>
		
	</div>

</form>