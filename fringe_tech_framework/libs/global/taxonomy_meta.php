<?php
global $wpdb;

add_action('created_term', 'save_meta_terms');
add_action('edit_term', 'save_meta_terms');
add_action('delete_term', 'delete_meta_terms');
add_action('portfolio_type_add_form_fields', 'add_meta_portfolio_type');
add_action('portfolio_type_edit_form', 'add_meta_portfolio_type');
add_action('blog_type_add_form_fields', 'add_meta_blog_type');
add_action('blog_type_edit_form', 'add_meta_blog_type');


function add_meta_portfolio_type($tag){

	global $wpdb, $taxonomy_table;

	$t_value = "";
	if( @$_REQUEST["action"] == "edit" ) {
		$t_values = $wpdb->get_row("SELECT * FROM " . $taxonomy_table . " WHERE t_id = '".$tag->term_id."'");
		$t_value = $t_values->t_value;
	}
	
	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/taxonomy_portfolio_fields.php");
	
}


function add_meta_blog_type($tag){

	global $wpdb, $taxonomy_table;
	$t_value = "";
	if(@$_REQUEST["action"] == "edit") {
		$t_values = $wpdb->get_row("SELECT * FROM " . $taxonomy_table . " WHERE t_id = '".$tag->term_id."'");
		$t_value = $t_values->t_value;
	}
	
	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/taxonomy_blog_fields.php");
	
}


function save_meta_terms($id) {
	
	global $wpdb, $taxonomy_table;

	$request_options = $_REQUEST["options"];
	
	if( count($request_options) > 0 ) {
	
		$s_value = "";
		foreach($request_options as $key => $option) {

			if ("s_name" != $key && "s_desc" != $key && "cats" != $key ) {

				if(!is_array($option)){
					$s_value	.=  "{". $key . "=" . $option . "}";
				}else{
				
					foreach($request_options[$key] as $option_name => $op) {

						foreach($op as $k => $v){
							if($v != ""){
								$s_value	.= "{". $option_name . "=" . $v."}";
							}
						}

					}
					
				}
				
			}

		}
		

		if($_REQUEST["action"] == "add-tag"){
		
			$sql_exec = "INSERT INTO " . $taxonomy_table . " SET t_id	= '". $id ."', t_value	= '". $s_value . "'";
		
		}else if($_REQUEST["action"] == "editedtag"){
		
			$sql_exec = "UPDATE " . $taxonomy_table . " SET t_value = '". $s_value . "' WHERE t_id	= '". $id ."'";
		
		}
		
		$wpdb->query( $sql_exec );
	
	}
	
}

function delete_meta_terms($id) {

	global $wpdb, $taxonomy_table;
	$sql_exec = "DELETE FROM " . $taxonomy_table . " WHERE t_id = '". $id ."'";
	$wpdb->query( $sql_exec );
	
}
?>