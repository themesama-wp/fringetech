<?php
add_filter('query_vars','dynamic_vars');
function dynamic_vars($vars) {
    $vars[] = 'dyn';
    return $vars;
}

add_action('template_redirect', 'var_check');
function var_check() {
	$get_var = get_query_var('dyn');

	if($get_var == "css") {
	header('Content-type: text/css');

?>
/* 
 *
 *
 Begining Dynamic Header Stylesheets
 *
 *
*/

body{
	<?php if ( get_background_color() == "" ) { ?>background-image:url(<?php echo T_URI; ?>/images/bg.png);<?php } ?>
	color:<?php echo get_option("general_font_color", "#555555"); ?>; 
	font-size:<?php echo get_option("general_font_size", "12"); ?>px;
	font-family:"<?php echo get_font ( get_clean_option("general_font_family", "Tahoma") ); ?>";
	line-height:<?php echo get_option("general_font_line_height", "21"); ?>px;
	
	height:100%;
}

p{
	line-height:<?php echo get_option("general_font_line_height", "21"); ?>px;
}

.header_line{		background:url(<?php echo get_option("header_line", T_URI."/images/header_dashed.png"); ?>) repeat-x;	}

<?php if( get_option("header_shadow_effect", "on") == "on" ) { ?>
.header_effect {	
	background:url(<?php echo T_URI; ?>/images/header_dark.png) no-repeat top center;	
	<?php if( get_option("header_line_active", "on") == "off" ) { ?>top:0;<?php } ?>
}
<?php } ?>


/* begin header types */
<?php
	$header_type = get_option("header_type", "default");
	if( $header_type == "default" OR $header_type == "" ) { 
?>
.header_logo{			float:left;	}
.header_container .header_menu{			margin-top:80px;	float:right;	z-index:1; }
.header_container .header_menu ul{		list-style:none;	z-index:1;	}
<?php } ?>

<?php if( $header_type != "default" && $header_type != "" ) { ?>
.header_logo{	float:left;	width:100%;	text-align:center;	margin-top:20px;	}
.header_container .header_menu{	margin-top:20px;	text-align:center;	float:left;	width:100%;	}
<?php } ?>

<?php if( $header_type == "center" ) { ?>
.header_container .header_menu ul{		list-style:none;	z-index:1;	margin:0 auto; display:table;	}
<?php } ?>

<?php if( $header_type == "left" ) { ?>
.header_container .header_menu ul{		list-style:none;	z-index:1;	margin:0 auto; float:left; }
<?php } ?>

<?php if( $header_type == "right" ) { ?>
.header_container .header_menu ul{		list-style:none;	z-index:1;	margin:0 auto; float:right; }
<?php } ?>
/* end header types */



/* menu styles - colors */

.header_container .header_menu > ul > li > a{

	font-size:<?php echo get_option("main_menu_font_size", "13"); ?>px; 
	color:<?php echo get_option("header_link_color", "#555555"); ?>;

	<?php if( get_option("main_menu_font_weight", "on") == "on" ) { ?>
	font-weight:bold;
	<?php } ?>
	
	<?php if( get_option("main_menu_font_style", "off") == "on" ) { ?>
	font-style:italic;
	<?php } ?>

	font-family: "<?php echo get_font ( get_clean_option("main_menu_font_family", "Tahoma") ); ?>";
	
}

.recently .categories ul li a{
	color:<?php echo get_option("header_link_color", "#555555"); ?>;	
}

.recently .categories ul li a:hover,
.recently .categories ul li.current_page_item a,
.header_menu > ul > li.current_page_ancestor > a,
.header_menu > ul > li.current_page_item > a,
.header_menu > ul > li a:hover{	
	color:<?php echo get_option("header_link_hover_color", "#2ba09e"); ?>;
}




/* drop down menu styles */
.header_container .header_menu ul li ul{		

	background-color:<?php echo get_option("drop_down_color", "#111111"); ?>; 
	-webkit-border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px;	
	-moz-border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px; 	
	border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px;	
	width:<?php echo get_option("drop_down_width", "175"); ?>px;
}

.header_container .header_menu ul li ul li a{			
	color:<?php echo get_option("drop_down_link_color", "#888888"); ?>;	
	font-size:<?php echo get_option("drop_down_font_size", "11"); ?>px; 
	font-family:"<?php echo get_font ( get_clean_option("drop_down_font_family", "Tahoma") ); ?>";
	
	<?php if( get_option("drop_down_font_weight", "off") == "on" ) { ?>
	font-weight:bold;
	<?php } ?>
	
	<?php if( get_option("drop_down_font_style", "off") == "on" ) { ?>
	font-style:italic;
	<?php } ?>
}

.header_container .header_menu ul li ul li a:hover{	background-color:<?php echo get_option("drop_down_link_back_color", "#ffffff"); ?>;	color:<?php echo get_option("drop_down_link_hover_color", "#666666"); ?>;	}

.header_container .header_menu ul ul li.current_page_ancestor > a,
.header_container .header_menu ul ul li.current_page_item > a{
	color:<?php echo get_option("drop_down_link_active_color", "#2ba09e"); ?>;
}


/* header menu line and current_line effect */
.categories_cline,
.header_menu_cline{			border-bottom:1px solid <?php echo get_option("header_over_line", "#2ba09e"); ?>;	}


/* tooltip settings using dropdown vars */
.tooltip_text{

	color:<?php echo get_option("drop_down_link_color", "#888888"); ?>;	
	background-color:<?php echo get_option("drop_down_color", "#111111"); ?>; 
	-webkit-border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px;	
	-moz-border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px; 	
	border-radius: <?php echo get_option("drop_down_radius", "3"); ?>px;
	
}
.tooltip_arrow{				border-color:<?php echo get_option("drop_down_color", "#111111"); ?> transparent !important; }

/* General Site Lines */

.page_sidebar .recent_post,
hr,
.divider_with_top,
.divider_hr,
.blog_boxes .blog_box,
.no_slide,
.page_title,
.recently,
.recently .intro .title,
.header_menu_line,
.categories_line,
.home_cboxes .one_cbox .title,
.home_custom .home_custom_content,
.home_custom .home_custom_title,
.intro_content{	border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>;	}

.quickly_support,
.header_icon_seach{
	border-left:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>;
}

.lock_box,
.settings_box,
.megaphone_box,
.support_box,
pre.code{
	border:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>;
}


/* twitter settings */

.bottom_up_wrap{
	background-color: <?php echo get_option("twitter_background", "#f1f1f1"); ?>;
	color:<?php echo get_option("twitter_font_color", "#555555"); ?>;
}


/* footer settings */
.footer {	color:<?php echo get_option("footer_text_color", "#555555"); ?>; background:url(<?php echo get_option("footer_bg", T_URI."/images/footer_bg.png"); ?>) top center; }

.footer .footer_info{	border-top:1px solid <?php echo get_option("footer_info_border", "#cfcfcf"); ?>;	}
.footer_list ul li{		border-bottom:1px solid <?php echo get_option("footer_info_border", "#cfcfcf"); ?>;	}



/* general link styles */

.widget_nav_menu ul li a,
.recently_boxes .title a,
a{					color:<?php echo get_option("link_color", "#2ba09e"); ?>;	}

.widget_pages ul li.current_page_item > a,
.widget_nav_menu ul li a:hover,
.recently_boxes .title a:hover,
a:hover{			color:<?php echo get_option("link_hover_color", "#222222"); ?>;	}

::selection {	 	background: <?php echo get_option("link_color", "#2ba09e"); ?>; color: #f1f1f1; }
::-moz-selection {	background: <?php echo get_option("link_color", "#2ba09e"); ?>; color: #f1f1f1; }



/* Headline Typography Styles */
h1, h2, h3, 
h4, h5, h6 {
	font-family:"<?php echo get_font ( get_clean_option ( "h_font_family", "Georgia" ) ); ?>";
	letter-spacing:0.4px;	
	padding-bottom:10px;	
	font-weight:normal;
	
	<?php if( get_option("h_weight", "off") == "on" ) { ?>
	font-weight:bold;
	<?php } ?>
	
	<?php if( get_option("h_style", "off") == "on" ) { ?>
	font-style:italic;
	<?php } ?>
	
	
}

<?php 
$h1 = get_option( "h1_font_size", "36" ); 
$h2 = get_option( "h2_font_size", "28" ); 
$h3 = get_option( "h3_font_size", "22" ); 
$h4 = get_option( "h4_font_size", "18" ); 
$h5 = get_option( "h5_font_size", "16" ); 
$h6 = get_option( "h6_font_size", "14" ); 
?>

h1{				font-size:<?php echo $h1; ?>px;	line-height:<?php echo $h1+5; ?>px; }
h2{				font-size:<?php echo $h2; ?>px;	line-height:<?php echo $h2+7; ?>px; }
h3{				font-size:<?php echo $h3; ?>px;	line-height:<?php echo $h3+5; ?>px; }
h4{				font-size:<?php echo $h4; ?>px;	line-height:<?php echo $h3+7; ?>px; }
h5{				font-size:<?php echo $h5; ?>px; }
h6{				font-size:<?php echo $h6; ?>px; }

<?php echo get_option( "custom_css" ); ?>

<?php
	exit;
	}	
	
	
	if($get_var == "js") {
	header('Content-type: text/javascript');
?>
/* some settings */
T_URI				= "<?php echo T_URI; ?>";
T_NAME				= "<?php echo T_NAME; ?>";
F_PATH				= "<?php echo F_PATH; ?>";

/* sticky footer */
_sticky_footer		= "<?php echo get_option("sticky_footer", "on"); ?>";

/* twitter settings */
_twitter_active		= "<?php echo get_option("twitter_active", "on"); ?>";
_twitter_id			= "<?php echo get_option("twitter_id", "Envato"); ?>";
_twitter_items		= <?php echo get_option("twitter_items", "5"); ?>;
_twitter_delay		= <?php echo get_option("twitter_delay", "5"); ?>;

/* quickly contact form and some fields */
_form_name 			= "<?php echo get_option("field_name", "Name *"); ?>";
_form_email			= "<?php echo get_option("field_email", "Email *"); ?>";
_form_tel			= "<?php echo get_option("field_telephone", "Telephone"); ?>";
_form_subject		= "<?php echo get_option("field_subject", "Subject *"); ?>";
_form_message		= "<?php echo get_option("field_message", "Message *"); ?>";
_form_comment		= "<?php echo get_option("field_message", "Message *"); ?>";
_form_url			= "<?php echo get_option("field_url", "Web Site"); ?>";

_error_name			= "<?php echo get_option("_error_name", "Error, Please check <strong>your name</strong> :("); ?>";
_error_email		= "<?php echo get_option("_error_email", "Error, Please check <strong>your email</strong> :("); ?>";
_error_subject		= "<?php echo get_option("_error_subject", "Error, Please check <strong>your subject</strong> :("); ?>";
_error_message		= "<?php echo get_option("_error_message", "Error, Please check <strong>your message</strong> :("); ?>";
_error_comment		= "<?php echo get_option("_error_comment", "Error, Please check <strong>your comment</strong> :("); ?>";
_send_message		= "<?php echo get_option("_send_message", "Please wait, sending..."); ?>";
_thanks_message		= "<?php echo get_option("_thanks_message", "Success! Thank you for contact."); ?>";

/* prettyPhoto settings */
_prettyPhoto_active		= "<?php echo get_option("prettyphoto_active", "on"); ?>";
_prettyphoto_social		= "<?php echo get_option("prettyphoto_social", "on"); ?>";
_prettyphoto_autoplay	= <?php if ( get_option("prettyphoto_autoplay", "off") == "on" ) { echo "true"; } else { echo "false"; } ?>;
_prettyphoto_speed		= <?php echo get_option("prettyphoto_speed", "10") * 1000; ?>;

<?php
		exit;
	}	

}
?>