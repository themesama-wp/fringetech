<?php
global $wpdb;

function get_slider_title(){

	global $sliders;
	return $sliders[($_GET["id"]-1)];
	
}


$action = @$_REQUEST["do"];

switch($action){

	case 'add_category':
	
		if( $_REQUEST["options"]["c_name"] ) {
			
			$c_value	= get_the_requests($_REQUEST["options"], array("c_name", "c_desc") );
			$c_name		= makeSafe($_POST["options"]["c_name"]);
			$c_desc		= makeSafe($_POST["options"]["c_desc"]);
			$s_id		= $_POST["slider_id"];
			
			$sql_exec = "INSERT INTO ". $category_table ." SET 
			s_id	= '". $s_id ."', 
			c_name	= '". $c_name ."',
			c_desc	= '". $c_desc ."',
			c_value	= '". $c_value . "',
			orderby	= '0'";
			$wpdb->query( $sql_exec );

			$get_insert_id = $wpdb->insert_id;
			$upgrade_insert_id = "UPDATE ". $category_table ." SET 
			orderby = '". $get_insert_id ."'
			WHERE id = '". $get_insert_id ."'";
			
			$wpdb->query( $upgrade_insert_id );
			$mysqlresult = "New slider has been <strong><em>created!</em></strong>";
			
		} else {
			$mysqlerror = "error!";
		}
		
	break;

	case 'edit_category':
		
		if( $_REQUEST["options"]["c_name"] ) {
			
			$c_value	= get_the_requests($_REQUEST["options"], array("c_name", "c_desc") );
			$c_name		= makeSafe($_POST["options"]["c_name"]);
			$c_desc		= makeSafe($_POST["options"]["c_desc"]);
			$c_id		= $_POST["edit_id"];
			
			$sql_exec = "UPDATE ". $category_table ." SET 
			c_name = '". $c_name ."',
			c_desc = '". $c_desc ."',
			c_value = '". $c_value . "'
			WHERE id = '". $c_id ."'";
			$wpdb->query( $sql_exec );

			$mysqlresult = "Slider has been <strong><em>updated!</em></strong>";
			
		} else {
		
			$mysqlerror = "error!";
		
		}
		
	break;
	

	case 'del_category':
		
		$insert = "DELETE FROM ". $category_table ." WHERE id = '". $_GET["del_id"] ."'";
		$wpdb->query( $insert );
		
		$mysqlresult = "Slider has been <strong><em>deleted!</em></strong>";
		
	break;


	case 'add_item':
	
		$request_options = $_REQUEST["options"];
		
		if( $request_options["cats"] != "" ) {
			
			$s_value	= get_the_requests($request_options, array("s_name", "s_desc", "cats") );
			$s_name		= makeSafe($_POST["options"]["s_name"]);
			$s_desc		= makeSafe($_POST["options"]["s_desc"]);
			$s_id		= $_POST["slider_id"];
			$c_id		= implode_categories($request_options["cats"]);
			
			$sql_exec = "INSERT INTO ". $item_table ." SET 
			c_id = '". $c_id ."',
			s_id = '". $s_id ."', 
			s_name = '". $s_name ."',
			s_desc = '". $s_desc ."',
			s_value = '". $s_value . "',
			orderby = '0'";
			$wpdb->query( $sql_exec );

			$get_insert_id = $wpdb->insert_id;
			$upgrade_insert_id = "UPDATE ". $item_table ." SET 
			orderby = '". $get_insert_id ."'
			WHERE id = '". $get_insert_id ."'";
			
			$wpdb->query( $upgrade_insert_id );
			
			$mysqlresult = "New slider item has been <strong><em>created!</em></strong>";

			
		} else {
		
			$mysqlerror = "error!";
		
		}
		
	break;	


	case 'edit_item':
	
		$request_options = $_REQUEST["options"];
		
		if( $request_options["cats"] != "" ) {
			
			$s_value	= get_the_requests($request_options, array("s_name", "s_desc", "cats") );
			$s_name		= makeSafe($_POST["options"]["s_name"]);
			$s_desc		= makeSafe($_POST["options"]["s_desc"]);
			$c_id		= implode_categories($request_options["cats"]);
			$item_id 	= $_POST["item_id"];
			
			$sql_exec = "UPDATE ". $item_table ." SET 
			c_id = '". $c_id ."',
			s_name = '". $s_name ."',
			s_desc = '". $s_desc ."',
			s_value = '". $s_value . "'
			WHERE id = '" . $item_id . "'";
			$wpdb->query( $sql_exec );

			$mysqlresult = "Slider item has been <strong><em>updated!</em></strong>";
			
		} else {
		
			$mysqlerror = "error!";
		}
		
	break;	

	case 'del_item':
		
		$insert = "DELETE FROM ". $item_table ." WHERE id = '". $_GET["del_id"] ."'";
		$wpdb->query( $insert );
		
		$mysqlresult = "Slider item has been <strong><em>deleted!</em></strong>";
		
	break;	
	
}


if( $sliders[0] != "") {

	add_action('admin_menu', 'add_slider_menu');
	function add_slider_menu() {

		global $menu;

		$slider_position = 8;
		if( !empty ( $menu[$slider_position] ) ){ $slider_position = null; }
		
		$slider_icon = get_template_directory_uri().'/'.F_PATH.'/libs/images/slider_icon.png';
		add_menu_page('Sliders', 'Sliders', 'manage_options', 'sliders', 'sliders', $slider_icon, $slider_position);

	}

}

function sliders(){
	global $wpdb, $sliders, $category_table, $item_table, $option_table, $mysqlresult, $mysqlerror;

	echo '<div class="wrap"><div class="icon32" id="icon-edit"><br /></div>';


	$page = @$_GET["p"];
	
	switch ($page){
		
		case 'show':
			echo "<h2>Sliders of ". get_slider_title() ."<a class=\"button add_new_button\"  href=\"?page=sliders&p=add&id=". $_GET["id"] ."\">Add New Item</a></h2>";
		break;

		case 'add':
			echo "<h2>Add New Slider Item</h2>";
		break;
		
		case 'category':
			echo "<h2>Sliders of ". get_slider_title() ."</h2>";
		break;

		case 'settings':
			echo "<h2>Settings of ". get_slider_title() ."</h2>";
		break;
		
		default:
			echo "<h2>List of Slider Managers</h2>";
		break;
	}
	

	if ($mysqlresult){
		echo '<div class="updated"><p>'. $mysqlresult .'</p></div>';
	}
	
	if ($mysqlerror){
		echo '<div class="error"><p>There is a <strong><em>'. $mysqlerror .'</em></strong>.</p></div>';
	}

	if( ! @$_REQUEST["id"] ){
?>
	
	<div class="sliders_warp">
	
		<ul class="slider_ul">
		
			<?php 
			$item_count = 0;

			foreach ($sliders as $key => $slider){ 

				$cur_slider_id = $key+1;
				$item_count = $wpdb->get_var($wpdb->prepare("SELECT count(c_id) FROM ". $item_table ." WHERE s_id = %d", $cur_slider_id));
			?>
			<li>
			
				<span class="slider_img slider_id_<?php echo $cur_slider_id; ?>">&nbsp;</span>
				
				<ul>
					<li class="icon_preview"><a href="?page=sliders&p=show&id=<?php echo $cur_slider_id; ?>">Show all slider items (<?php echo $item_count; ?>)</a></li>
					<li class="icon_add"><a href="?page=sliders&p=add&id=<?php echo $cur_slider_id; ?>">Add new slider item</a></li>
					<li class="icon_category"><a href="?page=sliders&p=category&id=<?php echo $cur_slider_id; ?>">Add new slider</a></li>
				</ul>
				
			</li>
			<?php } ?>
		
		</ul>
		
	</div>
	
<?php
	}

	else if(@$_REQUEST["p"] == "show"){
	

		if(@$_GET["m"] != "" && $_GET["cur_id"] != "") {
			move_item($_GET["cur_id"], $_GET["m"], $item_table, "orderby", "DESC", "c_id");
		}
?>
  
    <div class="tablenav" style="margin-top:15px;">
	
		<div class="alignleft actions">
        
			<form method="post" action="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>">
				
				<select class="slider_filter" name="filterby">
				<?php
					$filter_categories = $wpdb->get_results("SELECT * FROM ". $category_table ." WHERE s_id = '". $_GET["id"] ."' ORDER BY orderby DESC");
					if(count($filter_categories) <= 0){
					
						echo "<option value=\"\">There is not any slider item.</option>";
					
					} else {
					
						echo "<option value=\"\">View all sliders</option>";
						
						foreach ($filter_categories as $get_data) {
							
							$d = $get_data->id;
							$count_filter = $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ". $item_table ." WHERE c_id REGEXP '(^".$d.",)|(,".$d."$)|(,".$d.",)|^".$d."$'"));

							$selected = "";
							if( $_REQUEST["filterby"] == $get_data->id ){ $selected = "selected=\"selected\""; }
							echo "<option value=\"" . $get_data->id . "\" " . $selected . ">" . htmlSafe($get_data->c_name) . " " . "(" . $count_filter . ")" . "</option>";
							
						}	
					}
				?>
				</select>
				<input type="submit" class="button-secondary" value="Filter" />
			
			</form>
	
		</div>

        <div class="view-switch">
			<a class="button" href="?page=sliders&p=add&id=<?php echo $_GET["id"]; ?>">Add new slider item</a>
			<a class="button" href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>">Add new slider</a>
			<a href="javascript:history.back();" class="button">Go Back</a>
		</div>
		<div class="clear"></div>
      
    </div>
    
    <div class="clear"></div>
	
    <table cellspacing="0" class="widefat">
    
		<thead>
			<tr>
				<th style="width:25px;" align="center">ID</th>
				<th class="column-column_preview">Preview</th>
				<th class="manage-column column-title" id="title" scope="col">Title</th>
				<th style="width:200px;">Sliders</th>
				<th style="width:50px;">&nbsp;</th>
				<th style="width:50px;">&nbsp;</th>
				<th style="width:50px;">&nbsp;</th>
			</tr>
		</thead>
	  
		<tfoot>
			<tr>
				<th colspan="7">&nbsp;</th>
			</tr>
		</tfoot>
		
		<tbody>
        
			<?php
				$sql_regex = "";
				if( @$_REQUEST["filterby"] != "" ){
					
					$v = $_REQUEST["filterby"];
					$sql_regex = "AND c_id REGEXP '(^".$v.",)|(,".$v."$)|(,".$v.",)|^".$v."$'";
					
				}
				
				$slider_items	= $wpdb->get_results("SELECT * FROM ". $item_table ." WHERE s_id = '". @$_GET["id"] ."' " . $sql_regex . " ORDER BY orderby DESC");
				$total_items	= count($slider_items);
				
				if($total_items <= 0){
					echo "<tr><th height=\"50\" colspan=\"5\">There is not any slider item.</th></tr>";
				}

				$a = 1; 
				$i = 0; 
				
				foreach ($slider_items as $item) {
			?>
			<tr valign="middle" class="<?php if($a > 1){ echo "alternate"; $a = 0; } ?>">

				<th><?php echo $item->id; ?></th>
				
				<th><a href="?page=sliders&p=add&id=<?php echo $_GET["id"]; ?>&edit_id=<?php echo $item->id;?>"><span class="<?php if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', get_the_value("s_picture", htmlSafe( $item->s_value) ) ) ) { echo "upload_pic"; } else { echo "external_pic"; } ?>"><?php if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', get_the_value("s_picture", htmlSafe( $item->s_value) ) ) ) { ?><img height="70" width="70" alt="" src="<?php echo get_the_value("s_picture", htmlSafe($item->s_value)); ?>" /><?php } ?></span></a></th>
				
				<th><strong><a href="?page=sliders&p=add&id=<?php echo $_GET["id"]; ?>&edit_id=<?php echo $item->id;?>" class="row-title"><?php echo htmlSafe( $item->s_name ); ?></a></strong></th>
				
				<th>
				<?php
					$cats = explode(",", $item->c_id);
					foreach($cats as $cat){
						$cat_name = $wpdb->get_row("SELECT id,c_name FROM " . $category_table . " WHERE id = '" . $cat . "'");
						echo "<a href=\"?page=sliders&p=show&id=" . $_GET["id"] . "&filterby=" .  $cat . "\">" . htmlSafe($cat_name->c_name) . "</a><br />";
					}
				?>
				</th>
				
				<th><a href="?page=sliders&p=add&id=<?php echo $_GET["id"]; ?>&edit_id=<?php echo $item->id;?>" class="icon_edit">&nbsp;</a></th>
				
				<th><a href="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>&do=del_item&del_id=<?php echo $item->id;?>" onclick="return confirm('Are you sure you want to delete?');" class="icon_remove">&nbsp;</a></th>
				
				<th>
				
					<?php if( $_REQUEST["filterby"] != ""){ ?>
					
						<?php if($i!=0) { ?>
						<a href="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>&m=up&cur_id=<?php echo $item->id; ?>&filterby=<?php echo $cat; ?>" class="icon_up">&nbsp;</a>
						
						<?php } if($i != $total_items-1 ) { ?>
						<a href="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>&m=down&cur_id=<?php echo $item->id; ?>&filterby=<?php echo $cat; ?>" class="icon_down">&nbsp;</a>
						<?php } ?>
					
					<?php } ?>
				
				</th>

			</tr>	
			<?php $a++; $i++; } ?>
		
		</tbody>
		
    </table>
	<br />
<?php
	}
	
	else if($_REQUEST["p"] == "add"){
	
		if($_GET["edit_id"]){
			$edit_data_slider = $wpdb->get_row("SELECT * FROM ". $item_table ." WHERE id = '". $_GET["edit_id"] ."'");
		}
?>

	<form method="post" action="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>">
    
		<div class="has-right-sidebar" id="poststuff">
		
			<div class="inner-sidebar">
				
				<div id="side-sortables">
		
					<div class="postbox" id="submitdiv">
					
						<h3><span>Choose a slider(s)</span></h3>
						
						<div class="inside">
						
							<div class="categorydiv">
							
								<ul class="category-tabs">
									<li class="tabs">All Sliders</li>
									<li><a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>">+ Add more slider(s)</a></li>
								</ul>
								
								<div class="tabs-panel">
								
									<ul>
								  
										<?php
										$getting_sliders = $wpdb->get_results("SELECT * FROM ". $category_table ." WHERE s_id = '". $_GET["id"] ."' ORDER BY orderby DESC");
										
										if( count($getting_sliders) <= 0 ){
											echo "<li>There is not any slider</li>";
										} 
										
										foreach ($getting_sliders as $get_data){ 
										
											$str_out	= "";
											
											$str_out	.= '<li><label class="selectit">';
											
											$str_out	.= '<input type="checkbox" class="categories_sliders" name="options[cats][categories][]" value="' . $get_data->id . '" ';
											
											if( $_GET["c_id"] == $get_data->id ) { 
											
												$str_out	.= "checked=\"checked\"";
											
											}
											
											if( $edit_data_slider->c_id != ""){
											
												$mkarray = explode(",", $edit_data_slider->c_id);
												
												if ( in_array( trim( $get_data->id ), $mkarray) ) {
													
													$str_out	.= "checked=\"checked\"";
												
												}
											
											}
											
											$str_out	.= '> ';
											$str_out	.= htmlSafe($get_data->c_name);
											$str_out	.= '</label></li>';
											
											echo $str_out;
										
										}
										
										?>
								  
									</ul>

								</div>
								
							</div>

							<div id="major-publishing-actions">
								
								<div class="go_back_save"><a href="javascript:history.back();" class="button">Go Back</a></div>
								
								<div id="publishing-action">
									
									<?php if($_GET["edit_id"]){ ?>
									
									<input type="hidden" value="edit_item" name="do">
									<input type="hidden" value="<?php echo $_GET["edit_id"]; ?>" name="item_id">
									<input type="submit" value="Update" class="button-primary save_new_item" name="update">
									
									<?php } else { ?>
									
									<input type="hidden" value="add_item" name="do">
									<input type="hidden" value="<?php echo $_GET["c_id"]; ?>" name="c_id">
									<input type="hidden" value="<?php echo $_GET["id"]; ?>" name="slider_id">
									<input type="submit" value="Save" class="button-primary save_new_item" name="save">
									
									<?php } ?>
									
								</div>
								
								<div class="clear"></div>
							
							</div>
							
						</div>
						
					</div>

				</div>
			
			</div>

		  
			<div id="post-body">
			
			
				<div id="post-body-content">
				
					<?php include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/slider_item_fields.php"); ?>

				</div>
		  
			</div>
		
		</div>
	
	</form>
	


<?php
	}
	
	else if($_REQUEST["p"] == "category"){
		
		if( @$_GET["m"] != "" && @$_GET["cur_id"] != "") {
			
			move_item($_GET["cur_id"], $_GET["m"], $category_table, "orderby", "DESC", "s_id");

		}
?>

	<div id="col-container">

		<div id="col-right">

			<div class="tablenav">
				<div class="alignright actions">
					<a href="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>" class="button" style="float:left; margin-right:10px;">Show all slider items</a>
					<a href="javascript:history.back();" class="button" style="float:left;">Go Back</a>
				</div>
			</div>

			<div class="clear"></div>
			
			<table cellspacing="0" class="widefat">
			
				<thead>
					<tr>
						<th style="width:25px;" align="center">ID</th>
						<th>Slider(s) Name</th>
						<th class="num" style="width:75px;">Items</th>
						<th style="width:50px;">&nbsp;</th>
						<th style="width:50px;">&nbsp;</th>
						<th style="width:50px;">&nbsp;</th>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<th colspan="6"><em>( Choose a slider for add to slider item )</em></th>
					</tr>
				</tfoot>

				<tbody>
					<?php
					$slider_categories = $wpdb->get_results("SELECT * FROM ". $category_table ." WHERE s_id = '". $_GET["id"] ."' ORDER BY orderby DESC");
					$slider_count = count($slider_categories);

					if($slider_count <= 0){
						
						echo '<tr><th height="50" colspan="4">There is not any slider</th></tr>';
						
					}
					
					$a = 1;
					$i = 0;

					foreach ($slider_categories as $get_data) {
						$h = $get_data->id;
						$item_count = $wpdb->get_var($wpdb->prepare("SELECT count(c_id) FROM ". $item_table ." WHERE c_id RLIKE '^".$h."$' OR c_id RLIKE ',".$h."$' OR c_id RLIKE '^".$h.",' OR c_id RLIKE ',".$h.",' ", ''));
					?>
					<tr class="<?php if($a > 1){ echo "alternate"; $a = 0; } ?>">
						
						<th height="50" align="center"><?php echo $get_data->id; ?></th>
						
						<th><strong><a href="?page=sliders&p=add&id=<?php echo $_GET["id"]; ?>&c_id=<?php echo $get_data->id;?>" style="padding-top:5px;"><?php echo htmlSafe($get_data->c_name);?></a></strong></th>
						
						<th class="num"><a href="?page=sliders&p=show&id=<?php echo $_GET["id"]; ?>&filterby=<?php echo $get_data->id; ?>"><?php echo $item_count;?></a></th>
						
						<th><a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>&edit_id=<?php echo $get_data->id;?>" class="icon_edit">&nbsp;</a></th>
						
						<th><a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>&do=del_category&del_id=<?php echo $get_data->id;?>" onclick="return confirm('Are you sure you want to delete?');" class="icon_remove">&nbsp;</a></th>
						
						<th>
							
							<?php if($i!=0) { ?>
							<a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>&m=up&cur_id=<?php echo $get_data->id; ?>" class="icon_up">&nbsp;</a>
							<?php } if($i != $slider_count-1 ) { ?>
							<a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>&m=down&cur_id=<?php echo $get_data->id; ?>" class="icon_down">&nbsp;</a>
							<?php } ?>
							
						</th>
						
					</tr>
					<?php $a++; $i++; } ?>
					
				</tbody>
				
			</table>
			
		</div>
		
		<?php 
			if( @$_GET["edit_id"] ){
			
				$edit_data = $wpdb->get_row("SELECT * FROM ". $category_table ." WHERE id = '". $_GET["edit_id"] ."'");
			
			}
		?>
		<div id="col-left">
		
			<div class="col-wrap">
			
				<div class="form-wrap">

					<form action="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>" method="post">

						<?php if($_GET["edit_id"]){ ?>
						<h3>Update "<?php echo htmlSafe($edit_data->c_name); ?>" slider</h3>
						<?php } else { ?>
						<h3>Add New <?php echo get_slider_title(); ?></h3>
						<?php } ?>
					
						<div class="slider_form_field">
							<label for="c_name">Name</label>
							<?php render_item("input", 'c_name', htmlSafe($edit_data->c_name) ); ?>
						</div>
						
						<div class="slider_form_field">
							<label for="c_desc">Description <em>(optional)</em></label>
							<?php render_item("textarea", 'c_desc', htmlSafe($edit_data->c_desc) ); ?>
						</div>
						
						<?php include_once( TEMPLATEPATH ."/". F_PATH ."/libs/fields/slider_category_fields.php"); ?>
						
						<?php
							if($_GET["edit_id"]){
						?>
						
						<input type="hidden" value="edit_category" name="do">
						<input type="hidden" value="<?php echo $_GET["edit_id"]; ?>" name="edit_id">
						<input type="submit" value="Update" id="submit_slider_category" name="submit" class="button">
						<a href="?page=sliders&p=category&id=<?php echo $_GET["id"]; ?>" class="button" style="float:right;">Cancel Update or Go Back to Add New <?php echo get_slider_title(); ?></a>
						
						<?php } else { ?>
						
						<input type="hidden" value="<?php echo $_GET["id"]; ?>" name="slider_id">
						<input type="hidden" value="add_category" name="do">
						<input type="submit" value="Add New <?php echo get_slider_title(); ?>" id="submit_slider_category" name="submit" class="button">
						
						<?php } ?>
					
					</form>
					
				</div>
				
			</div>
			
		</div>
		
	</div>

<?php } ?>

</div>
<?php } ?>