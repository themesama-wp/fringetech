<?php
/*	starting with bonus slider */	
if( $_GET["id"] == 1 ){ 
?>

<div id="titlediv">
	<div id="titlewrap">
		<div style="margin:10px 0;"><strong>Slider Title</strong> <em>(optional)</em></div>
		<?php render_item("input", "s_name", htmlSafe($edit_data_slider->s_name) ); ?>
	</div>
</div>


<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Picture</span></h3>
	
	<div class="inside">
	
		<div class="slider_custom_fields">
		
			<div id="upload_s_picture_preview" class="upload_pic"></div>
			
			<div class="upload_preview_input">

				<div class="upload_preview_warp">
					<?php render_item('upload', 's_picture', htmlSafe( get_the_value("s_picture", $edit_data_slider->s_value) ) ); ?>
				</div>
				
			</div>
			
		</div>
		

	</div>
  
</div>


<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Link</span></h3>
	
	<div class="inside">
		

			<div class="option_label"><strong>Ex. http://www.google.com.tr/</strong></div>
			<?php 
				$optionName = "s_link";
				render_item('input', $optionName, '', htmlSafe( get_the_value($optionName, $edit_data_slider->s_value) ) ); 
			?>
			
			<div style="float:left;width:100%; margin-top:10px;">&nbsp;</div>
			
			<div class="option_label"><strong>Link Target</strong></div>
			<?php 
				$optionName = "s_target";
				render_item("select", $optionName, "_self,_blank", get_the_value($optionName, $edit_data_slider->s_value) ); 
			?>
			
		
	</div>
  
</div>

<?php } if( $_GET["id"] == 2 ){ ?>

<div id="titlediv">
	<div id="titlewrap">
		<div style="margin:10px 0;"><strong>Slider Title</strong> <em>(optional)</em></div>
		<?php render_item("input", "s_name", htmlSafe($edit_data_slider->s_name) ); ?>
	</div>
</div>

<div style="margin:10px 0;"><strong>Slider Description</strong> <em>(optional)</em></div>
<?php render_item("textarea", "s_desc", htmlSafe($edit_data_slider->s_desc)); ?>

<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Picture</span></h3>
	
	<div class="inside">
	
		<div class="slider_custom_fields">
		
			<div id="upload_s_picture_preview" class="upload_pic"></div>
			
			<div class="upload_preview_input">

				<div class="upload_preview_warp">
					<?php render_item('upload', 's_picture', htmlSafe( get_the_value("s_picture", $edit_data_slider->s_value) ) ); ?>
				</div>
				
			</div>
			
		</div>
		

	</div>
  
</div>


<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Link</span></h3>
	
	<div class="inside">
		

			<div class="option_label"><strong>Ex. http://www.google.com.tr/</strong></div>
			<?php 
				$optionName = "s_link";
				render_item('input', $optionName, '', htmlSafe( get_the_value($optionName, $edit_data_slider->s_value) ) ); 
			?>
			
			<div style="float:left;width:100%; margin-top:10px;">&nbsp;</div>
			
			<div class="option_label"><strong>Link Target</strong></div>
			<?php 
				$optionName = "s_target";
				render_item("select", $optionName, "_self,_blank", get_the_value($optionName, $edit_data_slider->s_value) ); 
			?>
			
		
	</div>
  
</div>

<?php } if( $_GET["id"] == 3 ){ ?>

<div id="titlediv">
	<div id="titlewrap">
		<div style="margin:10px 0;"><strong>Slider Title</strong> <em>(optional)</em></div>
		<?php render_item("input", "s_name", htmlSafe($edit_data_slider->s_name) ); ?>
	</div>
</div>

<div style="margin:10px 0;"><strong>Slider Description</strong> <em>(optional)</em></div>
<?php render_item("textarea", "s_desc", htmlSafe($edit_data_slider->s_desc)); ?>

<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Picture</span></h3>
	
	<div class="inside">
	
		<div class="slider_custom_fields">
		
			<div id="upload_s_picture_preview" class="upload_pic"></div>
			
			<div class="upload_preview_input">

				<div class="upload_preview_warp">
					<?php render_item('upload', 's_picture', htmlSafe( get_the_value("s_picture", $edit_data_slider->s_value) ) ); ?>
				</div>
				
			</div>
			
		</div>
		

	</div>
  
</div>


<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Link</span></h3>
	
	<div class="inside">
		

			<div class="option_label"><strong>Ex. http://www.google.com.tr/</strong></div>
			<?php 
				$optionName = "s_link";
				render_item('input', $optionName, '', htmlSafe( get_the_value($optionName, $edit_data_slider->s_value) ) ); 
			?>
			
			<div style="float:left;width:100%; margin-top:10px;">&nbsp;</div>
			
			<div class="option_label"><strong>Link Target</strong></div>
			<?php 
				$optionName = "s_target";
				render_item("select", $optionName, "_self,_blank", get_the_value($optionName, $edit_data_slider->s_value) ); 
			?>
			
		
	</div>
  
</div>


<?php } if( $_GET["id"] == 4 ){ ?>

<div id="titlediv">
	<div id="titlewrap">
		<div style="margin:10px 0;"><strong>Slider Short Title (first title)</strong> <em>(optional)</em></div>
		<?php 
			$optionName = "s_short_name";
			render_item('input', $optionName, '', htmlSafe( get_the_value($optionName, $edit_data_slider->s_value) ) ); 
		?>
	</div>
</div>

<div id="titlediv">
	<div id="titlewrap">
		<div style="margin:10px 0;"><strong>Slider Title</strong> <em>(optional)</em></div>
		<?php render_item("input", "s_name", htmlSafe($edit_data_slider->s_name) ); ?>
	</div>
</div>



<div style="margin:10px 0;"><strong>Slider Description</strong> <em>(optional)</em></div>
<?php render_item("textarea", "s_desc", htmlSafe($edit_data_slider->s_desc)); ?>

<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Picture</span></h3>
	
	<div class="inside">
	
		<div class="slider_custom_fields">
		
			<div id="upload_s_picture_preview" class="upload_pic"></div>
			
			<div class="upload_preview_input">

				<div class="upload_preview_warp">
					<?php render_item('upload', 's_picture', htmlSafe( get_the_value("s_picture", $edit_data_slider->s_value) ) ); ?>
				</div>
				
			</div>
			
		</div>
		

	</div>
  
</div>


<div class="postbox" style="margin-top:20px;">
  
	<h3><span>Slider Link</span></h3>
	
	<div class="inside">
		

			<div class="option_label"><strong>Ex. http://www.google.com.tr/</strong></div>
			<?php 
				$optionName = "s_link";
				render_item('input', $optionName, '', htmlSafe( get_the_value($optionName, $edit_data_slider->s_value) ) ); 
			?>
			
			<div style="float:left;width:100%; margin-top:10px;">&nbsp;</div>
			
			<div class="option_label"><strong>Link Target</strong></div>
			<?php 
				$optionName = "s_target";
				render_item("select", $optionName, "_self,_blank", get_the_value($optionName, $edit_data_slider->s_value) ); 
			?>
			
		
	</div>
  
</div>

<?php } ?>