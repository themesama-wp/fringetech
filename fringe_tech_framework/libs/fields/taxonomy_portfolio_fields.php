<div style="display:table; <?php if( @$_REQUEST["action"] == "edit" ) { echo "clear: both; margin-top:20px; margin-left: 230px; width: 50%;"; } ?>">

	<div class="option_holder">
		
		<div class="option_label"><strong>Thumbnail width</strong></div>
		<?php 
			$optionName = "p_width";
			render_item('slider_ui', $optionName, '280,960,1,px,width', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">
		
		<div class="option_label"><strong>Thumbnail height</strong></div>
		<?php 
			$optionName = "p_height";
			render_item('slider_ui', $optionName, '150,960,1,px,height', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">
		
		<div class="option_label"><strong>Portfolio Columns</strong></div>
		<?php 
			$optionName = "p_column";
			render_item('slider_ui', $optionName, '3,10,1,column(s)', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">

		<div class="option_label"><strong>Portfolio Item Padding</strong></div>
		<?php 
			$optionName = "p_padding";
			render_item('slider_ui', $optionName, '40,100,1,px', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">

		<div class="option_label"><strong>Portfolio Item Limit (per page)</strong></div>
		<?php 
			$optionName = "p_limit";
			render_item('slider_ui', $optionName, '9,100,1,page item', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>


	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Read More" link ?</strong></div>
		<?php 
			$optionName = "p_more";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>
	
	
	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Post Date" ?</strong></div>
		<?php 
			$optionName = "p_date";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Effect UP" ?</strong></div>
		<?php 
			$optionName = "p_up";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>
	
	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Title" ?</strong></div>
		<?php 
			$optionName = "p_title";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Short Description" ?</strong></div>
		<?php 
			$optionName = "p_desc";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>
	
	<div class="option_holder">

		<div class="option_label"><strong>Would you like "jQuery Filterable Portfolio" ?</strong> <em>(Sortable working by tags)</em></div>
		<?php 
			$optionName = "p_filter";
			render_item('on_off_ui', $optionName, 'off', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>
	
	<div class="option_holder">

		<div style="clear:both; margin-top:10px; float:left; display:table;">
			
			<span class="available_formats"><strong>Click for columns settings without Sidebars (how-to):</strong></span>
			
			<ul class="hows_to">
				<li>&bull; One column<span>Pre-Configured Settings<br /><strong>width:</strong> 950, <strong>height:</strong> 350, <strong>column(s):</strong> 1, <strong>padding:</strong> 30, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="one-column">Click here to use these settings</a></span></li>
				<li>&bull; 2 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 450, <strong>height:</strong> 300, <strong>column(s):</strong> 2, <strong>padding:</strong> 35, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="2-columns">Click here to use these settings</a></span></li>
				<li>&bull; 3 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 280, <strong>height:</strong> 150, <strong>column(s):</strong> 3, <strong>padding:</strong> 40, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="3-columns">Click here to use these settings</a></span></li>
				<li>&bull; 4 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 200, <strong>height:</strong> 125, <strong>column(s):</strong> 4, <strong>padding:</strong> 37, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="4-columns">Click here to use these settings</a></span></li>
				<li>&bull; 5 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 160, <strong>height:</strong> 100, <strong>column(s):</strong> 5, <strong>padding:</strong> 25, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="5-columns">Click here to use these settings</a></span></li>
			</ul>
			
		</div>
		
		<div style="clear:both; margin-top:10px; float:left; display:table;">
			
			<span class="available_formats"><strong>Click for columns settings with Sidebars (how-to):</strong></span>
			
			<ul class="hows_to">
				<li>&bull; One column<span>Pre-Configured Settings<br /><strong>width:</strong> 700, <strong>height:</strong> 350, <strong>column(s):</strong> 1, <strong>padding:</strong> 30, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="one-column-ws">Click here to use these settings</a></span></li>
				<li>&bull; 2 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 325, <strong>height:</strong> 250, <strong>column(s):</strong> 2, <strong>padding:</strong> 35, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="2-columns-ws">Click here to use these settings</a></span></li>
				<li>&bull; 3 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 200, <strong>height:</strong> 150, <strong>column(s):</strong> 3, <strong>padding:</strong> 35, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="3-columns-ws">Click here to use these settings</a></span></li>
				<li>&bull; 4 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 140, <strong>height:</strong> 100, <strong>column(s):</strong> 4, <strong>padding:</strong> 25, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="4-columns-ws">Click here to use these settings</a></span></li>
				<li>&bull; 5 columns<span>Pre-Configured Settings<br /><strong>width:</strong> 110, <strong>height:</strong> 90, <strong>column(s):</strong> 5, <strong>padding:</strong> 25, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="5-columns-ws">Click here to use these settings</a></span></li>
			</ul>
			
		</div>
		
		<div style="clear:both; margin-top:10px; float:left; display:table;">
			
			<span class="available_formats"><strong>Click for bonus columns settings (how-to):</strong></span>
			
			<ul class="hows_to">
				<li>&bull; Just Photos ( 5 Columns, No Attributes )<span>Pre-Configured Settings<br /><strong>width:</strong> 160, <strong>height:</strong> 100, <strong>column(s):</strong> 5, <strong>padding:</strong> 25, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="just-photos">Click here to use these settings</a></span></li>
				<li>&bull; Just Photos ( 3 Columns, Just Title )<span>Pre-Configured Settings<br /><strong>width:</strong> 280, <strong>height:</strong> 150, <strong>column(s):</strong> 3, <strong>padding:</strong> 40, <strong>limit:</strong> 10 <br /><a href="javascript:void(0);" class="set_auto_portfolio" title="just-photos-2">Click here to use these settings</a></span></li>
			</ul>
			
		</div>
		
		
		<div style="clear:both; margin-top:10px; float:left; display:table;">
			
			<span class="available_formats"><strong>Also</strong> You can create unlimited style portfolio pages! just try settings! and i will add more quickly pre-configured soon! follow me! ;)</span>
		
		</div>
		
		<div class="hows_to_infos">
			<div class="hows_to_infos_bg"></div>
		</div>

	</div>

</div>