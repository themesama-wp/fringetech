<?php if( $_GET["id"] == 1 ){ ?>

<div class="slider_form_field">

	<div class="option_holder">
		<div class="option_label">Would you like slider Arrows ?</div>
		<?php render_item("on_off_ui", 's_arrows', 'on', get_the_value("s_arrows", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider Buttons ?</div>
		<?php render_item("on_off_ui", 's_buttons', 'on', get_the_value("s_buttons", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider AutoPlay ?</div>
		<?php render_item("on_off_ui", 's_autoplay', 'on', get_the_value("s_autoplay", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider Drop Shadow ?</div>
		<?php render_item("on_off_ui", 's_shadow', 'on', get_the_value("s_shadow", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Would you like slider Line ?</div>
		<?php render_item("on_off_ui", 's_line', 'on', get_the_value("s_line", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Align Position ?</div>
		<?php render_item("select", 's_position', 'center=selected,left,right', get_the_value("s_position", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider max-width</div>
		<?php render_item("slider_ui", 's_width', "960,960,1,px", get_the_value("s_width", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>

	<div class="option_holder">
		<div class="option_label">Slider max-height</div>
		<?php render_item("slider_ui", 's_height', "350,1000,1,px", get_the_value("s_height", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Speed (Delay Time)</div>
		<?php render_item("slider_ui", 's_delay', "5,50,1,second", get_the_value("s_delay", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Top Between</div>
		<?php render_item("slider_ui", 's_padding_top', "10,100,1,px", get_the_value("s_padding_top", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Bottom Between</div>
		<?php render_item("slider_ui", 's_padding_bottom', "10,100,1,px", get_the_value("s_padding_bottom", $edit_data->c_value) ); ?>
	</div>
	
</div>

<?php } if( $_GET["id"] == 2 ){ ?>

<div class="slider_form_field">

	<div class="option_holder">
		<div class="option_label">Would you like slider Arrows ?</div>
		<?php render_item("on_off_ui", 's_arrows', 'on', get_the_value("s_arrows", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider Buttons ?</div>
		<?php render_item("on_off_ui", 's_buttons', 'on', get_the_value("s_buttons", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider AutoPlay ?</div>
		<?php render_item("on_off_ui", 's_autoplay', 'on', get_the_value("s_autoplay", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider Drop Shadow ?</div>
		<?php render_item("on_off_ui", 's_shadow', 'on', get_the_value("s_shadow", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Would you like slider Line ?</div>
		<?php render_item("on_off_ui", 's_line', 'on', get_the_value("s_line", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Would you like slider pause on Mouse Hover ?</div>
		<?php render_item("on_off_ui", 's_pause', 'on', get_the_value("s_pause", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Effect ?</div>
		<?php render_item("select", 's_effect', 'random=selected,sliceDownRight,sliceDownLeft,sliceUpRight,sliceUpLeft,sliceUpDown,sliceUpDownLeft,fold,fade,boxRandom,boxRain,boxRainReverse,boxRainGrow,boxRainGrowReverse', get_the_value("s_effect", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Align Position ?</div>
		<?php render_item("select", 's_position', 'center=selected,left,right', get_the_value("s_position", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider max-width</div>
		<?php render_item("slider_ui", 's_width', "960,960,1,px", get_the_value("s_width", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>

	<div class="option_holder">
		<div class="option_label">Slider max-height</div>
		<?php render_item("slider_ui", 's_height', "350,1000,1,px", get_the_value("s_height", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Speed (Delay Time)</div>
		<?php render_item("slider_ui", 's_delay', "5,50,1,second", get_the_value("s_delay", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Top Between</div>
		<?php render_item("slider_ui", 's_padding_top', "10,100,1,px", get_the_value("s_padding_top", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Bottom Between</div>
		<?php render_item("slider_ui", 's_padding_bottom', "10,100,1,px", get_the_value("s_padding_bottom", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Slices</div>
		<?php render_item("slider_ui", 's_slices', "15,100,1,slices", get_the_value("s_slices", $edit_data->c_value) ); ?>
	</div>


	<div class="option_holder">
		<div class="option_label">Slider Title Font Color</div>
		<?php render_item("colorpicker", 's_title_color', '#333333', get_the_value("s_title_color", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Title Bg Color</div>
		<?php render_item("colorpicker", 's_title_bg_color', '#ffffff', get_the_value("s_title_bg_color", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Title Font Size</div>
		<?php render_item("slider_ui", 's_title_size', "15,100,1,px", get_the_value("s_title_size", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="option_holder">
		<div class="option_label">Slider Description Font Color</div>
		<?php render_item("colorpicker", 's_desc_color', '#ffffff', get_the_value("s_desc_color", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Description Bg Color</div>
		<?php render_item("colorpicker", 's_desc_bg_color', '#333333', get_the_value("s_desc_bg_color", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Description Font Size</div>
		<?php render_item("slider_ui", 's_desc_size', "11,100,1,px", get_the_value("s_desc_size", $edit_data->c_value) ); ?>
	</div>

</div>

<?php } if( $_GET["id"] == 3 ){ ?>

<div class="slider_form_field">

	<div class="option_holder">
		<div class="option_label">Would you like slider Buttons ?</div>
		<?php render_item("on_off_ui", 's_buttons', 'on', get_the_value("s_buttons", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider AutoPlay ?</div>
		<?php render_item("on_off_ui", 's_autoplay', 'on', get_the_value("s_autoplay", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label">Would you like slider Drop Shadow ?</div>
		<?php render_item("on_off_ui", 's_shadow', 'on', get_the_value("s_shadow", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Would you like slider Line ?</div>
		<?php render_item("on_off_ui", 's_line', 'on', get_the_value("s_line", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Align Position ?</div>
		<?php render_item("select", 's_position', 'center=selected,left,right', get_the_value("s_position", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider max-width</div>
		<?php render_item("slider_ui", 's_width', "960,960,1,px", get_the_value("s_width", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>

	<div class="option_holder">
		<div class="option_label">Slider max-height</div>
		<?php render_item("slider_ui", 's_height', "350,1000,1,px", get_the_value("s_height", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Speed (Delay Time)</div>
		<?php render_item("slider_ui", 's_delay', "5,50,1,second", get_the_value("s_delay", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Top Between</div>
		<?php render_item("slider_ui", 's_padding_top', "10,100,1,px", get_the_value("s_padding_top", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Bottom Between</div>
		<?php render_item("slider_ui", 's_padding_bottom', "10,100,1,px", get_the_value("s_padding_bottom", $edit_data->c_value) ); ?>
	</div>

</div>

<?php } if( $_GET["id"] == 4 ){ ?>

<div class="slider_form_field">

	<div class="option_holder">
		<div class="option_label">Would you like slider Drop Shadow ?</div>
		<?php render_item("on_off_ui", 's_shadow', 'on', get_the_value("s_shadow", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Would you like slider Line ?</div>
		<?php render_item("on_off_ui", 's_line', 'on', get_the_value("s_line", $edit_data->c_value) ); ?>
	</div>
	
	<div class="option_holder">
		<div class="option_label">Slider Align Position ?</div>
		<?php render_item("select", 's_position', 'center=selected,left,right', get_the_value("s_position", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider max-width</div>
		<?php render_item("slider_ui", 's_width', "960,960,1,px", get_the_value("s_width", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>

	<div class="option_holder">
		<div class="option_label">Slider max-height</div>
		<?php render_item("slider_ui", 's_height', "350,1000,1,px", get_the_value("s_height", $edit_data->c_value) ); ?>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Top Between</div>
		<?php render_item("slider_ui", 's_padding_top', "10,100,1,px", get_the_value("s_padding_top", $edit_data->c_value) ); ?>
	</div>
	
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Slider Padding Bottom Between</div>
		<?php render_item("slider_ui", 's_padding_bottom', "10,100,1,px", get_the_value("s_padding_bottom", $edit_data->c_value) ); ?>
	</div>

	<div class="option_holder">
		<div class="option_label"><strong>Slider Colors & Font Types Settings</strong></div>
	</div>
	
	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Title Color</div>
		<?php render_item("colorpicker", 's_title_color', "#ffffff", get_the_value("s_title_color", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Description Color</div>
		<?php render_item("colorpicker", 's_desc_color', "#777", get_the_value("s_desc_color", $edit_data->c_value) ); ?>
	</div>

	<div class="clear"></div>
	
	<div class="option_holder">
		<div class="option_label">Title & Description Background Color</div>
		<?php render_item("colorpicker", 's_desc_bg_color', "#222222", get_the_value("s_desc_bg_color", $edit_data->c_value) ); ?>
	</div>
	
</div>
<?php } ?>