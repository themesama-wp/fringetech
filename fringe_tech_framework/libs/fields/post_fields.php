<div class="post_fields">
	
	
	<div class="post_thumbnail">
	
		
		<div class="thumbnail_holder">
	
			<div class="post_field_title">Thumbnail Method</div>
			
			<div id="upload__post_thumbnail_preview" class="upload_pic _post_thumbnail"></div>
			
			<div class="upload_preview_input">
			
				<div class="upload_preview_warp">
					<?php render_item('upload', '_post_thumbnail'); ?>
					<div class="post_thumbnail_note">If you do not want to thumbnail.<br />do not write any word. it will be disable automatically!</div>
				</div>
			
			</div>
			
		</div>

	</div>
	
	<div class="post_thumbnail">
	
		<div class="post_field_title">Fullsize Method</div>
		
		<div id="upload__post_fullsize_preview" class="upload_pic"></div>
		
		<div class="upload_preview_input">
			
				<div class="upload_preview_warp">
					<?php render_item('upload', '_post_fullsize'); ?>
				</div>
				
		</div>
		
	</div>
	
	<!-- begin available formats -->
	<div class="post_thumbnail">


		<div class="available_formats">Click for available formats (how-to):</div>
		<ul class="hows_to">
			<li>[ &bull; jpg, png, gif ]<span>Click to Upload button, and click to "Insert into Post" button!<br /><strong>Output:</strong> http://site_link/file.jpg or png or gif</li>
			<li>[ &bull; swf ]<span>Click to Upload button, select your item and click to "Insert into Post" button!<br /><strong>Output:</strong> http://site_link/your_file_name.swf<i>?width=550&height=400</i><br />You must set width & height!</span></li>
			<li>[ &bull; flv, mp4 ]<span>Click to Upload button, select your item and click to "Insert into Post" button!<br /><strong>Output:</strong> http://site_link/screencast.flv<i>&width=600&height=400&autostart=true</i></span></li>
			<li>[ &bull; external-videos ]<span>Upload a thumbnail, and FullSize method write your link so :<br /><strong>Output:</strong> http://youtube,vimeo,yahoo links/<i>?&width=1000&height=600</i></span></li>
			<li>[ &bull; external-links ]<span>Upload a thumbnail, and FullSize method write your link so :<br /><strong>Output:</strong> http://site_link/<i>?type=link&target=_self</i></span></li>
			<li>[ &bull; iframe ]<span>Upload a thumbnail, and FullSize method write your link so :<br /><strong>Output:</strong> http://site_link/<i>?iframe=true&width=800&height=600</i></span></li>
		</ul>

		<div class="hows_to_infos">
			<div class="hows_to_infos_bg"></div>
		</div>
	
	</div>
	<!-- end available formats -->


	<!-- Begin The Editor -->
	<div class="post_thumbnail">
		
		<div class="post_field_title">Short Description</div>
		<?php render_item('tinymce', '_short_description'); ?>
		
	</div>
	<!-- End The Editor -->
	
	<div class="cleardiv"></div>
	
</div>