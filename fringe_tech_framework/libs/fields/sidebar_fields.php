<?php 
global $sidebars;
render_item('checkbox', '_sidebar_type', $sidebars, false, false, false, 'checkbox__sidebar_type'); 
?>

<div style="margin:10px 0;">
	<?php
		$sidebar_typ = get_post_meta($post->ID, "_sidebar_type", true);
		
		if( preg_match( "/\{" . $post->ID . "\}/", get_option("option_sidebars") ) ){
			$current_value = "on";
		}
	?>
	<div class="sidebar_active" style="<?php if($current_value != "on" && $sidebar_typ != "right" && $sidebar_typ != "left" ) echo "display:none;"; ?>">
	
		<p>Do you want to create a custom sidebar for this page ?</p>
		<?php 
			render_item('on_off_ui', 'custom_sidebar', $current_value);
		?>
		
		<p class="sidebar_info">
			<em>
				<strong>Note:</strong> When you set it on and save the post, Sidebar will register automatically. after Just take a look <a href="widgets.php">Widgets Manager</a>
			</em>
		</p>
		
	</div>

</div>