<div style="display:table; <?php if(@$_REQUEST["action"] == "edit") { echo "clear: both; margin-top:20px; margin-left: 230px; width: 50%;"; } ?>">


	<div class="option_holder">
		
		<div class="option_label"><strong>Blog Models</strong></div>
		<?php 
			$optionName = "blog_type";
			render_item('checkbox', 'blog_type', 'style1,style2', htmlSafe( get_the_value($optionName, $t_value ) ), false, false, 'blog_type'); 
		?>
		
	</div>
	
		
	<div class="option_holder">

		<div class="option_label"><strong>Would you like "Post Info (by admin, date, in category, comments, etc)" ?</strong></div>
		<?php 
			$optionName = "b_info";
			render_item('on_off_ui', $optionName, 'on', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>

	<div class="option_holder">

		<div class="option_label"><strong>Portfolio Item Limit (per page)</strong></div>
		<?php 
			$optionName = "b_limit";
			render_item('slider_ui', $optionName, '10,100,1,page item', htmlSafe( get_the_value($optionName, $t_value ) ) ); 
		?>
		
	</div>
	
</div>