<?php

/*

	Framework Name: CP (Codestar Panel)

	Plugin URI: http://codestarlive.com/

	Description: CP Framework is qucikly fron-end generator for our wordpress themes!

	Version: 1.0

	Author: Codestar

	Author URI: http://codestarlive.com/

	License: General Public Licence

	This is file has been created by CP Framework v1.0!
*/

define('F_PATH', 'fringe_tech_framework');

define('T_NAME', 'fringe_tech');

define('T_VER', 'v1.4');

$sliders = array("Bonus Slider","Nivo Slider","3D Slider","Kwicks Slider");

$register_types = array("blog_mod","portfolio_mod");

$sidebars = "full,right,left";

$home_models = "nothing,recently_and_boxes,boxes,custom";

include_once( TEMPLATEPATH ."/". F_PATH ."/libs/options.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/form_submit.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/global.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/functions.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/filters.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/actions.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_requests.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_columns.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_post.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_metabox.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_sidebars.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_widgets.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/register_shortcodes.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/slider_manager.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/home_design.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/global/taxonomy_meta.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_fonts.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_sliders.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_portfolios.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_blogs.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_home_design.php");
include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_home_intro.php");
if( !function_exists('wp_pagenavi') ){	include_once( TEMPLATEPATH ."/". F_PATH ."/libs/plugins/get_pagenavi.php"); }



add_action('admin_menu', 'framework_create_menu');
function framework_create_menu() {
          add_menu_page('Fringe Tech', 'Fringe Tech', 'administrator', 'fringe_tech', 'fringe_tech');
          add_submenu_page('fringe_tech', 'Header Settings', 'Header Settings', 'administrator', 'fringe_tech', 'fringe_tech');
          add_submenu_page('fringe_tech', 'General Settings', 'General Settings', 'administrator', 'general_settings', 'general_settings');
          add_submenu_page('fringe_tech', 'Home Intro Settings', 'Home Intro Settings', 'administrator', 'home_intro_settings', 'home_intro_settings');
          add_submenu_page('fringe_tech', 'Twitter Bar Settings', 'Twitter Bar Settings', 'administrator', 'twitter_bar_settings', 'twitter_bar_settings');
          add_submenu_page('fringe_tech', 'Footer Settings', 'Footer Settings', 'administrator', 'footer_settings', 'footer_settings');
          add_submenu_page('fringe_tech', 'Skin Settings', 'Skin Settings', 'administrator', 'skin_settings', 'skin_settings');
          add_submenu_page('fringe_tech', 'Contact Settings', 'Contact Settings', 'administrator', 'contact_settings', 'contact_settings');
          add_submenu_page('fringe_tech', 'Typography Settings', 'Typography Settings', 'administrator', 'typography_settings', 'typography_settings');
          add_submenu_page('fringe_tech', 'Dashboard Settings', 'Dashboard Settings', 'administrator', 'dashboard_settings', 'dashboard_settings');
          add_submenu_page('fringe_tech', 'Translate Settings', 'Translate Settings', 'administrator', 'translate_settings', 'translate_settings');
}


function fringe_tech(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Header Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','header-line','Header Dashed Line and Shadow Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Dashed Line','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','<div id="upload_header_line_preview" class="upload_pic header_line repeat-x" style="width:90%; height:1px; margin-bottom:10px;"></div>','', false,'','',''); ?>
</td>
</tr>
<tr>
<td>
<?php render_item('upload','header_line','[path]/images/header_dashed.png', false,'','css',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can upload your own dashed line effect from here.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Dashed Line','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','header_line_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Uh, also you can disable - enable header dashed line...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Top Shadow Effect','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','header_shadow_effect','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to top dark shadow effect ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','header-quicktag','Header Quicktag Icons Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Quicktags','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','quicktags_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to header quicktag icons (Twitter, Facebook, Dribbble, etc....) ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Quicktags','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','header_quicktags','<li class="header_icon">
	<a href="http://facebook.com/" target="_blank">
		<img src="[path]/images/icons/facebook.png" alt=""/>
	</a>
	<span class="tooltip">Follow me!</span>
</li>

<li class="header_icon">
	<a href="http://twitter.com/Codestarlive" target="_blank">
		<img src="[path]/images/icons/twitter.png" alt=""/>
	</a>
	<span class="tooltip">Follow me!</span>
</li>

<li class="header_icon">
	<a href="http://dribbble.com/Codestar" target="_blank">
		<img src="[path]/images/icons/dribble.png" class="dribbble" alt=""/>
	</a>
	<span class="tooltip">Follow me on Dribbble!</span>
</li>', false,'style="height:188px;"','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change your quicktag icons from here.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','header-search','Header Search Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Search','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','header_search','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to header settings ? you can disable - enable it ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Search','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','header_search_value','Enter your search keywords...', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Header search default text, you can change it.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','logo-settings','Logo and Favicon Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Site Logo','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','<div id="upload_site_logo_preview" class="upload_pic site_logo" style="width:256px; height:104px; margin-bottom:10px;"></div>','', false,'','',''); ?>
</td>
</tr>
<tr>
<td>
<?php render_item('upload','site_logo','[path]/images/logo.png', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Upload your own logo from here, it is easyly. but do not forget, you must upload a small logo, example 200x100 or 250x150...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Site Favicon','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','<div id="upload_favicon_preview" class="upload_pic favicon" style="width:20px; height:20px; margin-bottom:5px;"></div><div style="clear:both;  margin-bottom: 5px;">Also you can create your own favicon via <a href="http://www.favicon.cc/" targeT="blank">http://www.favicon.cc/</a></div>','', false,'','',''); ?>
</td>
</tr>
<tr>
<td>
<?php render_item('upload','favicon','[path]/images/favicon.ico', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Upload your own favicon! must be .ico format!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','menu-settings','Header Main Menu Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Header Model','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('checkbox','header_type','default,right,left,center', false,'','none','header_type'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Choose your header model, there is left menu, right menu, center menu, etc... also i will add more models in the future..','', false,'style="width:600px;"','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','menu-settings-more','Now, Here is <strong>Main Menu Settings</strong>, menu Link color, menu font size, menu padding etc....', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','header_link_color','#555555', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Header main menu color. (link color)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Link Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','header_link_hover_color','#2ba09e', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Header menu link color (link hover color)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Line Effect','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','header_over_line','#2ba09e', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Header menu line effect color.<br /><img src="[path]/images/helps/menu_line_effect.jpg" />','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Font','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('select','main_menu_font_family','Georgia,Arial,Verdana,Tahoma=selected,Trebuchet MS,Times New Roman,sans-serif,Allan:bold,Allerta,Allerta+Stencil,Amaranth,Anton,Anonymous+Pro,Anonymous+Pro:regular|italic|bold|bolditalic,Architects+Daughter,Arimo,Arimo:regular|italic|bold|bolditalic,Arvo,Arvo:regular|italic|bold|bolditalic,Astloch,Astloch:regular|bold,Bentham,Bevan,Buda:light,Cabin,Cabin:regular|500|600|bold,Cabin+Sketch:bold,Calligraffitti,Candal,Cantarell,Cantarell:regular|italic|bold|bolditalic,Cardo,Cherry+Cream+Soda,Chewy,Coda:800,Coming+Soon,Copse,Corben:bold,Cousine,Cousine:regular|italic|bold|bolditalic,Crafty+Girls,Crimson+Text,Crushed,Covered+By+Your+Grace,Cuprum,Dancing+Script,Droid+Sans,Droid+Sans:regular|bold,Droid+Sans+Mono,Droid+Serif,Droid Serif:regular|italic|bold|bolditalic,EB+Garamond,Expletus+Sans,Expletus+Sans:regular|500|600|bold,Fontdiner+Swanky,Geo,Goudy+Bookletter+1911,Gruppo,Homemade+Apple,Inconsolata,Indie+Flower,IM+Fell+DW+Pica,IM+Fell+DW+Pica:regular|italic,IM+Fell+DW+Pica SC,IM+Fell+Double+Pica,IM+Fell+Double+Pica:regular|italic,IM+Fell+Double+Pica SC,IM+Fell+English,IM+Fell+English:regular|italic,IM+Fell+English+SC,IM+Fell+French+Canon,IM+Fell+French+Canon:regular|italic,IM+Fell+French+Canon SC,IM+Fell+Great+Primer,IM+Fell+Great+Primer:regular|italic,IM+Fell+Great+Primer+SC,Irish+Grover,Irish+Growler,Josefin+Sans:100,Josefin+Sans:100|100italic,Josefin+Sans:light,Josefin+Sans:light|lightitalic,Josefin+Sans,Josefin+Sans:regular|regularitalic,Josefin+Sans:600,Josefin+Sans:600|600italic,Josefin+Sans:bold,Josefin+Sans:bold|bolditalic,Josefin+Slab:100,Josefin+Slab:100|100italic,Josefin+Slab:light,Josefin+Slab:light|lightitalic,Josefin+Slab,Josefin+Slab:regular|regularitalic,Josefin+Slab:600,Josefin+Slab:600|600italic,Josefin+Slab:bold,Josefin+Slab:bold|bolditalic,Just+Another+Hand,Just+Me+Again+Down+Here,Kenia,Kranky,Kreon,Kreon:light|regular|bold,Kristi,Lato:100,Lato:100|100italic,Lato:light,Lato:100|100italic,Lato:regular,Lato:regular|regularitalic,Lato:bold,Lato:bold|bolditalic,Lato:900,Lato:900|900italic,League+Script,Lekton,Lekton:regular|italic|bold,Lobster,Luckiest+Guy,Maiden+Orange,Meddon,MedievalSharp,Merriweather,Molengo,Mountains+of+Christmas,Neucha,Neuton,Nobile,Nobile:regular|italic|bold|bolditalic,Nova+Slim,Nova+Script,Nova+Round,Nova+Oval,Nova+Mono,Nova+Flat,Nova+Cut,OFL+Sorts+Mill+Goudy+TT,OFL Sorts Mill Goudy TT:regular|italic,Old+Standard+TT,Old Standard TT:regular|italic|bold,Orbitron,Orbitron:500,Orbitron:bold,Orbitron:900,Oswald,Pacifico,Permanent+Marker,Philosopher,PT+Sans,PT+Sans:regular|italic|bold|bolditalic,PT+Sans+Caption,PT+Sans+Caption:Bold,PT+Sans+Narrow,PT+Sans+Narrow:Bold,PT+Serif,PT+Serif:regular|italic|bold|bolditalic,PT+Serif+Caption,PT+Serif+Caption:regular|italic,Puritan,Puritan:regular|italic|bold|bolditalic,Quattrocento,Radley,Raleway:100,Reenie+Beanie,Rock+Salt,Schoolbell,Six+Caps,Slackey,Sniglet:800,Sunshiney,Syncopate,Tangerine,Tinos,Tinos:regular|italic|bold|bolditalic,Ubuntu,Ubuntu:regular|italic|bold|bolditalic,UnifrakturCook:bold,UnifrakturMaguntia,Unkempt,Vibur,Vollkorn,Vollkorn:regular|italic|bold|bolditalic,VT323,Walter+Turncoat,Yanone+Kaffeesatz,Yanone+Kaffeesatz:300,Yanone+Kaffeesatz:400,Yanone+Kaffeesatz:700', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Huh, You can change menu font size :)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','main_menu_font_size','13,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change your main menu font-size here.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Font Italic','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','main_menu_font_style','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to main menu font style <em>italic</em> ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Menu Font Bold','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','main_menu_font_weight','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to main menu font style <strong>bold</strong> ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','drop-down-settings','Now, Here is <strong>Main Meun Drop-Down Settings</strong> You can change all drop-down skin here..', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Drop-Down Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','drop_down_color','#111111', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change dropdown background color here...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Link Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','drop_down_link_color','#888888', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change drop-down link color here..','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Link Hover Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','drop_down_link_hover_color','#666666', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change link hover color.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Link BG Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','drop_down_link_back_color','#ffffff', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change drop down link background hover color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Link Active Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','drop_down_link_active_color','#2ba09e', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change drop down link active color.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Drop-Down Font','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('select','drop_down_font_family','Georgia,Arial,Verdana,Tahoma=selected,Trebuchet MS,Times New Roman,sans-serif,Allan:bold,Allerta,Allerta+Stencil,Amaranth,Anton,Anonymous+Pro,Anonymous+Pro:regular|italic|bold|bolditalic,Architects+Daughter,Arimo,Arimo:regular|italic|bold|bolditalic,Arvo,Arvo:regular|italic|bold|bolditalic,Astloch,Astloch:regular|bold,Bentham,Bevan,Buda:light,Cabin,Cabin:regular|500|600|bold,Cabin+Sketch:bold,Calligraffitti,Candal,Cantarell,Cantarell:regular|italic|bold|bolditalic,Cardo,Cherry+Cream+Soda,Chewy,Coda:800,Coming+Soon,Copse,Corben:bold,Cousine,Cousine:regular|italic|bold|bolditalic,Crafty+Girls,Crimson+Text,Crushed,Covered+By+Your+Grace,Cuprum,Dancing+Script,Droid+Sans,Droid+Sans:regular|bold,Droid+Sans+Mono,Droid+Serif,Droid Serif:regular|italic|bold|bolditalic,EB+Garamond,Expletus+Sans,Expletus+Sans:regular|500|600|bold,Fontdiner+Swanky,Geo,Goudy+Bookletter+1911,Gruppo,Homemade+Apple,Inconsolata,Indie+Flower,IM+Fell+DW+Pica,IM+Fell+DW+Pica:regular|italic,IM+Fell+DW+Pica SC,IM+Fell+Double+Pica,IM+Fell+Double+Pica:regular|italic,IM+Fell+Double+Pica SC,IM+Fell+English,IM+Fell+English:regular|italic,IM+Fell+English+SC,IM+Fell+French+Canon,IM+Fell+French+Canon:regular|italic,IM+Fell+French+Canon SC,IM+Fell+Great+Primer,IM+Fell+Great+Primer:regular|italic,IM+Fell+Great+Primer+SC,Irish+Grover,Irish+Growler,Josefin+Sans:100,Josefin+Sans:100|100italic,Josefin+Sans:light,Josefin+Sans:light|lightitalic,Josefin+Sans,Josefin+Sans:regular|regularitalic,Josefin+Sans:600,Josefin+Sans:600|600italic,Josefin+Sans:bold,Josefin+Sans:bold|bolditalic,Josefin+Slab:100,Josefin+Slab:100|100italic,Josefin+Slab:light,Josefin+Slab:light|lightitalic,Josefin+Slab,Josefin+Slab:regular|regularitalic,Josefin+Slab:600,Josefin+Slab:600|600italic,Josefin+Slab:bold,Josefin+Slab:bold|bolditalic,Just+Another+Hand,Just+Me+Again+Down+Here,Kenia,Kranky,Kreon,Kreon:light|regular|bold,Kristi,Lato:100,Lato:100|100italic,Lato:light,Lato:100|100italic,Lato:regular,Lato:regular|regularitalic,Lato:bold,Lato:bold|bolditalic,Lato:900,Lato:900|900italic,League+Script,Lekton,Lekton:regular|italic|bold,Lobster,Luckiest+Guy,Maiden+Orange,Meddon,MedievalSharp,Merriweather,Molengo,Mountains+of+Christmas,Neucha,Neuton,Nobile,Nobile:regular|italic|bold|bolditalic,Nova+Slim,Nova+Script,Nova+Round,Nova+Oval,Nova+Mono,Nova+Flat,Nova+Cut,OFL+Sorts+Mill+Goudy+TT,OFL Sorts Mill Goudy TT:regular|italic,Old+Standard+TT,Old Standard TT:regular|italic|bold,Orbitron,Orbitron:500,Orbitron:bold,Orbitron:900,Oswald,Pacifico,Permanent+Marker,Philosopher,PT+Sans,PT+Sans:regular|italic|bold|bolditalic,PT+Sans+Caption,PT+Sans+Caption:Bold,PT+Sans+Narrow,PT+Sans+Narrow:Bold,PT+Serif,PT+Serif:regular|italic|bold|bolditalic,PT+Serif+Caption,PT+Serif+Caption:regular|italic,Puritan,Puritan:regular|italic|bold|bolditalic,Quattrocento,Radley,Raleway:100,Reenie+Beanie,Rock+Salt,Schoolbell,Six+Caps,Slackey,Sniglet:800,Sunshiney,Syncopate,Tangerine,Tinos,Tinos:regular|italic|bold|bolditalic,Ubuntu,Ubuntu:regular|italic|bold|bolditalic,UnifrakturCook:bold,UnifrakturMaguntia,Unkempt,Vibur,Vollkorn,Vollkorn:regular|italic|bold|bolditalic,VT323,Walter+Turncoat,Yanone+Kaffeesatz,Yanone+Kaffeesatz:300,Yanone+Kaffeesatz:400,Yanone+Kaffeesatz:700', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change your drop down font style. default is tahoma :)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','drop_down_font_size','11,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change drop down font size here.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Drop-Down Radius','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','drop_down_radius','3,10,1,px', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Here, you can change drop down corner radius value.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Drop-Down Width','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','drop_down_width','175,500,1,px', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can resize drop-down width here...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Menu Font Italic','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','drop_down_font_style','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to drop down menu font <em>italic</italic> ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','D.D. Menu Font Bold','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','drop_down_font_weight','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to drop down font style <strong>bold</strong> ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','A href link titles','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','link_titles','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','<strong>Do you want to link titles ? Sometimes users do not like it. You can disable / enable it here!</strong><br /><img src="[path]/images/helps/link_title.jpg" />','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Home Link Name','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','home_link_name','Home', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can translate home link name here! :) also if you do not want to home link, remove all words. not write any word...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function general_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; General Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','home-slider','Home Page Slider Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','home-slider-set','You can create slider from Slider Manager, later you can choose your slider for home page.. <a href="admin.php?page=sliders">Please click here for to create new sliders</a>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Home Page Slider','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','home_page_slider_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to a slider on home page? if you do not want to a slider on home page, disable it ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Home Page Slider','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('select','home_page_slider','[sliders],[all],[desc],[id]', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Choose your home page slider.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','comment-settings','Comment Settings', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Page Comments','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','page_comment','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can turn on/off comment for standard pages!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Post Comments','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','post_comment','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can turn on/off comment for post detail page.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','prettyphoto','jQuery PrettyPhoto Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','jQuery prettyPhoto Social Tools','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','prettyphoto_social','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to Social Network Tools on light box ? (Twitter & Facebook)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','jQuery prettyPhoto','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','prettyphoto_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','do you want to prettyPhoto script for lightbox effect ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','jQuery prettyPhoto AutoPlay','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','prettyphoto_autoplay','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','do you want to prettyPhoto autoplay true?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','jQuery prettyPhoto Speed','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','prettyphoto_speed','10,100,1,second', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','jQuery prettyPhoto slideshow speed','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','breadcrumbs','Breadcrumb Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','breadcrumb-ex','You know what is breadcrum. You can enable/disable it. That is showing so below Post Title<br />Home » Page Level 1 » Page Level 2 » Page Level 3 ', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Breadcrumb','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','breadcrumb','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to enable breadcrumb below titles ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','google-analis','Google Analytics', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Google Analytics Code','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','google_analytics','', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','write your google analytics code here!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','site-meta','Meta Keywords & Description', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Meta Keywords','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','meta_keywords','website, blog, wordpress, seo, etc', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Meta Keywords, write some keywords for seo!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Meta Description','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','meta_description','Just another WordPress site', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Write a description for your web site. it will be great for seo!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','single-page','Single Page Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','single-page-sidebar-text','Take a look <a href="widgets.php">Widgets Manager</a> for single page sidebar. There is a widget ( as Single Page Sidebar )', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Single Page Sidebar Position','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('checkbox','single_page_sidebar_position','full=selected,left,right', false,'','none','sidebar_position'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Sidebar Position. You can move it left or right','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function home_intro_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Home Intro Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','intro-settings','Home Page Intro Text Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Home Intro','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','home_page_intro_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to intro text on home page?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Home Intro Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tinymce','home_page_intro','<h4 style="text-align: center;">WordPress is web software you can use to create a beautiful website or blog. We like to say that WordPress is both free and priceless at the same time.</h4>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can modify your intro text from here!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function twitter_bar_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Twitter Bar Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','twitter-set','Twitter Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Twitter','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','twitter_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to twitter on home page footer ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Twitter ID','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','twitter_id','Envato', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Write your twitter ID/Username','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Loading Message','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','twitter_loading','just a sec, loading tweets...', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Twitter module loading message.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Tweet Limit','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','twitter_items','5,100,1,tweet', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','How many items/tweet to display ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Delay Time','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','twitter_delay','5,25,1,second', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Tweet slide effect delay time, pause time.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','quickly-support-settings','Quick Support Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Quickly Support','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','quickly_support_active','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to quickly support on footer ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Title','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','quickly_support_text','Quickly Support ?', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change "Quickly Support ?" Link title text here...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Inside Title','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','quickly_support_inside_title','Do you need quickly support ?', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change inside title "Do you need quickly support ?" Link title text here...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Inside Content Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tinymce','quickly_support_inside_text','Some text here...', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change quickly support inside content text...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function footer_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Footer Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','footer-widgets','Footer Widgets Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','footer-widgets-set','Choose your footer widget model, after go to <a href="widgets.php">Widgets Manager</a> for modifications. and default is <strong>"1st (1,2,3,4)"</strong> model', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Footer Widget Model','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('checkbox','footer_type','model1,model2,model3,model4,model5,model6,model7,model8,model9,model10', false,'','none','footer_type'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Choose your favorite footer widget model. You have 10 widget model!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Footer Widgets','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','footer_widgets','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to use footer widget models ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','footer-copyright','Footer Copyright Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Copyright Left Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','footer_copyright_left','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to use footer copyright left ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Copyright Left Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','footer_info_left','Copyright &copy; 2010 - 2011 Codestar Inc<br />Premium a WordPress Theme for Your Website - All Rights Reserved', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change your copyright message from here.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Copyright Right Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','footer_copyright_right','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want use footer copyright right ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Copyright Right Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','footer_info_right','Take a look at me via <a href="http://facebook.com/" target="_blank">Facebook</a>, <a href="http://twitter.com/Codestarlive" target="_blank">Twitter</a> and <a href="http://dribbble.com/Codestar" target="_blank">Dribbble</a>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change copyright right text.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','footer-stick','Make Footer Stick', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','stickly-footer','You can use the Sticky Footer there! It is easyly, just turn on it!', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Sticky Footer','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','sticky_footer','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Turn on for sticky footer ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function skin_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Skin Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','general-settings','General Skin Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','general-settings','<strong>General Color Settings</strong>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','General Font Family','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('select','general_font_family','Georgia,Arial,Verdana,Tahoma=selected,Trebuchet MS,Times New Roman,sans-serif,Allan,Anonymous Pro,Anonymous Pro:regular|italic|bold|bolditalic,Allerta Stencil,Allerta,Amaranth,Anton,Architects Daughter,Arimo,Arimo:regular|italic|bold|bolditalic,Arvo,Arvo:regular|italic|bold|bolditalic,Astloch,Astloch:regular|bold,Bentham,Bevan,Buda:light,Cabin,Cabin:regular|500|600|bold,Cabin Sketch:bold,Calligraffitti,Candal,Cantarell,Cantarell:regular|italic|bold|bolditalic,Cardo,Cherry Cream Soda,Chewy,Coda,Coming Soon,Copse,Corben:bold,Cousine,Cousine:regular|italic|bold|bolditalic,Covered By Your Grace,Crafty Girls,Crimson Text,Crushed,Cuprum,Dancing Script,Droid Sans,Droid Sans:regular|bold,Droid Sans Mono,Droid Serif,Droid Serif:regular|italic|bold|bolditalic,EB Garamond,Expletus Sans,Expletus Sans:regular|500|600|bold,Fontdiner Swanky,Geo,Goudy Bookletter 1911,Gruppo,Homemade Apple,Inconsolata,Indie Flower,IM Fell DW Pica,IM Fell DW Pica:regular|italic,IM Fell DW Pica SC,IM Fell Double Pica,IM Fell Double Pica:regular|italic,IM Fell Double Pica SC,IM Fell English,IM Fell English:regular|italic,IM Fell English SC,IM Fell French Canon,IM Fell French Canon:regular|italic,IM Fell French Canon SC,IM Fell Great Primer,IM Fell Great Primer:regular|italic,IM Fell Great Primer SC,Irish Grover,Irish Growler,Josefin Sans:100,Josefin Sans:100|100italic,Josefin Sans:light,Josefin Sans:light|lightitalic,Josefin Sans,Josefin Sans:regular|regularitalic,Josefin Sans:600,Josefin Sans:600|600italic,Josefin Sans:bold,Josefin Sans:bold|bolditalic,Josefin Slab:100,Josefin Slab:100|100italic,Josefin Slab:light,Josefin Slab:light|lightitalic,Josefin Slab,Josefin Slab:regular|regularitalic,Josefin Slab:600,Josefin Slab:600|600italic,Josefin Slab:bold,Josefin Slab:bold|bolditalic,Just Another Hand,Just Me Again Down Here,Kenia,Kranky,Kreon,Kreon:light|regular|bold,Kristi,Lato:100,Lato:100|100italic,Lato:light,Lato:light|lightitalic,Lato:regular,Lato:regular|regularitalic,Lato:bold,Lato:bold|bolditalic,Lato:900,Lato:900|900italic,League Script,Lekton,Lekton:regular|italic|bold,Lobster,Luckiest Guy,Maiden Orange,Meddon,MedievalSharp,Merriweather,Molengo,Mountains of Christmas,Nobile,Nobile:regular|italic|bold|bolditalic,Nova Cut,Nova Flat,Nova Mono,Nova Oval,Nova Round,Nova Script,Nova Slim,Neucha,Neuton,OFL Sorts Mill Goudy TT,OFL Sorts Mill Goudy TT:regular|italic,Old Standard TT,Old Standard TT:regular|italic|bold,Orbitron,Orbitron:500,Orbitron:bold,Orbitron:900,Oswald,Reenie Beanie,Pacifico,Permanent Marker,Philosopher,PT Sans,PT Sans:regular|italic|bold|bolditalic,PT Sans Caption,PT Sans Caption:regular|bold,PT Sans Narrow,PT Sans Narrow:regular|bold,PT Serif,PT Serif:regular|italic|bold|bolditalic,PT Serif Caption,PT Serif Caption:regular|italic,Puritan,Puritan:regular|italic|bold|bolditalic,Quattrocento,Radley,Raleway:100,Rock Salt,Schoolbell,Six Caps,Slackey,Sniglet:800,Sunshiney,Syncopate,Tangerine,Tinos,Tinos:regular|italic|bold|bolditalic,Ubuntu,Ubuntu:regular|italic|bold|bolditalic,Unkempt,UnifrakturCook:bold,UnifrakturMaguntia,Vibur,Vollkorn,Vollkorn:regular|italic|bold|bolditalic,VT323,Walter Turncoat,Yanone Kaffeesatz,Yanone Kaffeesatz:300,Yanone Kaffeesatz:400,Yanone Kaffeesatz:700', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','General Font family, also you can use google web fonts there :)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','General Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','general_font_size','12,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can hange general font size here. all content font size.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','General Font Line-Height','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','general_font_line_height','21,100,1,px', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','General Font Line-Height, it is important if you want to use bigger font sizes ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','General Font Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','general_font_color','#555555', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change general font color here. All of content text color here! default is #555555','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','General Line Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','global_lines','#e1e1e1', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','General Site Line Color, all of line color ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Site Link Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','link_color','#2ba09e', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can set to general site link color here..','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Site Link Hover Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','link_hover_color','#222222', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change general site link hover color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Button Colors','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('checkbox','button_colors','ocean=selected,blue,green,pink,gray,yellow,red,orange', false,'','none','button_colors'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change generel site button color! <strong>default is 1st.</strong> also i will add more color in my next upgrade ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','twitter-bar','Twitter and Quickly Support Background Color', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Twitter Font Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','twitter_font_color','#555555', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Twitter Bar Font Color! also there is quickly contact font color..','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Background Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','twitter_background','#f1f1f1', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Twitter and Q.Support background color, you can write transparent. if you don not want to background color...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','footer-set','Footer Settings', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Footer Lines Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','footer_info_border','#cfcfcf', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Footer Border color, you can change it ;)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Footer Background','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','<div id="upload_footer_bg_preview" class="upload_pic site_logo" style="width:256px; height:204px; margin-bottom:10px; background-position:0 10px; background-repeat:repeat-x;"></div>','', false,'','',''); ?>
</td>
</tr>
<tr>
<td>
<?php render_item('upload','footer_bg','[path]/images/footer_bg.png', false,'','none','footer_bg'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change footer background image.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Footer Text Color','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('colorpicker','footer_text_color','#555555', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','General footer text color, widgets, footer copyright text and all of texts.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','custom','Custom CSS', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Add Custom CSS*','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','custom_css','', false,'style="height:300px;"','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can add custom css code...','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function contact_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Contact Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Email Address','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','site_form_mail','', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','General contact forms email address is here. and <strong>default form email is wordpress admin email.</strong>','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Mail Header Line Break Type','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','email_set','LF', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Please write <strong>LF</strong> or <strong>CRLF</strong>','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','email-set','If mail messages are not received, try using a LF (&#92;n) only. Some poor quality Unix mail transfer agents replace LF (&#92;n) by CRLF (&#92;r&#92;n) automatically (which leads to doubling C (&#92;r) if CRLF (&#92;r&#92;n) is used).. also you can change field names from <a href="admin.php?page=translate_settings"><strong>Translate Settings</strong></a>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function typography_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Typography Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','typography','Headline Typography Management', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','h1-set','<strong>H1, H2, H3, H4, H5 Font Style Settings</strong>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Font Family','', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('select','h_font_family','Georgia=selected,Arial,Verdana,Tahoma,Trebuchet MS,Times New Roman,sans-serif,Allan:bold,Allerta,Allerta+Stencil,Amaranth,Anton,Anonymous+Pro,Anonymous+Pro:regular|italic|bold|bolditalic,Architects+Daughter,Arimo,Arimo:regular|italic|bold|bolditalic,Arvo,Arvo:regular|italic|bold|bolditalic,Astloch,Astloch:regular|bold,Bentham,Bevan,Buda:light,Cabin,Cabin:regular|500|600|bold,Cabin+Sketch:bold,Calligraffitti,Candal,Cantarell,Cantarell:regular|italic|bold|bolditalic,Cardo,Cherry+Cream+Soda,Chewy,Coda:800,Coming+Soon,Copse,Corben:bold,Cousine,Cousine:regular|italic|bold|bolditalic,Crafty+Girls,Crimson+Text,Crushed,Covered+By+Your+Grace,Cuprum,Dancing+Script,Droid+Sans,Droid+Sans:regular|bold,Droid+Sans+Mono,Droid+Serif,Droid Serif:regular|italic|bold|bolditalic,EB+Garamond,Expletus+Sans,Expletus+Sans:regular|500|600|bold,Fontdiner+Swanky,Geo,Goudy+Bookletter+1911,Gruppo,Homemade+Apple,Inconsolata,Indie+Flower,IM+Fell+DW+Pica,IM+Fell+DW+Pica:regular|italic,IM+Fell+DW+Pica SC,IM+Fell+Double+Pica,IM+Fell+Double+Pica:regular|italic,IM+Fell+Double+Pica SC,IM+Fell+English,IM+Fell+English:regular|italic,IM+Fell+English+SC,IM+Fell+French+Canon,IM+Fell+French+Canon:regular|italic,IM+Fell+French+Canon SC,IM+Fell+Great+Primer,IM+Fell+Great+Primer:regular|italic,IM+Fell+Great+Primer+SC,Irish+Grover,Irish+Growler,Josefin+Sans:100,Josefin+Sans:100|100italic,Josefin+Sans:light,Josefin+Sans:light|lightitalic,Josefin+Sans,Josefin+Sans:regular|regularitalic,Josefin+Sans:600,Josefin+Sans:600|600italic,Josefin+Sans:bold,Josefin+Sans:bold|bolditalic,Josefin+Slab:100,Josefin+Slab:100|100italic,Josefin+Slab:light,Josefin+Slab:light|lightitalic,Josefin+Slab,Josefin+Slab:regular|regularitalic,Josefin+Slab:600,Josefin+Slab:600|600italic,Josefin+Slab:bold,Josefin+Slab:bold|bolditalic,Just+Another+Hand,Just+Me+Again+Down+Here,Kenia,Kranky,Kreon,Kreon:light|regular|bold,Kristi,Lato:100,Lato:100|100italic,Lato:light,Lato:100|100italic,Lato:regular,Lato:regular|regularitalic,Lato:bold,Lato:bold|bolditalic,Lato:900,Lato:900|900italic,League+Script,Lekton,Lekton:regular|italic|bold,Lobster,Luckiest+Guy,Maiden+Orange,Meddon,MedievalSharp,Merriweather,Molengo,Mountains+of+Christmas,Neucha,Neuton,Nobile,Nobile:regular|italic|bold|bolditalic,Nova+Slim,Nova+Script,Nova+Round,Nova+Oval,Nova+Mono,Nova+Flat,Nova+Cut,OFL+Sorts+Mill+Goudy+TT,OFL Sorts Mill Goudy TT:regular|italic,Old+Standard+TT,Old Standard TT:regular|italic|bold,Orbitron,Orbitron:500,Orbitron:bold,Orbitron:900,Oswald,Pacifico,Permanent+Marker,Philosopher,PT+Sans,PT+Sans:regular|italic|bold|bolditalic,PT+Sans+Caption,PT+Sans+Caption:Bold,PT+Sans+Narrow,PT+Sans+Narrow:Bold,PT+Serif,PT+Serif:regular|italic|bold|bolditalic,PT+Serif+Caption,PT+Serif+Caption:regular|italic,Puritan,Puritan:regular|italic|bold|bolditalic,Quattrocento,Radley,Raleway:100,Reenie+Beanie,Rock+Salt,Schoolbell,Six+Caps,Slackey,Sniglet:800,Sunshiney,Syncopate,Tangerine,Tinos,Tinos:regular|italic|bold|bolditalic,Ubuntu,Ubuntu:regular|italic|bold|bolditalic,UnifrakturCook:bold,UnifrakturMaguntia,Unkempt,Vibur,Vollkorn,Vollkorn:regular|italic|bold|bolditalic,VT323,Walter+Turncoat,Yanone+Kaffeesatz,Yanone+Kaffeesatz:300,Yanone+Kaffeesatz:400,Yanone+Kaffeesatz:700', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','help me','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Font Italic Style ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','h_style','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to italic style for this font ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Font Bold Style ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','h_weight','off', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to bold style for this font ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','h-font-sizes','<strong>H1, H2, H3, H4, H5 Font SizeSettings</strong>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H1 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h1_font_size','36,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H1 Font size.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H2 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h2_font_size','28,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H2 Font Size<br /><br /><strong>Using elements:</strong><br/>Search Page Title<br/>Archives Page Title<br/>All  Of Page Titles','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H3 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h3_font_size','22,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H3 Font Size<br /><br /><strong>Using elements:</strong><br/>Comment Form Title','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H4 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h4_font_size','18,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H4 Font Size<br /><br /><strong>Using elements:</strong><br/>Intro Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H5 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h5_font_size','16,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H5 Font Size<br /><br /><strong>Using elements:</strong><br/>Recently Added<br />Home Boxes Title<br />Quickly Support Titles','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','H6 Font Size','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('slider_ui','h6_font_size','14,100,1,px,font', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','H6 Font Size<br /><br /><strong>Using elements:</strong><br/>Twitter Tweets<br/>Send Message<br />Post Comment','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function dashboard_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Dashboard Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Codestar News','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','codestar_news','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Do you want to Codestar News & Features on admin dashboard ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Quick Press','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_quick_press','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "Dashboard Quick Press" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Recent Drafts','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_recent_drafts','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "Dashboard Recent Drafts" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','WordPress Blog','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_primary','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "WordPress Blog" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','WordPress News','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_secondary','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "Dashboard WordPress News" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Incoming Links','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_incoming_links','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "Dashboard Incoming Links" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Recent Comments','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_recent_comments','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would youı like "Dashboard Recent Comments" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','WordPress Plugins','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('on_off_ui','dashboard_plugins','on', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Would you like "Dashboard Plugins" ?','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Admin Copyright Text','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('textarea','admin_copyright','Custom text | Copyright &copy; message', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can change wordpress admin copyright text here :)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Admin Logo','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','<div id="upload_admin_logo_preview" class="upload_pic site_logo" style="width:256px; height:104px; margin-bottom:10px;"></div>','', false,'','',''); ?>
</td>
</tr>
<tr>
<td>
<?php render_item('upload','admin_logo','[path]/images/dummy/admin_logo.png', false,'','none','admin_logo'); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','You can upload your own logo on wordpress admin panel logo :)','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

function translate_settings(){
?>
<form method="post"><div class="framework_header">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
<div class="framework_info">Fringe Tech &raquo; Translate Settings</div>
</div>
<table cellspacing="0" class="option_tables">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('header','translate-settings','Translate Settigns', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','translate-words','Uh, i added all of words here, but you can use any plugin for translate. You know. if you do not want to use any plugin use here :) Enjoy...', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','quickly-support-sett','<strong>Quickly Support Form and Shortcode Contact Form and Comment Form Fields</strong>', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Name *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_name','Name *', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Name *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Email *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_email','Email *', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Email *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Telephone','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_telephone','Telephone', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Telephone','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Subject *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_subject','Subject *', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Subject *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Message *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_message','Message *', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Message *','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Web Site','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','field_url','Web Site', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Field Web Site','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','* required','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','text_required','* required', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','text * required','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Send Message','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','button_send','Send Message', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Send Message Button','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Post Comment','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','button_post_comment','Post Comment', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Post Comment','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('full_text','trans-text','You can change all of text via Plugin. or wait my next upgrade for more text field translate :)', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Form Error Name','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_name','Error, Please check <strong>your name</strong> :(', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Quickly Support and Comment form errors for field name.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Form Error Email','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_email','Error, Please check <strong>your email</strong> :(', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Quickly Support and Comment form errors for field email.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Form Error Subject','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_subject','Error, Please check <strong>your subject</strong> :(', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Quickly Support and Comment form errors for field subject.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Form Error Message','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_message','Error, Please check <strong>your message</strong> :(', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Quickly Support and Comment form errors for field message.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Form Error Comment','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_comment','Error, Please check <strong>your comment</strong> :(', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Quickly Support and Comment form errors for field comment.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Waiting Message','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_send_message','Please wait, sending...', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Sending message.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Thanks Message','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_thanks_message','Success! Thank you for contact.', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Form Thanks Message!','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Error Mail','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','_error_mail','Mail Failed, Please try again!', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Mail error message.','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="option_rows">
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="for_0">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('text','Mail Message Title','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_1">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('input','mail_inbox_title','You got an email from your website!', false,'','none',''); ?>
</td>
</tr>
</table>
</td>
<td class="for_2">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<?php render_item('tooltip','Mail Message Title','', false,'','',''); ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
<div class="framework_footer">
<div class="framework_action">
<input type="submit" name="action" value="save" class="form_submits save_options"><input type="submit" name="action" value="reset" class="form_submits reset_options" onclick="return confirm('Are you sure you want to reset ?');">
</div>
</div>
<div class="tooltip_content">
<div class="tooltip_arrow">&nbps;</div>
<div class="tooltip_text">&nbps;</div>
</div>
</form><?php
}

?>