<?php if ( get_option( "footer_widgets", "on" ) == "on" || get_option( "footer_copyright", "on" ) == "on" || get_option("twitter_active", "on") == "on" || get_option("quickly_support_active", "on") == "on" ) { ?>
<div class="sticky_footer"></div>
<!-- begin bottom footer -->
<div class="bottom_wrap">

	<?php if( get_option("twitter_active", "on") == "on" || get_option("quickly_support_active", "on") == "on" ){ ?>
	<!-- begin bottom up -->
	<div class="bottom_up_wrap">

		<div class="bottom_up_container">
		
			<?php if( get_option("twitter_active", "on") == "on" ){ ?>
			<div class="twitter_icon">
			
				<div class="twitter_ticker">
					<div class="tweetLoading"><h6><?php echo get_option("twitter_loading", "just a sec, loading tweets..."); ?></h6></div>
					<!-- including here -->
				</div>

			</div>
			<?php } ?>
			
			<?php
			if( get_option("quickly_support_active", "on") == "on" ){
			$quickly_support_text =  get_option("quickly_support_text", "Quickly Support ?"); 
			?>
			<div class="quickly_support">
				<h5>
					<img src="<?php echo T_URI; ?>/images/icons/quickly_support.png" alt="<?php echo $quickly_support_text; ?>" /><?php echo $quickly_support_text; ?>
				</h5>
			</div>
			<?php } ?>
			
		</div>
		
		<?php
		
			if ( get_option( "quickly_support", "on" ) == "on" ) { 
			
				$qform_rand = rand(10, 100);
		?>
		
		<div class="quickly_container form_quickly">
			
			<div class="quickly_content">

				<div class="quickly_content_left">
					
					<div class="quickly_title">
						<h5><?php echo get_option("quickly_support_inside_title", "Do you need quickly support ?"); ?></h5>
					</div>
					
					<div class="quickly_text">
						<p><?php echo add_lightbox_rel ( do_shortcode ( get_option("quickly_support_inside_text", "Some text here.") ) ); ?></p>
					</div>
					
				</div>

				<div class="quickly_content_right">
					
					<div class="quickly_elements_left">
					
						<div class="quickly_elements_label">
							<input type="text" name="name" class="input" value="<?php echo get_option("field_name", "Name *"); ?>">
						</div>
						<div class="quickly_elements_label">
							<input type="text" name="email" class="input" value="<?php echo get_option("field_email", "Email *"); ?>">
						</div>
						
						<div class="quickly_elements_label">
							<input type="text" name="telephone" class="input" value="<?php echo get_option("field_telephone", "Telephone"); ?>">
						</div>
						
						<div class="quickly_elements_label quickly_required">
							<?php echo get_option("text_required", "* required"); ?>
						</div>
						
					</div>
					
					<div class="quickly_elements_right">
						
						<div class="quickly_elements_label">
							<input type="text" name="subject" class="input" value="<?php echo get_option("field_subject", "Subject *"); ?>" style="width:280px;">
						</div>
						
						<div class="quickly_elements_label">
							<textarea class="textarea" name="message" style="width:280px; height:65px;"><?php echo get_option("field_message", "Message *"); ?></textarea>
						</div>
						
						<div class="quickly_elements_label">
							
							<div class="form_quickly quickly_submit alignright">
							
								<h6>
									<span class="small_buttons">
										<span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_l small_left"><span class="<?php echo get_clean_option("button_colors", "ocean"); ?>_r small_right"><?php echo get_option("button_send", "Send Message"); ?></span></span>
									</span>
								</h6>
								
							</div>

							<div class="quickly_form_message alignright">&nbsp;</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="cleardiv"></div>
				<div class="quickly_close_container">
					<div class="quickly_close">&nbsp;</div>
				</div>
			
			</div>
			
		</div>
		
		<?php } ?>

	</div>
	<!-- end bottom up -->
	<?php } ?>
	
	<?php if ( get_option( "footer_widgets", "on" ) == "on" || get_option("footer_copyright_left", "on") == "on" || get_option("footer_copyright_right", "on") == "on" ) { ?>
	
	<!-- begin footer -->
	<div class="footer">
		
		
		<!-- begin footer container -->
		<div class="footer_container">
			
			<?php 
			if ( get_option( "footer_widgets", "on" ) == "on" ) { 
				$footer_type = get_clean_option("footer_type", "model1");
				
				if($footer_type == ""){ $footer_type = "model1"; }
				
			?>
			<div class="footer_columns <?php echo $footer_type; ?>">
				
				<?php
				switch ($footer_type) {
				
					case "model1":
						$num = 4;
					break;
					
					case "model2":
					case "model4":
					case "model9":
					case "model10":
						$num = 3;
					break;
					
					case "model3":
					case "model5":
					case "model6":
						$num = 2;
					break;
					
					case "model7":
						$num = 1;
					break;
					
					case "model8":
						$num = 5;
					break;
					
				}
				?>
				
				<?php
					for ($v = 1; $v < ( $num + 1) ; $v++){
				?>
				<div class="footer_column <?php echo "column_".$v; if ($v == $num){ echo " last"; } ?>">
					<?php dynamic_sidebar("sidebar_footer_widget_".$v); ?>
				</div>
				<?php } ?>				
				
			</div>
			<?php } ?>
			
			
			<?php if( get_option("footer_copyright_left", "on") == "on" || get_option("footer_copyright_right", "on") == "on" ){ ?>
			<!-- begin footer informations -->
			<div class="footer_info">
				
				<?php if( get_option("footer_copyright_left", "on") == "on" ){ ?>
				<span class="alignleft">
					<?php echo get_option("footer_info_left", "Copyright (c) 2010 - 2011 Codestar Inc<br />Premium a WordPress Theme for Your Website - All Rights Reserved"); ?>
				</span>
				<?php } ?>
				
				<?php if( get_option("footer_copyright_right", "on") == "on" ){ ?>
				<span class="alignright">
					<?php echo get_option("footer_info_right", 'Take a look at me via <a href="http://facebook.com/" target="_blank">Facebook</a>, <a href="http://twitter.com/Codestarlive" target="_blank">Twitter</a> and <a href="http://dribbble.com/Codestar" target="_blank">Dribbble</a>'); ?>
				</span>
				<?php } ?>
				
			</div>
			<!-- end footer informations -->
			<?php } ?>
			
			
		</div>
		<!-- begin footer container -->

	</div>
	<!-- end footer -->
	<?php } ?>
	
</div>
<!-- end bottom footer -->
<?php } ?>
<div class="cleardiv"></div>
<?php wp_footer(); ?>
<script src="<?php echo T_JS; ?>/jquery.register.1.0.js"></script>
<?php google_fonts(); ?>
<?php echo get_option("google_analytics"); ?>
</body>
</html>