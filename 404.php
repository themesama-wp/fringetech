<?php
/*
	The template for displaying 404 pages (Not Found).
*/
get_header(); ?>

<div class="page_wrap">
	
	<div class="page_title">
		<h2 class="italic"><?php _e( '404 - Page Not Found', 'fringe_tech' ); ?></h2>
	</div>
	
	<div class="page_container alignleft full">
		<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'fringe_tech' ); ?></p>
		<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
			<input size="50" name="s" id="s" type="text" value="<?php echo get_option("header_search_value", "Enter your search keywords..."); ?>" />
			<input type="submit" value="search">
		</form>
		<?php echo do_shortcode('[divider_hr top="30" bottom="20"] [sitemap pages="true" posts="true" posts_limit="30" categories="true"]'); ?>
	</div>
</div>

<?php get_footer(); ?>