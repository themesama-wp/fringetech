<?php
/******************************************************************
	The Loop v1.0
	by Codestar
	WordPress Loop using Custom Post Type / Taxonomies
*******************************************************************/


$args['paged']				= get_query_var("paged");
$args['suppress_filters']	= true;

/* is Archives Loops */
if ( is_month() || is_day() ){

	$the_date = explode(",", get_the_time('Y,m,d'));
	$args["year"] 				= $the_date[0];
	$args["monthnum"] 			= $the_date[1];
	
	if ( is_day() ){
		$args["day"] 				= $the_date[2];
	}
	
}

/* is Author Loops */
if( is_author() ) {
	$args["author"]			= get_query_var("author");
}

/* is Tag Loops */
if ( is_tag() ){
	$args["tag"] 			= get_query_var("tag");
}

$get_taxonomy				= get_query_var("taxonomy");
$get_cat_name				= get_query_var("category_name");

/* All Post Types Select */
$args['post_type'] 			= array("blog_mod", "portfolio_mod", "post");
$args['category_name'] 		= $get_cat_name;
$args[$get_taxonomy]		= get_query_var("term");


if ( !is_search() ){
	
	$wp_query 				= new WP_Query($args);

}

?>
<?php if ( $wp_query->max_num_pages > 1 ) { ?>
	<div id="nav-above" class="navigation" style="float:left; width:100%; margin-bottom:20px;">
		
		<?php wp_pagenavi(); ?>
		
	</div>
	
<?php } ?>


<?php  if ( ! $wp_query->have_posts() ) { ?>
	<div>
		<h2 class="italic"><?php _e( 'Not Found', 'fringe_tech' ); ?></h2>
		<div class="entry-content">
		
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'fringe_tech' ); ?></p>
		
			<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
				<input size="50" name="s" id="s" type="text" value="<?php echo get_option("header_search_value", "Enter your search keywords..."); ?>" />
				<input type="submit" value="search">
			</form>
			

		</div>
	</div>
<?php } 
   
   while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

		<div style="padding-bottom:20px; border-bottom:1px solid <?php echo get_option("global_lines", "#e1e1e1"); ?>; margin-bottom:40px; float:left; width:100%;">
			
			<h2>	
				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'fringe_tech' ), the_title_attribute( 'echo=0' ) ); ?>">
					<?php the_title(); ?>
				</a>
			</h2>

			<?php if ( $post->post_type != "page" ) { ?>
			<div class="entry-meta" style="width:100%; float:left;">
				
				
				<?php 
					
					$p_type = $post->post_type;
					
					if ( $p_type == "post" ) {
						
						$term_type = "category";
						
					} else if ( $p_type == "portfolio_mod" ) {
					
						$term_type = "portfolio_type";

					} else if ( $p_type == "blog_mod" ) {
					
						$term_type = "blog_type";

					}
					
				?>
				
				<div class="blog_boxes">
				
					<div class="blog_info">
					
						<ul>
						
							<li><?php _e( 'Posted in', 'fringe_tech' ); ?> <?php the_terms($post->ID, $term_type, '', ', ', ''); ?></li>
							<?php if( get_the_tag_list('', ', ' ) != "" ) {?>
							<li><?php printf( "Tags: %s", get_the_tag_list('', ', ' ) ); ?></li>
							<?php } ?>
							<li><?php echo sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>', get_permalink(), esc_attr( get_the_time() ), get_the_date() ); ?></li>
							<li><?php _e( 'by', 'fringe_tech' ); ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author(); ?></a></li>
						
						</ul>
						
					</div>
				
				</div>
		
			</div>
			
			<?php } ?>

			<div class="blog_boxes">
				<?php
					$post_thumbnail		= get_post_meta($post->ID, "_post_thumbnail", true);
					
					$thumb_type	= "";
					if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $post_thumbnail) ){
						$thumb_type	= "pic";
					}
					
					$post_fullsize		= get_post_meta($post->ID, "_post_fullsize", true);
					$post_short_desc	= get_post_meta($post->ID, "_short_description", true);
					
					preg_match_all("/(.*?)[\&\?]type=(.*?)\&target=(.*?)$/i", $post_fullsize, $matches);
					
				?>
				
				<?php if ( $post_thumbnail != "" ) { ?>
					<div class="box_image_shadow">
					
						<div class="box_image_inside">
							
							<?php if( $thumb_type == "pic" ) { ?>
							
								<?php if ( $post_fullsize != "" && !$matches[2][0] == "link" ) { ?>
									<a href="<?php echo $post_fullsize; ?>" rel="prettyPhoto[<?php echo $b_id; ?>]">
								<?php } ?>
								
								<?php if ( $matches[2][0] == "link" ) { ?>
									
										<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>">
										
								<?php } ?>
								
								<img src="<?php echo $post_thumbnail; ?>" alt="<?php echo get_the_title(); ?>" style="max-width:950px"/>
							
								<?php if ( $post_fullsize != "") { ?></a><?php } ?>
							
							<?php } ?>
							
						</div>
						
					</div>
					<?php } ?>
				</div>	
			<div class="entry-content" style="clear:both; float:left; width:100%;">
				<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'fringe_tech' ) ); ?>
			</div><!-- .entry-content -->

			<div class="entry-content" style="clear: both; float: left; width: 100%; margin-top: 10px;">
		
				<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'fringe_tech' ), __( '1 Comment', 'fringe_tech' ), __( '% Comments', 'fringe_tech' ) ); ?></span>
				<?php edit_post_link( __( 'Edit', 'fringe_tech' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
				
			</div>
			
		</div>

<?php endwhile; // End the loop. Whew. ?>

<?php if (  $wp_query->max_num_pages > 1 ) : ?>
	<div id="nav-below" class="navigation">
		<?php wp_pagenavi(); ?>
	</div>
<?php endif; ?>