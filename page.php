<?php
/*
	General Page Template v1.0
*/

get_header();
?>

<div class="page_wrap">
	
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div class="page_title">
			<h2><?php the_title(); ?></h2>
			<?php if( get_option ("breadcrumb", "off") == "on" ){ echo get_breadcrumb(); } ?>
			<?php edit_post_link( __( 'Edit', 'fringe_tech' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

		<?php
			/*
				Getting Sidebar Type
			*/
			$sidebar_mod	= get_sidebar_type($post->ID);
			$sidebar_pos	= $sidebar_mod[2];
			$position		= $sidebar_mod[1];
			$class			= $sidebar_mod[0];
		?>
		
		<div class="page_container align<?php echo $position; ?> <?php echo $class; ?>">
			
			<?php the_content(); ?>
			
			<?php if ( get_option("page_comment", "off") == "on" ) { comments_template( '', true ); } ?>

		</div>

		<?php if($class != "" && $class != "full") { ?>
		<div class="page_sidebar align<?php echo $sidebar_pos; ?>">
			
			<?php get_sidebar(); ?>
		
		</div>
		<?php } ?>
		
	<?php endwhile; ?>

</div>

<?php get_footer(); ?>