<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div class="page_wrap">

<?php
	/* Queue the first post, that way we know
	 * what date we're dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		the_post();
?>

	<div class="page_title">
		<h2 class="italic">
				<?php if ( is_day() ) : ?>
					<?php printf( __( 'Daily Archives: <span>%s</span>', 'fringe_tech' ), get_the_date() ); ?>
				<?php elseif ( is_month() ) : ?>
					<?php printf( __( 'Monthly Archives: <span>%s</span>', 'fringe_tech' ), get_the_date( 'F Y' ) ); ?>
				<?php elseif ( is_month() ) : ?>
					<?php printf( __( 'Monthly Archives: <span>%s</span>', 'fringe_tech' ), get_the_date( 'F Y' ) ); ?>
				<?php elseif ( is_tag() ) : ?>
					<?php printf( __( 'Tag Archives: <span>%s</span>', 'fringe_tech' ), single_tag_title( '', false ) ); ?>
				<?php elseif ( is_author() ) : ?>
					<?php printf( __( 'Author Archives: <span>%s</span>', 'fringe_tech' ), "<span class='vcard'><a class='url fn n' href='" . get_author_posts_url( get_the_author_meta( 'ID' ) ) . "' title='" . esc_attr( get_the_author() ) . "' rel='me'>" . get_the_author() . "</a></span>" ); ?>
				<?php elseif ( is_category() ) : ?>
					<?php printf( __( 'Category Archives: <span>%s</span>', 'fringe_tech' ), single_cat_title( '', false ) ); ?>
				<?php elseif ( is_tax() ) : ?>
					<?php _e( single_term_title() . ' Archives', 'fringe_tech' ); ?>
				<?php else : ?>
					<?php _e( 'Archives', 'fringe_tech' ); ?>
				<?php endif; ?>
			</h2>
	</div>

			<div class="page_container alignleft full">
<?php
	/* Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/* Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archive.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'archive' );
?>
	</div>
	
	</div>
	
<?php get_footer(); ?>
