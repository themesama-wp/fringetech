<?php
/*
	The Template for displaying all single posts.
*/

get_header(); ?>


<div class="page_wrap">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<div class="page_title">
		<h2><?php the_title(); ?></h2>
		<?php if( get_option ("breadcrumb", "off") == "on" ){ echo get_breadcrumb(); } ?>
		<?php edit_post_link( __( 'Edit', 'fringe_tech' ), '<span class="edit-link">', '</span>' ); ?>
	</div>
	
	<?php
		$sidebar = get_clean_option("single_page_sidebar_position", "full");
		
		$sidebar_mod	= get_sidebar_single($sidebar);
		$sidebar_pos	= $sidebar_mod[2];
		$position		= $sidebar_mod[1];
		$class			= $sidebar_mod[0];
			
	
	?>

	<?php if($class != "" && $class != "full") { ?>
	<div class="page_sidebar align<?php echo $sidebar_pos; ?>">
		
		<?php dynamic_sidebar( "single_sidebar_all" ); ?>
		<?php get_sidebar(); ?>
	
	</div>
	<?php } ?>
   
	<div class="page_container align<?php echo $position; ?> <?php echo $class; ?>">
		

		<?php the_content(); ?>

		<?php if ( get_option("post_comment", "on") == "on" ) { comments_template( '', true ); } ?>

	</div>


	<div class="cleardiv"></div>
	
	<div id="nav-below" class="navigation">
		<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'fringe_tech' ) . '</span> %title' ); ?></div>
		<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'fringe_tech' ) . '</span>' ); ?></div>
	</div>
	
<?php endwhile; ?>

</div>
<?php get_footer(); ?>
