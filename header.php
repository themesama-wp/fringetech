<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$s_desc = get_bloginfo( 'description', 'display' );
	if ( $s_desc && ( is_home() || is_front_page() ) ) echo " | " . $s_desc;
?></title>

<meta name="keywords" content="<?php echo get_option("meta_keywords", "website, blog, wordpress, seo, etc"); ?>" />
<meta name="description" content="<?php echo get_option("meta_description", $s_desc); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if( get_option("favicon", T_URI."/images/favicon.ico") != "" ) { ?>
<link href="<?php echo get_option("favicon", T_URI."/images/favicon.ico"); ?>" rel="icon" type="image/x-icon" />
<?php } ?>

<!-- including stylesheets -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo home_url( '/' ); ?>?dyn=css" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" media="all" href="<?php echo T_CSS; ?>/ie8.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="all" href="<?php echo T_CSS; ?>/ie7.css" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="all" href="<?php echo T_CSS; ?>/ie6.css" /><![endif]-->

<?php
if ( is_singular() && get_option( 'thread_comments' ) )
	wp_enqueue_script( 'comment-reply' );
wp_head(); 
?>

<!-- including javascripts -->
<script src="<?php echo home_url( '/' ); ?>?dyn=js"></script>
<script src="<?php echo T_JS; ?>/jquery.easing.1.3.js"></script>
<script src="<?php echo T_JS; ?>/jquery.codestar.1.0.js"></script>
<script src="<?php echo T_JS; ?>/jquery.nivo.slider.js"></script>
<?php if( get_option("prettyphoto_active", "on") == "on" ){ ?>
<script src="<?php echo T_JS; ?>/jquery.prettyPhoto.js"></script>
<?php } ?>
<?php if( get_option("twitter_active", "on") == "on" ){ ?>
<script src="<?php echo T_JS; ?>/jquery.tweet.js"></script>
<?php } ?>

<script src="<?php echo T_JS; ?>/jquery.kwicks-1.5.1.js"></script>
<script src="<?php echo T_JS; ?>/jquery.flashgateway.js"></script>
<script src="<?php echo T_JS; ?>/jquery.quicksand.js"></script>

</head>
<body <?php body_class(); ?>>

<?php if( get_option("header_line_active", "on") != "off" ) { ?>
<div class="header_line"></div>
<?php } ?>

<div class="header_effect"></div>

<!-- begin header -->
<div class="header_wrap">

	<!-- begin quicktags -->
	<div class="header_quicktags">
		
		<ul>
		
			<!-- begin header icons -->
			<?php 
				ob_start();
			?>
			<li class="header_icon"><a href="http://facebook.com/" target="_blank"><img src="<?php echo T_URI; ?>/images/icons/facebook.png" alt=""/></a><span class="tooltip">Follow me!</span></li>
			<li class="header_icon"><a href="http://twitter.com/" target="_blank"><img src="<?php echo T_URI; ?>/images/icons/twitter.png" alt=""/></a><span class="tooltip">Follow me!</span></li>
			<li class="header_icon"><a href="http://dribbble.com/" target="_blank"><img src="<?php echo T_URI; ?>/images/icons/dribble.png" class="dribbble" alt=""/></a><span class="tooltip">Follow me on Dribbble</span></li>
			<?php
				$str = ob_get_clean();
				$header_quicktags	= get_option("header_quicktags", $str);
				$quicktags_active	= get_option("quicktags_active", "on");;
				if($quicktags_active != "off") echo $header_quicktags;
			?>
			<!-- end header icons -->
			
			<?php
				if (get_option("header_search", "on") == "on"){
			?>
			<li class="header_icon_seach">
		
				<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
					<input class="header_search_input valcheck" name="s" id="s" type="text" value="<?php echo get_option("header_search_value", "Enter your search keywords..."); ?>" />
					<input type="submit" class="header_search_button" value="search">
				</form>
				
			</li>
			<?php } ?>
			
		</ul>
		
		<div class="tooltip_content">
			<div class="tooltip_arrow"></div>
			<div class="tooltip_text"></div>
		</div>
		
	</div>
	<!-- end quicktags -->

	<!-- begin header container -->
	<div class="header_container clearfix">

		<div class="header_logo">
			<a href="<?php echo home_url( '/' ); ?>"><img src="<?php echo get_option("site_logo", T_URI."/images/logo.png"); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>"/></a>
		</div>

		<!-- begin menu -->
		<?php if (function_exists('wp_nav_menu')){ wp_nav_menu( array( 'theme_location' => 'primary' ) ); } ?>
		<!-- begin menu -->

	</div>
	<!-- end header container -->

	<div class="header_menu_line"><div class="header_menu_cline"></div></div>
	
</div>
<!-- end header -->