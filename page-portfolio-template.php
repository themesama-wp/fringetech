<?php
/*
	Template Name: Portfolio Page
*/

	get_header();
?>

<div class="page_wrap">
	
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div class="page_title">
		<h2><?php the_title(); ?></h2>
		<?php if( get_option ("breadcrumb", "off") == "on" ){ echo get_breadcrumb(); } ?>
		<?php edit_post_link( __( 'Edit', 'fringe_tech' ), '<span class="edit-link">', '</span>' ); ?>
	</div>

	<?php
		$sidebar_mod	= get_sidebar_type($post->ID);
		$sidebar_pos	= $sidebar_mod[2];
		$position		= $sidebar_mod[1];
		$class			= $sidebar_mod[0];
	?>
	
	<div class="page_container align<?php echo $position; ?> <?php echo $class; ?>">
		
		<?php the_content(); ?>
		
		<?php

		$p_id = get_post_meta($post->ID, "portfolio_id", true);
	
		/* getting category settings */
		
		$get_datas = $wpdb->get_results("SELECT * FROM " . $taxonomy_table . " WHERE t_id IN (" . $p_id . ")");
		
		foreach($get_datas as $data){
			
			$p_width	= get_the_value( "p_width", htmlSafe( $data->t_value) );
			$p_height	= get_the_value( "p_height", htmlSafe( $data->t_value) );
			$p_column 	= get_the_value( "p_column", htmlSafe( $data->t_value) );
			$p_padding 	= get_the_value( "p_padding", htmlSafe( $data->t_value) );
			$p_limit 	= get_the_value( "p_limit", htmlSafe( $data->t_value) );
			$p_title 	= get_the_value( "p_title", htmlSafe( $data->t_value) );
			$p_more 	= get_the_value( "p_more", htmlSafe( $data->t_value) );
			$p_desc 	= get_the_value( "p_desc", htmlSafe( $data->t_value) );
			$p_up		= get_the_value( "p_up", htmlSafe( $data->t_value) );
			$p_filter	= get_the_value( "p_filter", htmlSafe( $data->t_value) );
			
		}
		
		$p_items = get_taxonomy_posts($p_id, $p_limit, "DESC", true);

		/* if filterable active */
		if($p_filter == "on"){
		
			$tag_ids = array();
			foreach ( $p_items as $p ) {
				$tag_ids[] = $p->ID;
			}
			
			
			
			$the_tags =  get_tags_name_by_id(implode(",", $tag_ids ));
			
			$tag_slugs = array();
			foreach($the_tags as $tag){
				$tag_slugs[$tag->slug] = $tag->name;
			}

			$unique_tags = array_unique($tag_slugs);
		
		?>
		<div class="filterable">
			<div class="filter_count" rel="<?php echo $p_column; ?>"></div>
			<div class="filter_title"><?php _e("Filter by:", 'fringe_tech'); ?></div>
			<ul>
				<li class="filter active" data-type="all"><?php _e("All", 'fringe_tech'); ?></li>
				<?php 
				foreach($unique_tags as $key => $tag){
					echo '<li class="filter" data-type="'.$key.'">'.$tag.'</li>';
				}
				?>
			</ul>
		</div>
		<?php } ?>

		<div class="portfolio_boxes">
		
			<ul id="filter_container">

				<?php
				$i = 1;
				$s = 1;
				foreach ( $p_items as $post ) {
					setup_postdata($post);
					
					$post_thumbnail		= get_post_meta($post->ID, "_post_thumbnail", true);
				
					$thumb_type	= "";
					if( preg_match('/gif$|jpg$|png$|GIF$|JPG$|PNG$/', $post_thumbnail) ){
						$thumb_type	= "pic";
					}
					
					$post_fullsize		= get_post_meta($post->ID, "_post_fullsize", true);
					$post_short_desc	= get_post_meta($post->ID, "_short_description", true);
					
					preg_match_all("/(.*?)[\&\?]type=(.*?)\&target=(.*?)$/i", $post_fullsize, $matches);
					
					$get_tags = array();
					
					if($p_filter == "on"){
					
						$get_tags = get_tags_name_by_id($post->ID);
						
					}
				?>
				<?php if($i == $p_column+1 && $p_column != 1) { echo "<div class=\"clear_li\">&nbsp;</div>"; $i=1; } ?>

				<li data-id="id-<?php echo $s; ?>" data-type="<?php foreach($get_tags as $tag){ echo $tag->slug.' '; } ?>" <?php if($i == $p_column && $p_column != 1) { echo " class=\"last\""; } ?> style="margin-right:<?php echo $p_padding; ?>px; margin-bottom:<?php echo ($p_padding-9); ?>px;">
					
					<div class="content" style="width:<?php echo $p_width; ?>px;">
	
						<div class="preview" style="height:<?php echo $p_height; ?>px;">
							
							<div class="<?php if( $p_up != "off" && $post_fullsize != "" && $matches[2][0] != "link" ) { ?>picture<?php } ?>">
								
								<div class="alpha">
									
									<?php if( $thumb_type == "pic" ) { ?>
							
									<?php if ( $post_fullsize != "" && $matches[2][0] != "link") { ?>
									
										<a href="<?php echo $post_fullsize; ?>" rel="prettyPhoto[<?php echo $p_id; ?>]">
										
									<?php } ?>

									<?php if ( $matches[2][0] == "link" ) { ?>
									
										<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>">
										
									<?php } ?>
									
										<img src="<?php echo $post_thumbnail; ?>" alt="<?php echo $post->post_title; ?>" style="width:<?php echo $p_width; ?>px; height:<?php echo $p_height; ?>px;"/>
								
									<?php if ( $post_fullsize != "") { ?></a><?php } ?>
								
									<?php } else { ?>
									
										<?php echo $post_thumbnail; ?>
								
									<?php } ?>
									
								</div>
								
								<?php if ( $p_date != "off" ) { ?>
								<span class="date"><?php the_time('M j, Y'); ?></span>
								<?php } ?>
								
								<?php if ( $p_more != "off" ) { ?>
								<span class="comment"><?php comments_popup_link("0", "1", '% C', 'commentslink', 'Off'); ?></span>
								<?php } ?>
							
							</div>
							
						</div>
						
						<?php if ( $p_title == "on" ){ ?>
						<div class="title">
						
							<?php if ( $p_more != "off" && $matches[2][0] != "link") { ?>
							
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							
							<?php } else if ($matches[2][0] == "link" ) { ?>

							<a href="<?php echo $matches[1][0]; ?>" target="<?php echo $matches[3][0]; ?>"><?php the_title(); ?></a>
							
							<?php } else { the_title(); } ?>
							
						</div>
						<?php } ?>
						
						
						<?php if ( $post_short_desc != "" && $p_desc == "on" ){ ?>
						<div class="description">
							<?php echo add_lightbox_rel ( do_shortcode ( htmlSafe ( $post_short_desc ) ) ); ?>
						</div>
						<?php } ?>
						
					
					</div>
					
				</li>
				

				
				<?php $i++; $s++; } ?>

			</ul>

		</div>
		
		<?php wp_pagenavi(); ?>

	</div>

	<?php if($class != "" && $class != "full") { ?>
	<div class="page_sidebar align<?php echo $sidebar_pos; ?>">
		
		<?php get_sidebar(); ?>

	</div>
	<?php } ?>
	
<?php endwhile; ?>

</div>

<?php get_footer(); ?>