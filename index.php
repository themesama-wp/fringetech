<?php
/**
  * The main template file.
  * The Fringe Tech WordPress Theme for ThemeForest Buyers!
  * Enjoy.
**/

/* getting header */
get_header();

/* getting home page slider */
echo get_slider( get_option("home_page_slider") );

/* getting intro text */
echo get_home_intro();

/* getting home page design model */
echo get_home_design();

/* getting footer */
get_footer();
?>